# LIBFORMS #

A PHP HTML forms library with server side validation.

For complete documentation please visit the [libforms site](http://libforms.oratush.com)