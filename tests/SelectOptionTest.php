<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  SelectOptionTest.php
 *
 * Test select option functionality
 */

use com\oratush\forms\controls;

class SelectOptionTest extends PHPUnit_Framework_TestCase
{
    public function testSelectOption()
    {
        $selected = false;
        $disabled = true;
        $label = "Greece";
        $value = "GR";

        if ( $selected )
            $selected_text = "true";
        else
            $selected_text = "false";

        if ( $disabled )
            $disabled_text = "true";
        else
            $disabled_text = "false";

        $json_string = <<<EOT
{
    "label": "$label",
    "value": "$value",
    "disabled": $disabled_text,
    "selected": $selected_text
 }
EOT;

        $a = new controls\SelectOption();
        $a->parseJSON(json_decode($json_string, true));

        $this->assertEquals($label, $a->getLabel());
        $this->assertEquals($value, $a->getValue());
        $this->assertEquals($disabled, $a->isDisabled());
        $this->assertEquals($selected, $a->isSelected());

        $selected = true;
        $a->setSelected($selected);
        $this->assertEquals($selected, $a->isSelected());
    }
}
