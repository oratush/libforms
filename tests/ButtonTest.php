<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  ButtonTest.php
 *
 * Test button functionality
 */

use com\oratush\forms\controls;

class ButtonTest extends PHPUnit_Framework_TestCase
{
    private function getButton($type, $link = null)
    {
        $button = '{ "label": "Submit", "type": "'. $type .'"';
        if ( isset($link) )
        {
            $button .= ', "link": "'. $link .'"';
        }
        $button .= ' }';

        $a = new controls\Button();
        $a->parseJSON(json_decode($button, true));

        return $a;
    }

    public function testButton()
    {
        $type = "button";

        $a = self::getButton($type);

        $this->assertEquals($type, $a->getType());
    }

    public function testReset()
    {
        $type = "reset";

        $a = self::getButton($type);

        $this->assertEquals($type, $a->getType());
    }

    public function testSubmit()
    {
        $type = "submit";

        $a = self::getButton($type);

        $this->assertEquals($type, $a->getType());
    }

    public function testLink()
    {
        $type = "link";
        $link = "http://www.example.com";

        $a = self::getButton($type, $link);

        $this->assertEquals($type, $a->getType());
        $this->assertEquals($link, $a->getLink());
    }

    public function testDummy()
    {
        $type = "dummy";

        $a = self::getButton($type);

        $this->assertNotEquals($type, $a->getType());
    }
}
