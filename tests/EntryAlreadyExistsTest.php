<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  EntryAlreadyExistsTest.php
 *
 * Test EntryAlreadyExists validator functionality, requires database
 */

use com\oratush\forms;

class EntryAlreadyExistsTest extends ValidatorBase
{
    public function getValidators()
    {
        $validators = <<<EOT
{
    "EntryAlreadyExists": {
        "id": "CheckUsername1",
        "name": "inputtext1",
        "dbc": "db_conn",
        "query": "SELECT COUNT(user_id) FROM users WHERE username=?"
    }
},
{
    "EntryAlreadyExists": {
        "id": "CheckUsername2",
        "name": "inputtext2",
        "dbc": "db_conn",
        "query": "BAD QUERY"
    }
},
{
    "EntryAlreadyExists": {
        "id": "CheckUsername3",
        "name": "inputtext3",
        "dbc": "db_conn",
        "query": "SELECT COUNT(user_id) FROM users WHERE username=?"
    }
},
{
    "EntryAlreadyExists": {
        "id": "CheckUsername4",
        "name": "inputtext1",
        "dbc": "db_conn",
        "query": "SELECT user_id FROM users WHERE username='dummy' AND username=?"
    }
},
{
    "EntryAlreadyExists": {
        "id": "CheckUsername5",
        "name": "inputtext1",
        "dbc": "db_conn",
        "query": "SELECT username FROM users WHERE username=?"
    }
}
EOT;

        return $validators;
    }

    public function getBadValidators()
    {
        $validators = <<<EOT
{
    "EntryAlreadyExists": {
        "id": "CheckUsername1",
        "name": "inputtext1",
        "dbc": "db_conn",
        "query": "SELECT COUNT(user_id) FROM users WHERE username=?"
    }
},
{
    "EntryAlreadyExists": {
        "id": "CheckUsername2",
        "name": "inputpass1",
        "dbc": "db_conn",
        "query": "SELECT COUNT(user_id) FROM users WHERE username=?"
    }
},
{
    "EntryAlreadyExists": {
        "id": "CheckUsername3",
        "name": "inputtext2",
        "dbc": "db_conn",
        "query": "SELECT COUNT(user_id) FROM users WHERE username=?"
    }
},
{
    "EntryAlreadyExists": {
        "id": "CheckUsername4",
        "name": "inputtext3",
        "dbc": "db_conn",
        "query": "SELECT COUNT(user_id) FROM users WHERE username=?"
    }
}
EOT;

        return $validators;
    }

    public function getExcludeValidators()
    {
        $validators = <<<EOT
{
    "EntryAlreadyExists": {
        "id": "CheckUsername1",
        "name": "inputtext1",
        "dbc": "db_conn",
        "query": "SELECT COUNT(user_id) FROM users WHERE username=?"
    }
}
EOT;

        return $validators;
    }

    public function getElements1()
    {
        $fields  = parent::getInputText("inputtext1", "oratush");               //Error exists
        $fields .= ", ". parent::getInputText("inputtext2", "oratush");         //Error query
        $fields .= ", ". parent::getInputText("inputtext3", "another");         //No error not exists

        return $fields;
    }

    public function getElements2()
    {
        $fields  = parent::getInputText("inputtext1", "oratush");               //Error DB
        $fields .= ", ". parent::getInputPassword("inputpass1", "oratush");     //No error password field
        $fields .= ", ". parent::getInputText("inputtext2");                    //No error empty value

        return $fields;
    }

    public function getElements3()
    {
        $fields  = parent::getInputText("inputtext1", "oratush");               //Error exists

        return $fields;
    }

    public function testValidator()
    {
        $db_conn = new PDO('mysql:host=localhost;dbname=oratush;charset=utf8', 'oratush', 'oratush');
        $db_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $GLOBALS["db_conn"] = $db_conn;

        $errors = self::getForm(self::getElements1(), self::getValidators());

        $this->assertEquals(4, count($errors));

        $this->assertEquals(forms\FormError::ERROR_DB_ENTRY_EXISTS, $errors[0]->getCode());
        $this->assertEquals("inputtext1", $errors[0]->getControl()->getName());
        $payload = json_decode($errors[0]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals("Result should be 0 but got: 1", $payload["message"]);

        $this->assertEquals(forms\FormError::ERROR_DB_ENTRY_EXISTS, $errors[1]->getCode());
        $this->assertEquals("inputtext2", $errors[1]->getControl()->getName());
        $payload = json_decode($errors[1]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertStringStartsWith("Database exception: ", $payload["message"]);

        $this->assertEquals(forms\FormError::ERROR_DB_ENTRY_EXISTS, $errors[2]->getCode());
        $this->assertEquals("inputtext1", $errors[2]->getControl()->getName());
        $payload = json_decode($errors[2]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertStringStartsWith("Query must return a row!", $payload["message"]);

        $this->assertEquals(forms\FormError::ERROR_DB_ENTRY_EXISTS, $errors[3]->getCode());
        $this->assertEquals("inputtext1", $errors[3]->getControl()->getName());
        $payload = json_decode($errors[3]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals("Result should be 0 but got: oratush", $payload["message"]);
    }

    public function testNoDBValidator()
    {
        $errors = self::getForm(self::getElements2(), self::getBadValidators());

        $this->assertEquals(2, count($errors));

        $this->assertEquals(forms\FormError::ERROR_DB_ENTRY_EXISTS, $errors[0]->getCode());
        $this->assertEquals("inputtext1", $errors[0]->getControl()->getName());
        $payload = json_decode($errors[0]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals("Database variable not found in GLOBALS array: db_conn", $payload["message"]);

        $this->assertEquals(forms\FormError::ERROR_FIELD_NOT_FOUND, $errors[1]->getCode());
        $this->assertEquals("inputtext3", json_decode($errors[1]->getPayload(), true)["field"]);
    }

    public function testExclude()
    {
        $db_conn = new PDO('mysql:host=localhost;dbname=oratush;charset=utf8', 'oratush', 'oratush');
        $db_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $GLOBALS["db_conn"] = $db_conn;

        $form = self::getOriginalForm(self::getElements3(), self::getExcludeValidators());

        $form->isValid();
        $errors = $form->getErrors();

        $this->assertEquals(1, count($errors));

        $this->assertEquals(forms\FormError::ERROR_DB_ENTRY_EXISTS, $errors[0]->getCode());
        $this->assertEquals("inputtext1", $errors[0]->getControl()->getName());
        $payload = json_decode($errors[0]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals("Result should be 0 but got: 1", $payload["message"]);

        $validators = $form->getValidators();
        $validators[0]->setExclude(" AND user_id<>10");

        $this->assertTrue($form->isValid());
    }
}
