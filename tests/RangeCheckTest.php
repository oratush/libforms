<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  RangeCheckTest.php
 *
 * Test range validator functionality
 */

use com\oratush\forms;

class RangeCheckTest extends ValidatorBase
{
    public function getValidators()
    {
        $validators = <<<EOT
{
    "RangeCheck": {
        "fields": [
            { "name": "inputtext1", "min": 2, "max": 10 },
            { "name": "inputtext2", "min": 2, "max": 10 },
            { "name": "inputtext3", "min": 2, "max": 10 },

            { "name": "inputtext2", "min": 2 },
            { "name": "inputtext1", "max": 10 },

            { "name": "inputtext3", "min": 5, "max": 5 },

            { "name": "inputtext1", "min": 2 },
            { "name": "inputtext2", "max": 10 },

            { "name": "inputtext1", "min": 1 },
            { "name": "inputtext2", "max": 11 },

            { "name": "inputtext4", "min": 2, "max": 10 }
        ]
    }
}
EOT;

        return $validators;
    }

    public function getElements()
    {
        $fields  = parent::getInputText("inputtext1", "1");
        $fields .= ", ". parent::getInputText("inputtext2", "11");
        $fields .= ", ". parent::getInputText("inputtext3", "5");
        $fields .= ", ". parent::getInputText("inputtext4", "value");

        return $fields;
    }

    public function testValidator()
    {
        $errors = self::getForm(self::getElements(), self::getValidators());

        $this->assertEquals(4, count($errors));

        $this->assertEquals(forms\FormError::ERROR_RANGE_TOO_LOW, $errors[0]->getCode());
        $this->assertEquals("inputtext1", $errors[0]->getControl()->getName());
        $payload = json_decode($errors[0]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals(2, $payload["min_range"]);

        $this->assertEquals(forms\FormError::ERROR_RANGE_TOO_HIGH, $errors[1]->getCode());
        $this->assertEquals("inputtext2", $errors[1]->getControl()->getName());
        $payload = json_decode($errors[1]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals(10, $payload["max_range"]);

        $this->assertEquals(forms\FormError::ERROR_RANGE_TOO_LOW, $errors[2]->getCode());
        $this->assertEquals("inputtext1", $errors[2]->getControl()->getName());
        $payload = json_decode($errors[2]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals(2, $payload["min_range"]);

        $this->assertEquals(forms\FormError::ERROR_RANGE_TOO_HIGH, $errors[3]->getCode());
        $this->assertEquals("inputtext2", $errors[3]->getControl()->getName());
        $payload = json_decode($errors[3]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals(10, $payload["max_range"]);
    }
}
