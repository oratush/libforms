<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  FileCheckTest.php
 *
 * Test file upload validator functionality
 */

use com\oratush\forms;

class FileCheckTest extends ValidatorBase
{
    public function getValidators()
    {
        $validators = <<<EOT
{
    "FileCheck": {
        "fields": [
            { "name": "file1", "max": 2048 },
            { "name": "file2", "max": 1024 },
            { "name": "file1", "mime": "image/gif" },
            { "name": "file2", "mime": "image/x-png" },
            { "name": "file1", "ext": "gif" },
            { "name": "file2", "ext": "png" },
            { "name": "file1", "img_width": 200, "img_height": 200 },
            { "name": "file1", "img_width": 200 },
            { "name": "file1", "img_height": 200 },
            { "name": "file2", "img_width": 300, "img_height": 300 },
            { "name": "file2", "img_width": 300 },
            { "name": "file2", "img_height": 300 },
            { "name": "file1", "max": 2048, "mime": "image/x-png", "img_height": 200 },
            { "name": "file3", "max": 2048 },
            { "name": "file4", "img_height": 300 },
            { "name": "inputtext1", "max": 1024 }
        ]
    }
}
EOT;

        return $validators;
    }

    public function getElements()
    {
        $fields  = parent::getInputFile("file1");
        $fields .= ", ". parent::getInputFile("file2");
        $fields .= ", ". parent::getInputFile("file3");
        $fields .= ", ". parent::getInputFile("file4");
        $fields .= ", ". parent::getInputText("inputtext1");

        return $fields;
    }

    private function getInputFileMultipleControl($name)
    {
        $json_element = <<<EOT
            {
                "InputFile": {
                    "name": "$name",
                    "multiple": true
                }
            }
EOT;

        return $json_element;
    }

    public function getElementsMultiFiles()
    {
        $fields  = $this->getInputFileMultipleControl("file1");
        $fields .= ", ". $this->getInputFileMultipleControl("file2");
        $fields .= ", ". $this->getInputFileMultipleControl("file3");
        $fields .= ", ". $this->getInputFileMultipleControl("file4");
        $fields .= ", ". parent::getInputFile("file5");
        $fields .= ", ". parent::getInputText("inputtext1");

        return $fields;
    }

    public function testValidator()
    {
        $_FILES["file1"]["error"] = 0;
        $_FILES["file1"]["size"] = 1536;
        $_FILES["file1"]["type"] = "image/gif";
        $_FILES["file1"]["name"] = "sample.gif";
        $_FILES["file1"]["tmp_name"] = __DIR__ ."/gnu.png";

        $_FILES["file2"]["error"] = 0;
        $_FILES["file2"]["size"] = 1536;
        $_FILES["file2"]["type"] = "image/gif";
        $_FILES["file2"]["name"] = "sample.gif";
        $_FILES["file2"]["tmp_name"] = __DIR__ ."/gnu.png";

        $_FILES["file3"]["error"] = 1;

        $_FILES["file4"]["error"] = 0;
        $_FILES["file4"]["size"] = 1536;
        $_FILES["file4"]["type"] = "image/gif";
        $_FILES["file4"]["name"] = "sample.gif";
        $_FILES["file4"]["tmp_name"] = __DIR__ ."/FileCheckTest.php";

        $_FILES["inputtext1"]["error"] = 0;
        $_FILES["inputtext1"]["size"] = 1536;
        $_FILES["inputtext1"]["type"] = "image/gif";
        $_FILES["inputtext1"]["name"] = "sample.gif";
        $_FILES["inputtext1"]["tmp_name"] = __DIR__ ."/gnu.png";

        $errors = self::getForm(self::getElements(), self::getValidators());

        $this->assertEquals(8, count($errors));

        $this->assertEquals(forms\FormError::ERROR_FILE_MAX_SIZE_LIMIT_WITH_PAYLOAD, $errors[0]->getCode());
        $this->assertEquals("file2", $errors[0]->getControl()->getName());
        $payload = json_decode($errors[0]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals(1024, $payload["max"]);
        $this->assertEquals(1536, $payload["actual"]);

        $this->assertEquals(forms\FormError::ERROR_FILE_MIME_TYPE, $errors[1]->getCode());
        $this->assertEquals("file2", $errors[1]->getControl()->getName());
        $payload = json_decode($errors[1]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals("image/x-png", $payload["allowed"]);
        $this->assertEquals("image/gif", $payload["mime"]);

        $this->assertEquals(forms\FormError::ERROR_FILE_EXTENSION, $errors[2]->getCode());
        $this->assertEquals("file2", $errors[2]->getControl()->getName());
        $payload = json_decode($errors[2]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals("png", $payload["allowed"]);
        $this->assertEquals("gif", $payload["extension"]);

        $this->assertEquals(forms\FormError::ERROR_FILE_BAD_IMAGE_DIMENSIONS, $errors[3]->getCode());
        $this->assertEquals("file1", $errors[3]->getControl()->getName());
        $payload = json_decode($errors[3]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals(200, $payload["width"]["required"]);
        $this->assertEquals(300, $payload["width"]["actual"]);
        $this->assertEquals(200, $payload["height"]["required"]);
        $this->assertEquals(300, $payload["height"]["actual"]);

        $this->assertEquals(forms\FormError::ERROR_FILE_BAD_IMAGE_DIMENSIONS, $errors[4]->getCode());
        $this->assertEquals("file1", $errors[4]->getControl()->getName());
        $payload = json_decode($errors[4]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals(200, $payload["width"]["required"]);
        $this->assertEquals(300, $payload["width"]["actual"]);

        $this->assertEquals(forms\FormError::ERROR_FILE_BAD_IMAGE_DIMENSIONS, $errors[5]->getCode());
        $this->assertEquals("file1", $errors[5]->getControl()->getName());
        $payload = json_decode($errors[5]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals(200, $payload["height"]["required"]);
        $this->assertEquals(300, $payload["height"]["actual"]);

        $this->assertEquals(forms\FormError::ERROR_FILE_MIME_TYPE, $errors[6]->getCode());
        $this->assertEquals("file1", $errors[6]->getControl()->getName());
        $payload = json_decode($errors[6]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals("image/x-png", $payload["allowed"]);
        $this->assertEquals("image/gif", $payload["mime"]);

        $this->assertEquals(forms\FormError::ERROR_FILE_BAD_IMAGE, $errors[7]->getCode());
        $this->assertEquals("file4", $errors[7]->getControl()->getName());
    }

    public function testValidatorMultiFiles()
    {
        $_FILES["file1"]["error"][0] = 0;
        $_FILES["file1"]["size"][0] = 1536;
        $_FILES["file1"]["type"][0] = "image/gif";
        $_FILES["file1"]["name"][0] = "sample.gif";
        $_FILES["file1"]["tmp_name"][0] = __DIR__ ."/gnu2.png";
        $_FILES["file1"]["error"][1] = 0;
        $_FILES["file1"]["size"][1] = 1536;
        $_FILES["file1"]["type"][1] = "image/gif";
        $_FILES["file1"]["name"][1] = "sample.gif";
        $_FILES["file1"]["tmp_name"][1] = __DIR__ ."/gnu.png";

        $_FILES["file2"]["error"][0] = 0;
        $_FILES["file2"]["size"][0] = 1536;
        $_FILES["file2"]["type"][0] = "image/x-png";
        $_FILES["file2"]["name"][0] = "sample.gif";
        $_FILES["file2"]["tmp_name"][0] = __DIR__ ."/gnu.png";
        $_FILES["file2"]["error"][1] = 0;
        $_FILES["file2"]["size"][1] = 512;
        $_FILES["file2"]["type"][1] = "image/gif";
        $_FILES["file2"]["name"][1] = "sample.png";
        $_FILES["file2"]["tmp_name"][1] = __DIR__ ."/gnu.png";

        $_FILES["file3"]["error"][0] = 0;
        $_FILES["file3"]["error"][1] = 1;

        $_FILES["file4"]["error"][0] = 0;
        $_FILES["file4"]["size"][0] = 1536;
        $_FILES["file4"]["type"][0] = "image/gif";
        $_FILES["file4"]["name"][0] = "sample.gif";
        $_FILES["file4"]["tmp_name"][0] = __DIR__ ."/FileCheckTest.php";

        $_FILES["file5"]["error"] = 1;

        $_FILES["inputtext1"]["error"][0] = 0;
        $_FILES["inputtext1"]["size"][0] = 1536;
        $_FILES["inputtext1"]["type"][0] = "image/gif";
        $_FILES["inputtext1"]["name"][0] = "sample.gif";
        $_FILES["inputtext1"]["tmp_name"][0] = __DIR__ ."/gnu.png";

        $errors = self::getForm(self::getElementsMultiFiles(), self::getValidators());

        $this->assertEquals(8, count($errors));

        $this->assertEquals(forms\FormError::ERROR_FILE_MAX_SIZE_LIMIT_WITH_PAYLOAD, $errors[0]->getCode());
        $this->assertEquals("file2", $errors[0]->getControl()->getName());
        $payload = json_decode($errors[0]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals(1024, $payload["max"]);
        $this->assertEquals(1536, $payload["actual"]);

        $this->assertEquals(forms\FormError::ERROR_FILE_MIME_TYPE, $errors[1]->getCode());
        $this->assertEquals("file2", $errors[1]->getControl()->getName());
        $payload = json_decode($errors[1]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals("image/x-png", $payload["allowed"]);
        $this->assertEquals("image/gif", $payload["mime"]);

        $this->assertEquals(forms\FormError::ERROR_FILE_EXTENSION, $errors[2]->getCode());
        $this->assertEquals("file2", $errors[2]->getControl()->getName());
        $payload = json_decode($errors[2]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals("png", $payload["allowed"]);
        $this->assertEquals("gif", $payload["extension"]);

        $this->assertEquals(forms\FormError::ERROR_FILE_BAD_IMAGE_DIMENSIONS, $errors[3]->getCode());
        $this->assertEquals("file1", $errors[3]->getControl()->getName());
        $payload = json_decode($errors[3]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals(200, $payload["width"]["required"]);
        $this->assertEquals(300, $payload["width"]["actual"]);
        $this->assertEquals(200, $payload["height"]["required"]);
        $this->assertEquals(300, $payload["height"]["actual"]);

        $this->assertEquals(forms\FormError::ERROR_FILE_BAD_IMAGE_DIMENSIONS, $errors[4]->getCode());
        $this->assertEquals("file1", $errors[4]->getControl()->getName());
        $payload = json_decode($errors[4]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals(200, $payload["width"]["required"]);
        $this->assertEquals(300, $payload["width"]["actual"]);

        $this->assertEquals(forms\FormError::ERROR_FILE_BAD_IMAGE_DIMENSIONS, $errors[5]->getCode());
        $this->assertEquals("file1", $errors[5]->getControl()->getName());
        $payload = json_decode($errors[5]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals(200, $payload["height"]["required"]);
        $this->assertEquals(300, $payload["height"]["actual"]);

        $this->assertEquals(forms\FormError::ERROR_FILE_MIME_TYPE, $errors[6]->getCode());
        $this->assertEquals("file1", $errors[6]->getControl()->getName());
        $payload = json_decode($errors[6]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals("image/x-png", $payload["allowed"]);
        $this->assertEquals("image/gif", $payload["mime"]);

        $this->assertEquals(forms\FormError::ERROR_FILE_BAD_IMAGE, $errors[7]->getCode());
        $this->assertEquals("file4", $errors[7]->getControl()->getName());
    }
}
