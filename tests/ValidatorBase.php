<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  ValidatorBase.php
 *
 * Base class to test validator functionality
 */

use com\oratush\forms;

class ValidatorBase extends PHPUnit_Framework_TestCase
{
    private static function getFormHeader($elements)
    {
        $json_form = <<<EOT
{
  "id": "unit_frm",
  "name": "unit_frm",
  "controls": [
      {
        "Fieldset": {
          "legend": "Unit test",
          "controls": [
                $elements
          ]
        }
      }
    ],
  "validators": [
EOT;

        return $json_form;
    }

    private static function getFormFooter()
    {
        return "]}";
    }

    protected static function getForm($elements, $validators)
    {
        $total  = self::getFormHeader($elements);
        $total .= $validators;
        $total .= self::getFormFooter();

        //$json_input = json_decode($total, true);
        //print_r($json_input);
        //echo $total;

        $form = new forms\Form();
        $form->parseJSON($total);
        $form->isValid();

        return $form->getErrors();
    }

    protected static function getOriginalForm($elements, $validators)
    {
        $total  = self::getFormHeader($elements);
        $total .= $validators;
        $total .= self::getFormFooter();

        //$json_input = json_decode($total, true);
        //print_r($json_input);
        //echo $total;

        $form = new forms\Form();
        $form->parseJSON($total);

        return $form;
    }
    
    protected static function getInputText($name, $value = null)
    {
        return self::getControl("InputText", $name, $value);
    }

    protected static function getInputPassword($name, $value = null)
    {
        return self::getControl("InputPassword", $name, $value);
    }

    protected static function getInputHidden($name, $value = null)
    {
        return self::getControl("InputHidden", $name, $value);
    }

    protected static function getInputFile($name)
    {
        return self::getControl("InputFile", $name, null);
    }

    protected static function getCaptcha($name, $value = null)
    {
        return self::getControl("Captcha", $name, $value);
    }

    protected static function getTextArea($name, $value = null)
    {
        return self::getControl("TextArea", $name, $value);
    }

    private static function getControl($type, $name, $value = null)
    {
        $value_str = "";
        if ( !empty($value) )
            $value_str = ', "value": "'. $value .'"';

        $json_element = <<<EOT
            {
                "$type": {
                    "name": "$name"
                    $value_str
                }
            }
EOT;

        return $json_element;
    }

    protected static function getInputCheckbox($name, $checked = false)
    {
        return self::getSelectControl("InputCheckbox", $name, $checked);
    }

    protected static function getInputRadio($name, $checked = false)
    {
        return self::getSelectControl("InputRadio", $name, $checked);
    }

    protected static function getSelect($name, $checked = false)
    {
        return self::getSelectControl("Select", $name, $checked);
    }

    private static function getSelectControl($type, $name, $checked = false)
    {
        $value_str = "";
        if ( $checked )
            $value_str = ',"selected": true';

        $json_element = <<<EOT
            {
                "$type": {
                    "name": "$name",
                    "elements": [
                        {
                            "label": "Greece",
                            "value": "GR"
                            $value_str
                        },
                        {
                            "label": "Germany",
                            "value": "DE"
                        },
                        {
                            "label": "France",
                            "value": "FR"
                        }
                    ]
                }
            }
EOT;

        return $json_element;
    }
}
