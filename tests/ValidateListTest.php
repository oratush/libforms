<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  ValidateListTest.php
 *
 * Test list validator functionality
 */

use com\oratush\forms;

class ValidateListTest extends ValidatorBase
{
    public function getValidators()
    {
        $validators = <<<EOT
{
    "ValidateList": {
        "fields": [
            { "name": "inputtext1", "separator": ",", "values": "GR,FR" },
            { "name": "inputtext2", "separator": ",", "values": "GR,FR" },
            { "name": "inputtext3", "separator": ",", "values": "GR,FR" },
            { "name": "inputtext1", "multiple": true, "separator": ",", "values": "GR,FR" },
            { "name": "inputtext2", "multiple": true, "separator": ",", "values": "GR,FR" }
        ]
    }
}
EOT;

        return $validators;
    }

    public function getElements()
    {
        $fields  = parent::getInputText("inputtext1", "GR,FR");
        $fields .= ", ". parent::getInputText("inputtext2", "FR");
        $fields .= ", ". parent::getInputText("inputtext3", "DE");

        return $fields;
    }

    public function testLists()
    {
        $errors = self::getForm(self::getElements(), self::getValidators());

        $this->assertEquals(2, count($errors));

        $this->assertEquals(forms\FormError::ERROR_INVALID_LIST, $errors[0]->getCode());
        $this->assertEquals("inputtext1", $errors[0]->getControl()->getName());

        $payload = $errors[0]->getPayload();
        $this->assertNotNull($payload);
        $json = json_decode($payload, true);
        $this->assertFalse($json["multiple"]);
        $this->assertEquals("GR,FR", $json["selection"]);
        $this->assertEquals("GR,FR", $json["values"]);

        $this->assertEquals(forms\FormError::ERROR_INVALID_LIST, $errors[1]->getCode());
        $this->assertEquals("inputtext3", $errors[1]->getControl()->getName());

        $payload = $errors[1]->getPayload();
        $this->assertNotNull($payload);
        $json = json_decode($payload, true);
        $this->assertEquals("DE", $json["selection"]);
        $this->assertEquals("GR,FR", $json["values"]);
    }
}
