<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  ValidDateTest.php
 *
 * Test date validator functionality
 */

use com\oratush\forms;

class ValidDateTest extends ValidatorBase
{
    public function getValidators()
    {
        $validators = <<<EOT
{
    "ValidDate": {
        "format": "j/n/Y H:i",
        "fields": [
            "inputtext1",
            "inputtext2",
            "inputtext3"
        ]
    }
}
EOT;

        return $validators;
    }

    public function getElements()
    {
        $fields  = parent::getInputText("inputtext1", "26/10/2014 9:53");
        $fields .= ", ". parent::getInputText("inputtext2", "dummy");
        $fields .= ", ". parent::getInputText("inputtext3", "30/2/2014 9:53");

        return $fields;
    }

    public function testValidator()
    {
        $errors = self::getForm(self::getElements(), self::getValidators());

        $this->assertEquals(2, count($errors));

        $this->assertEquals(forms\FormError::ERROR_INVALID_DATE, $errors[0]->getCode());
        $this->assertEquals("inputtext2", $errors[0]->getControl()->getName());
        $payload = $errors[0]->getPayload();
        $this->assertNotNull($payload);
        $json = json_decode($payload, true);
        $this->assertEquals("j/n/Y H:i", $json["format"]);

        $this->assertEquals(forms\FormError::ERROR_INVALID_DATE, $errors[1]->getCode());
        $this->assertEquals("inputtext3", $errors[1]->getControl()->getName());
        $this->assertNotNull($errors[1]->getPayload());
    }
}
