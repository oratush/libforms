<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  ValidateSelectionTest.php
 *
 * Test selection validator functionality
 */

use com\oratush\forms;

class ValidateSelectionTest extends ValidatorBase
{
    public function getValidators()
    {
        $validators = <<<EOT
{
    "ValidateSelection": {
        "fields": [
            { "name": "inputcheck1", "separator": ",", "values": "GR,FR" },
            { "name": "inputradio1", "separator": ",", "values": "GR,FR" },
            { "name": "select1", "separator": ",", "values": "GR,FR" },
            { "name": "inputtext1", "separator": ",", "values": "GR,FR" }
        ]
    }
}
EOT;

        return $validators;
    }

    public function getElements()
    {
        $fields = <<<EOT
            {
                "InputCheckbox": {
                    "name": "inputcheck1",
                    "elements": [
                        {
                            "label": "Greece",
                            "value": "GR"
                        },
                        {
                            "label": "Germany",
                            "value": "DE"
                        },
                        {
                            "label": "France",
                            "value": "FR"
                        }
                    ]
                }
            },
            {
                "InputRadio": {
                    "name": "inputradio1",
                    "elements": [
                        {
                            "label": "Greece",
                            "value": "GR"
                        },
                        {
                            "label": "Germany",
                            "value": "DE"
                        },
                        {
                            "label": "France",
                            "value": "FR"
                        }
                    ]
                }
            },
            {
                "Select": {
                    "name": "select1",
                    "elements": [
                        {
                            "label": "Greece",
                            "value": "GR"
                        },
                        {
                            "label": "Rest",
                            "values": [
                                {
                                    "label": "Germany",
                                    "value": "DE"
                                },
                                {
                                    "label": "France",
                                    "value": "FR"
                                }
                            ]
                        }
                    ]
                }
            }
EOT;
        $fields .= ", ". parent::getInputText("inputtext1", "dummy");

        return $fields;
    }

    public function testNoneSelected()
    {
        $errors = self::getForm(self::getElements(), self::getValidators());

        $this->assertEquals(0, count($errors));
    }

    public function testGoodSelection()
    {
        $form = self::getOriginalForm(self::getElements(), self::getValidators());
        $items = $form->getControls()[0]->getControls();
        $items[0]->setSelected("GR");
        $items[1]->setSelected("FR");
        $items[2]->setMultiple(true);
        $items[2]->setSelected("GR");
        $items[2]->setSelected("FR");

        $form->isValid();
        $errors = $form->getErrors();

        $this->assertEquals(0, count($errors));

        $items[2]->removeSelections();
        $items[2]->setMultiple(false);
        $items[2]->setSelected("GR");

        $form->isValid();
        $errors = $form->getErrors();

        $this->assertEquals(0, count($errors));
    }

    public function testMultiselect()
    {
        $form = self::getOriginalForm(self::getElements(), self::getValidators());
        $items = $form->getControls()[0]->getControls();
        $items[1]->setSelected("GR");
        $items[1]->setSelected("FR");
        $items[2]->setSelected("GR");
        $items[2]->setSelected("FR");

        $form->isValid();
        $errors = $form->getErrors();

        $this->assertEquals(2, count($errors));

        $this->assertEquals(forms\FormError::ERROR_MULTIPLE_SELECTIONS, $errors[0]->getCode());
        $this->assertEquals("inputradio1", $errors[0]->getControl()->getName());

        $this->assertEquals(forms\FormError::ERROR_MULTIPLE_SELECTIONS, $errors[1]->getCode());
        $this->assertEquals("select1", $errors[1]->getControl()->getName());
    }

    public function testBadSelection()
    {
        $form = self::getOriginalForm(self::getElements(), self::getValidators());
        $items = $form->getControls()[0]->getControls();
        $items[0]->setSelected("DE");

        $form->isValid();
        $errors = $form->getErrors();

        $this->assertEquals(1, count($errors));

        $this->assertEquals(forms\FormError::ERROR_INVALID_SELECTIONS, $errors[0]->getCode());
        $this->assertEquals("inputcheck1", $errors[0]->getControl()->getName());

        $payload = $errors[0]->getPayload();
        $this->assertNotNull($payload);
        $json = json_decode($payload, true);
        $this->assertEquals("DE", $json["selection"]);
        $this->assertEquals("GR,FR", $json["values"]);
    }
}
