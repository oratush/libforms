<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  ValidFloatTest.php
 *
 * Test float validator functionality
 */

use com\oratush\forms;

class ValidFloatTest extends ValidatorBase
{
    public function getValidators1()
    {
        $validators = <<<EOT
{
    "ValidFloat": {
        "fields": [
            "inputtext1",
            "inputtext2",
            "inputtext3",
            "inputtext4",
            "inputtext5",
            "inputtext6",
            "inputtext8",
            "inputtext9",
            "inputtext10",
            "inputtext11"
        ]
    }
}
EOT;

        return $validators;
    }

    public function getElements1()
    {
        $fields  = parent::getInputText("inputtext1");
        $fields .= ", ". parent::getInputText("inputtext2", "3.14");
        $fields .= ", ". parent::getInputText("inputtext3", "+3.14");
        $fields .= ", ". parent::getInputText("inputtext4", "-3");
        $fields .= ", ". parent::getInputText("inputtext5", "3");
        $fields .= ", ". parent::getInputText("inputtext6", "0.14");
        $fields .= ", ". parent::getInputText("inputtext7", "value");
        $fields .= ", ". parent::getInputText("inputtext8", "value");
        $fields .= ", ". parent::getInputText("inputtext9", "3.14.14");
        $fields .= ", ". parent::getInputText("inputtext10", ".14");
        $fields .= ", ". parent::getInputText("inputtext11", "3,14");

        return $fields;
    }

    public function testValidator1()
    {
        $errors = self::getForm(self::getElements1(), self::getValidators1());

        $this->assertEquals(4, count($errors));

        $this->assertEquals(forms\FormError::ERROR_INVALID_FLOAT, $errors[0]->getCode());
        $this->assertEquals("inputtext8", $errors[0]->getControl()->getName());
        $this->assertEquals(forms\FormError::ERROR_INVALID_FLOAT, $errors[1]->getCode());
        $this->assertEquals("inputtext9", $errors[1]->getControl()->getName());
        $this->assertEquals(forms\FormError::ERROR_INVALID_FLOAT, $errors[2]->getCode());
        $this->assertEquals("inputtext10", $errors[2]->getControl()->getName());
        $this->assertEquals(forms\FormError::ERROR_INVALID_FLOAT, $errors[3]->getCode());
        $this->assertEquals("inputtext11", $errors[3]->getControl()->getName());
    }

    public function getValidators2()
    {
        $validators = <<<EOT
{
    "ValidFloat": {
        "separator": ",",
        "fields": [
            "inputtext1",
            "inputtext2",
            "inputtext3",
            "inputtext4",
            "inputtext5",
            "inputtext6",
            "inputtext8",
            "inputtext9",
            "inputtext10",
            "inputtext11"
        ]
    }
}
EOT;

        return $validators;
    }

    public function getElements2()
    {
        $fields  = parent::getInputText("inputtext1");
        $fields .= ", ". parent::getInputText("inputtext2", "3,14");
        $fields .= ", ". parent::getInputText("inputtext3", "+3,14");
        $fields .= ", ". parent::getInputText("inputtext4", "-3");
        $fields .= ", ". parent::getInputText("inputtext5", "3");
        $fields .= ", ". parent::getInputText("inputtext6", "0,14");
        $fields .= ", ". parent::getInputText("inputtext7", "value");
        $fields .= ", ". parent::getInputText("inputtext8", "value");
        $fields .= ", ". parent::getInputText("inputtext9", "3,14,14");
        $fields .= ", ". parent::getInputText("inputtext10", ",14");
        $fields .= ", ". parent::getInputText("inputtext11", "3.14");

        return $fields;
    }

    public function testValidator2()
    {
        $errors = self::getForm(self::getElements2(), self::getValidators2());

        $this->assertEquals(4, count($errors));

        $this->assertEquals(forms\FormError::ERROR_INVALID_FLOAT, $errors[0]->getCode());
        $this->assertEquals("inputtext8", $errors[0]->getControl()->getName());
        $this->assertEquals(forms\FormError::ERROR_INVALID_FLOAT, $errors[1]->getCode());
        $this->assertEquals("inputtext9", $errors[1]->getControl()->getName());
        $this->assertEquals(forms\FormError::ERROR_INVALID_FLOAT, $errors[2]->getCode());
        $this->assertEquals("inputtext10", $errors[2]->getControl()->getName());
        $this->assertEquals(forms\FormError::ERROR_INVALID_FLOAT, $errors[3]->getCode());
        $this->assertEquals("inputtext11", $errors[3]->getControl()->getName());
    }
}
