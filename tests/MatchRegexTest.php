<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  MatchRegexTest.php
 *
 * Test match regex validator functionality
 */

use com\oratush\forms;

class MatchRegexTest extends ValidatorBase
{
    public function getValidators()
    {
        $validators = <<<EOT
{
    "MatchRegex": {
        "fields": [
            { "name": "inputtext1", "expression": "^69[0-9]{8}$", "errorno": 10, "errordesc": "This is not a valid telephone number." },
            { "name": "inputtext2", "expression": "^69[0-9]{8}$", "errorno": 10, "errordesc": "This is not a valid telephone number." },
            { "name": "select1", "expression": "^69[0-9]{8}$" },
            { "name": "select2", "expression": "^69[0-9]{8}$" },
            { "name": "inputpass1", "expression": "^69[0-9]{8}$", "errorno": 10, "errordesc": "This is not a valid telephone number." },
            { "name": "textarea1", "expression": "^69[0-9]{8}$" },
            { "name": "inputhidden1", "expression": "^69[0-9]{8}$" },
            { "name": "inputtext5", "expression": "^69[0-9]{8}$" },
            { "name": "inputtext6", "expression": "^69[0-9]{8}$" },
            { "name": "inputtext7", "expression": "^69[0-9]{8}$" }
        ]
    }
}
EOT;

        return $validators;
    }

    public function getElements()
    {
        $fields  = parent::getInputText("inputtext1");
        $fields .= ", ". parent::getInputText("inputtext2", "6974447733");
        $fields .= ", ". parent::getInputText("inputtext3");
        $fields .= ", ". parent::getInputText("inputtext4", "value");

        $fields .= ", ". parent::getSelect("select1");
        $fields .= ", ". parent::getSelect("select2", true);

        $fields .= ", ". parent::getInputPassword("inputpass1", "value");
        $fields .= ", ". parent::getTextArea("textarea1", "6974447733");
        $fields .= ", ". parent::getInputHidden("inputhidden1", "6974447733");

        $fields .= ", ". parent::getInputText("inputtext5", " ");
        $fields .= ", ". parent::getInputText("inputtext6", "");

        return $fields;
    }

    public function testValidator()
    {
        $errors = self::getForm(self::getElements(), self::getValidators());

        $this->assertEquals(3, count($errors));

        $this->assertEquals(forms\FormError::ERROR_MATCHING_REGEX, $errors[0]->getCode());
        $this->assertEquals("inputpass1", $errors[0]->getControl()->getName());
        $payload = json_decode($errors[0]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals(10, $payload["errorno"]);
        $this->assertEquals("This is not a valid telephone number.", $payload["errordesc"]);

        $this->assertEquals(forms\FormError::ERROR_MATCHING_REGEX, $errors[1]->getCode());
        $this->assertEquals("inputtext5", $errors[1]->getControl()->getName());
        $this->assertNull($errors[1]->getPayload());

        $this->assertEquals(forms\FormError::ERROR_FIELD_NOT_FOUND, $errors[2]->getCode());
        $this->assertEquals("inputtext7", json_decode($errors[2]->getPayload(), true)["field"]);
    }
}
