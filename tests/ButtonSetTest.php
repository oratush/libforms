<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  ButtonSetTest.php
 *
 * Test button set functionality
 */

use com\oratush\forms\controls;

class ButtonSetTest extends PHPUnit_Framework_TestCase
{
    private function getButtonSet()
    {
        $json_string = <<<EOT
{
  "buttons": [
   {
    "name": "ok_btn",
    "type": "submit",
    "value": "ok",
    "label": "Ok"
   },
   {
    "name": "cancel_btn",
    "type": "reset",
    "value": "cancel",
    "label": "Cancel"
   }
  ]
 }
EOT;

        $a = new controls\ButtonSet();
        $a->parseJSON(json_decode($json_string, true));

        return $a;
    }
    
    public function testCount()
    {
        $buttonSet = self::getButtonSet();
        $buttons = $buttonSet->getButtons();

        $this->assertEquals(2, count($buttons));
    }

    public function testContents()
    {
        $buttonSet = self::getButtonSet();
        $buttons = $buttonSet->getButtons();

        $allButtons = true;
        for( $i=0; $i<count($buttons); $i++ )
        {
            if ( $buttons[$i]->getControlType()!=controls\AbstractControl::BUTTON )
            {
                $allButtons = false;
                break;
            }
        }

        $this->assertEquals(true, $allButtons);
    }
}
