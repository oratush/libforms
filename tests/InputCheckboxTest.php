<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  InputCheckboxTest.php
 *
 * Test input checkbox functionality
 */

use com\oratush\forms\controls;

class InputCheckboxTest extends PHPUnit_Framework_TestCase
{
    private function getInputCheckBox() {
        $json_string = <<<EOT
{
  "multiline": false,
  "elements": [
    {
      "label": "Greece",
      "value": "GR",
      "selected": true
    },
    {
      "label": "Germany",
      "value": "DE"
    },
    {
      "label": "France",
      "value": "FR"
    }
   ]
 }
EOT;

        $a = new controls\InputCheckbox();
        $a->parseJSON(json_decode($json_string, true));

        return $a;
    }

    public function testInputCheckbox()
    {
        $a = self::getInputCheckBox();

        //Check multiline
        $this->assertEquals(false, $a->isMultiline());
        $a->setMultiline(true);
        $this->assertEquals(true, $a->isMultiline());

        //Check element number
        $this->assertNotNull($a->getElements());
        $this->assertEquals(3, count($a->getElements()));
    }

    public function testInputCheckboxValue()
    {
        $a = self::getInputCheckBox();

        $this->assertEquals(true, $a->hasItemSelected());
        $this->assertEquals("GR", $a->getValue());
    }

    public function testInputCheckboxReset()
    {
        $a = self::getInputCheckBox();
        $a->removeSelections();

        $this->assertEquals(false, $a->hasItemSelected());
    }

    public function testInputCheckboxReselect()
    {
        $a = self::getInputCheckBox();
        $a->removeSelections();
        $a->setSelected("DE");

        $this->assertEquals(true, $a->hasItemSelected());
        $this->assertEquals("DE", $a->getValue());
    }

    public function testInputCheckboxValues()
    {
        $a = self::getInputCheckBox();
        $a->setSelected("DE");

        $this->assertEquals(array("GR", "DE"), $a->getValues());
    }
}
