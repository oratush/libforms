<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  FieldRequiredTest.php
 *
 * Test field required validator functionality
 */

use com\oratush\forms;

class FieldRequiredTest extends ValidatorBase
{
    public function getValidators()
    {
        $validators = <<<EOT
{
    "FieldRequired": {
        "fields": [
            "inputtext1",
            "inputtext2",
            "select1",
            "select2",
            "inputpass1",
            "inputpass2",
            "inputcheck1",
            "inputcheck2",
            "inputradio1",
            "inputradio2",
            "textarea1",
            "textarea2",
            "inputhidden1",
            "inputhidden2",
            "inputcaptcha1",
            "inputcaptcha2",
            "inputfile1",
            "inputfile2",
            "inputfile3",
            "inputfile4",
            "inputfile5",
            "inputtext5",
            "inputtext6"
        ]
    }
}
EOT;

        return $validators;
    }

    public function getElements()
    {
        $fields  = parent::getInputText("inputtext1");
        $fields .= ", ". parent::getInputText("inputtext2", "value");
        $fields .= ", ". parent::getInputText("inputtext3");
        $fields .= ", ". parent::getInputText("inputtext4", "value");

        $fields .= ", ". parent::getSelect("select1");
        $fields .= ", ". parent::getSelect("select2", true);
        $fields .= ", ". parent::getSelect("select3");
        $fields .= ", ". parent::getSelect("select4", true);

        $fields .= ", ". parent::getInputPassword("inputpass1");
        $fields .= ", ". parent::getInputPassword("inputpass2", "value");
        $fields .= ", ". parent::getInputPassword("inputpass3");
        $fields .= ", ". parent::getInputPassword("inputpass4", "value");

        $fields .= ", ". parent::getInputCheckbox("inputcheck1");
        $fields .= ", ". parent::getInputCheckbox("inputcheck2", true);
        $fields .= ", ". parent::getInputCheckbox("inputcheck3");
        $fields .= ", ". parent::getInputCheckbox("inputcheck4", true);

        $fields .= ", ". parent::getInputRadio("inputradio1");
        $fields .= ", ". parent::getInputRadio("inputradio2", true);
        $fields .= ", ". parent::getInputRadio("inputradio3");
        $fields .= ", ". parent::getInputRadio("inputradio4", true);

        $fields .= ", ". parent::getTextArea("textarea1");
        $fields .= ", ". parent::getTextArea("textarea2", "value");
        $fields .= ", ". parent::getTextArea("textarea3");
        $fields .= ", ". parent::getTextArea("textarea4", "value");

        $fields .= ", ". parent::getInputHidden("inputhidden1");
        $fields .= ", ". parent::getInputHidden("inputhidden2", "value");
        $fields .= ", ". parent::getInputHidden("inputhidden3");
        $fields .= ", ". parent::getInputHidden("inputhidden4", "value");

        $fields .= ", ". parent::getCaptcha("inputcaptcha1");
        $fields .= ", ". parent::getCaptcha("inputcaptcha2", "value");
        $fields .= ", ". parent::getCaptcha("inputcaptcha3");
        $fields .= ", ". parent::getCaptcha("inputcaptcha4", "value");

        $fields .= ", ". parent::getInputFile("inputfile1");
        $fields .= ", ". parent::getInputFile("inputfile2");
        $fields .= ", ". parent::getInputFile("inputfile3");
        $fields .= ", ". parent::getInputFile("inputfile4");
        $fields .= ", ". parent::getInputFile("inputfile5");
        $fields .= ", ". parent::getInputFile("inputfile6");

        $fields .= ", ". parent::getInputText("inputtext5", " ");
        $fields .= ", ". parent::getInputText("inputtext6", "");

        return $fields;
    }

    private function getInputFileMultipleControl($name)
    {
        $json_element = <<<EOT
            {
                "InputFile": {
                    "name": "$name",
                    "multiple": true
                }
            }
EOT;

        return $json_element;
    }

    public function getValidatorsMutliFiles()
    {
        $validators = <<<EOT
{
    "FieldRequired": {
        "fields": [
            "inputfile1",
            "inputfile2",
            "inputfile3",
            "inputfile4",
            "inputfile5"
        ]
    }
}
EOT;

        return $validators;
    }
    
    public function getElementsMultiFiles()
    {
        $fields = $this->getInputFileMultipleControl("inputfile1");
        $fields .= ", ". $this->getInputFileMultipleControl("inputfile2");
        $fields .= ", ". $this->getInputFileMultipleControl("inputfile3");
        $fields .= ", ". $this->getInputFileMultipleControl("inputfile4");
        $fields .= ", ". $this->getInputFileMultipleControl("inputfile5");
        $fields .= ", ". $this->getInputFileMultipleControl("inputfile6");

        return $fields;
    }

    public function testValidator()
    {
        $_FILES["inputfile1"]["error"] = 0;
        $_FILES["inputfile2"]["error"] = 2;
        $_FILES["inputfile3"]["error"] = 4;
        $_FILES["inputfile4"]["error"] = 5;

        $errors = self::getForm(self::getElements(), self::getValidators());

        $this->assertEquals(12, count($errors));

        $this->assertEquals(forms\FormError::ERROR_NO_VALUE, $errors[0]->getCode());
        $this->assertEquals("inputtext1", $errors[0]->getControl()->getName());

        $this->assertEquals(forms\FormError::ERROR_NO_SELECTION, $errors[1]->getCode());
        $this->assertEquals("select1", $errors[1]->getControl()->getName());

        $this->assertEquals(forms\FormError::ERROR_NO_VALUE, $errors[2]->getCode());
        $this->assertEquals("inputpass1", $errors[2]->getControl()->getName());

        $this->assertEquals(forms\FormError::ERROR_NO_SELECTION, $errors[3]->getCode());
        $this->assertEquals("inputcheck1", $errors[3]->getControl()->getName());

        $this->assertEquals(forms\FormError::ERROR_NO_SELECTION, $errors[4]->getCode());
        $this->assertEquals("inputradio1", $errors[4]->getControl()->getName());

        $this->assertEquals(forms\FormError::ERROR_NO_VALUE, $errors[5]->getCode());
        $this->assertEquals("textarea1", $errors[5]->getControl()->getName());

        $this->assertEquals(forms\FormError::ERROR_NO_VALUE, $errors[6]->getCode());
        $this->assertEquals("inputhidden1", $errors[6]->getControl()->getName());

        $this->assertEquals(forms\FormError::ERROR_FILE_MAX_SIZE_LIMIT, $errors[7]->getCode());
        $this->assertEquals("inputfile2", $errors[7]->getControl()->getName());
        $this->assertEquals(forms\FormError::ERROR_FILE_NOT_SELECTED, $errors[8]->getCode());
        $this->assertEquals("inputfile3", $errors[8]->getControl()->getName());
        $this->assertEquals(forms\FormError::ERROR_FILE_UPLOAD_ERROR, $errors[9]->getCode());
        $this->assertEquals("inputfile4", $errors[9]->getControl()->getName());
        $this->assertEquals(forms\FormError::ERROR_FILE_BAD_FORM_TYPE, $errors[10]->getCode());
        $this->assertEquals("inputfile5", $errors[10]->getControl()->getName());

        $this->assertEquals(forms\FormError::ERROR_NO_VALUE, $errors[11]->getCode());
        $this->assertEquals("inputtext6", $errors[11]->getControl()->getName());
    }

    public function testMultipleFiles()
    {
        $_FILES["inputfile1"]["error"][0] = 0;
        $_FILES["inputfile1"]["error"][1] = 0;
        $_FILES["inputfile2"]["error"][0] = 2;
        $_FILES["inputfile2"]["error"][1] = 0;
        $_FILES["inputfile3"]["error"][0] = 0;
        $_FILES["inputfile3"]["error"][1] = 4;
        $_FILES["inputfile4"]["error"][0] = 5;
        $_FILES["inputfile4"]["error"][1] = 0;

        $errors = self::getForm(self::getElementsMultiFiles(), self::getValidatorsMutliFiles());

        $this->assertEquals(4, count($errors));

        $this->assertEquals(forms\FormError::ERROR_FILE_MAX_SIZE_LIMIT, $errors[0]->getCode());
        $this->assertEquals("inputfile2", $errors[0]->getControl()->getName());
        $this->assertEquals(forms\FormError::ERROR_FILE_NOT_SELECTED, $errors[1]->getCode());
        $this->assertEquals("inputfile3", $errors[1]->getControl()->getName());
        $this->assertEquals(forms\FormError::ERROR_FILE_UPLOAD_ERROR, $errors[2]->getCode());
        $this->assertEquals("inputfile4", $errors[2]->getControl()->getName());
        $this->assertEquals(forms\FormError::ERROR_FILE_BAD_FORM_TYPE, $errors[3]->getCode());
        $this->assertEquals("inputfile5", $errors[3]->getControl()->getName());
    }
}
