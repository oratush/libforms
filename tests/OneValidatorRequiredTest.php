<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  OneValidatorRequiredTest.php
 *
 * Test one validator required functionality
 */

use com\oratush\forms;

class OneValidatorRequiredTest extends ValidatorBase
{
    public function getValidators()
    {
        $validators = <<<EOT
{
    "OneValidatorRequired": {
        "validators": [
            {
                "ValidEmail": {
                    "fields": [
                        "inputtext1",
                        "inputtext2"
                    ]
                }
            },
            {
                "ValidNumber": {
                    "fields": [
                        "inputtext1",
                        "inputtext2"
                    ]
                }
            }
        ]
    }
}
EOT;

        return $validators;
    }

    public function getElements1()
    {
        $fields  = parent::getInputText("inputtext1");
        $fields .= ", ". parent::getInputText("inputtext2", "email@example.com");

        return $fields;
    }

    public function getElements2()
    {
        $fields  = parent::getInputText("inputtext1");
        $fields .= ", ". parent::getInputText("inputtext2", "314");

        return $fields;
    }

    public function getElements3()
    {
        $fields  = parent::getInputText("inputtext1");
        $fields .= ", ". parent::getInputText("inputtext2", "oratush");

        return $fields;
    }

    public function testValidator1()
    {
        $errors = self::getForm(self::getElements1(), self::getValidators());

        $this->assertEquals(0, count($errors));
    }

    public function testValidator2()
    {
        $errors = self::getForm(self::getElements2(), self::getValidators());

        $this->assertEquals(0, count($errors));
    }

    public function testValidator3()
    {
        $errors = self::getForm(self::getElements3(), self::getValidators());

        $this->assertEquals(1, count($errors));
        $this->assertEquals(forms\FormError::ERROR_INVALID_EMAIL, $errors[0]->getCode());
        $this->assertEquals("inputtext2", $errors[0]->getControl()->getName());
    }
}
