<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  TextAreaTest.php
 *
 * Test text area functionality
 */

use com\oratush\forms\controls;

class TextAreaTest extends PHPUnit_Framework_TestCase
{
    public function testTextArea()
    {
        $readonly = true;
        $help = "This is a help text.";
        $placeholder = "Enter text";
        $cols = 10;
        $rows = 20;
        $wrap = "solf";
        $maxlength = 50;

        if ( $readonly )
            $readonly_text = "true";
        else
            $readonly_text = "false";

        $json_string = <<<EOT
{
    "cols": $cols,
    "rows": $rows,
    "wrap": "$wrap",
    "maxlength": $maxlength,
    "helptext": "$help",
    "placeholder": "$placeholder",
    "readonly": $readonly_text
 }
EOT;

        $a = new controls\TextArea();
        $a->parseJSON(json_decode($json_string, true));

        $this->assertEquals($readonly, $a->isReadonly());
        $this->assertEquals($help, $a->getHelptext());

        $this->assertEquals($cols, $a->getColumns());
        $this->assertEquals($rows, $a->getRows());
        $this->assertEquals($maxlength, $a->getMaxlength());
        $this->assertEquals($placeholder, $a->getPlaceholder());

        $this->assertNull($a->getWrap());
        $wrap = "soft";
        $a->setWrap($wrap);
        $this->assertEquals($wrap, $a->getWrap());
    }
}
