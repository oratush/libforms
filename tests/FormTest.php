<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  FormTest.php
 *
 * Test form functionality
 */

use com\oratush\forms;
use com\oratush\forms\controls\AbstractControl as AbstractControl;
use com\oratush\forms\validators\AbstractValidator as AbstractValidator;

class FormTest extends ValidatorBase
{
    public function testSettersGetters()
    {
        $id = "form_id";
        $name = "form_name";
        $action = "login.php";
        $method = "get";
        $target = "_parent";
        $enctype = "multipart/form-data";
        $acceptcharset = "UTF-8";
        $trim = true;
        $autocomplete = true;
        $novalidate = true;

        if ( $trim )
            $trim_text = "true";
        else
            $trim_text = "false";
        if ( $autocomplete )
            $autocomplete_text = "true";
        else
            $autocomplete_text = "false";
        if ( $novalidate )
            $novalidate_text = "true";
        else
            $novalidate_text = "false";

        $classes[0] = "btn";
        $classes[1] = "btn-default";
        $attributes["style"] = "padding-left: 20px";
        $attributes["onClick"] = "some js";

        $classes_str = '"'. $classes[0] .'"';
        for($i=1 ; $i<count($classes) ; $i++) {
            $classes_str .= ', "'. $classes[$i] .'"';
        }

        $attribute_keys = array_keys($attributes);
        $attributes_str = '"'. $attribute_keys[0] .'": "'. $attributes[$attribute_keys[0]] .'"';
        for($i=1 ; $i<count($attribute_keys) ; $i++) {
            $attributes_str .= ', "'. $attribute_keys[$i] .'": "'. $attributes[$attribute_keys[$i]] .'"';
        }

        $json_string = <<<EOT
{
    "id": "$id",
    "name": "$name",
    "action": "$action",
    "method": "$method",
    "target": "$target",
    "enctype": "$enctype",
    "accept-charset": "$acceptcharset",
    "trim": $trim_text,
    "autocomplete": $autocomplete_text,
    "novalidate": $novalidate_text,
    "classes": [
        $classes_str
     ],
    "attributes": {
        $attributes_str
     }
 }
EOT;

        $a = new forms\Form();
        $this->assertTrue($a->parseJSON($json_string));

        $this->assertEquals($id, $a->getID());
        $this->assertEquals($name, $a->getName());
        $this->assertEquals($action, $a->getAction());
        $this->assertEquals($method, $a->getMethod());
        $this->assertEquals($target, $a->getTarget());
        $this->assertEquals($enctype, $a->getEncodingType());
        $this->assertEquals($acceptcharset, $a->getAcceptcharset());
        $this->assertEquals($trim, $a->isTrimEnabled());
        $this->assertEquals($autocomplete, $a->isAutocomplete());
        $this->assertEquals(!$novalidate, $a->isValidate());
        $this->assertEquals($classes, $a->getClasses());
        $this->assertEquals($attributes, $a->getAttributes());

        $error = "Error message";
        $a->setTranslatedErrorMessage($error);
        $this->assertEquals($error, $a->getTranslatedErrorMessage());
    }

    public function testBadValues()
    {
        $a = new forms\Form();
        $a->setMethod("oratush");
        $a->setEncodingType("oratush");

        $this->assertEquals(forms\Form::METHOD_POST, $a->getMethod());
        $this->assertEquals(forms\Form::ENCODING_FORM_URLENCODED, $a->getEncodingType());
    }

    public function testElements()
    {
        $input_text = parent::getInputText("username", "oratush");
        $input_pass = parent::getInputPassword("password");
        $textarea = parent::getTextArea("comments");
        $select = parent::getSelect("country", true);

        $json_string = <<<EOT
{
    "name": "oratush",
    "action": "login.php",
    "controls": [
        {
            "Fieldset": {
                "legend": "Unit test",
                "controls": [
                    $input_text,
                    $input_pass
                ]
            }
        },
        $textarea,
        $select
    ]
 }
EOT;
        $a = new forms\Form();
        $this->assertTrue($a->parseJSON($json_string));

        $items = $a->getControls();
        $all_items = $a->getAllControls();
        $sub_items = $items[0]->getControls();

        $this->assertEquals(3, count($items));
        $this->assertEquals(4, count($all_items));
        $this->assertEquals(2, count($sub_items));

        $this->assertEquals(AbstractControl::FIELDSET, $items[0]->getControlType());

        $this->assertEquals(AbstractControl::TEXTAREA, $items[1]->getControlType());
        $this->assertEquals("comments", $items[1]->getName());
        $this->assertEquals(AbstractControl::SELECT, $items[2]->getControlType());
        $this->assertEquals("country", $items[2]->getName());

        $this->assertEquals(AbstractControl::INPUT, $all_items["username"]->getControlType());
        $this->assertEquals(AbstractControl::INPUT, $all_items["password"]->getControlType());
        $this->assertEquals(AbstractControl::INPUT_TEXT, $all_items["username"]->getType());
        $this->assertEquals(AbstractControl::INPUT_PASSWORD, $all_items["password"]->getType());

        $this->assertEquals(AbstractControl::TEXTAREA, $all_items["comments"]->getControlType());
        $this->assertEquals(AbstractControl::SELECT, $all_items["country"]->getControlType());

        $this->assertEquals(AbstractControl::INPUT, $sub_items[0]->getControlType());
        $this->assertEquals("username", $sub_items[0]->getName());
        $this->assertEquals(AbstractControl::INPUT, $sub_items[1]->getControlType());
        $this->assertEquals("password", $sub_items[1]->getName());
        $this->assertEquals(AbstractControl::INPUT_TEXT, $sub_items[0]->getType());
        $this->assertEquals(AbstractControl::INPUT_PASSWORD, $sub_items[1]->getType());
    }

    public function testValidators()
    {
        $json_string = <<<EOT
{
    "name": "oratush",
    "action": "login.php",
    "validators": [
        {
            "FieldRequired": {
                "fields": [
                    "username",
                    "password"
                ]
            }
        },
        {
            "ValidEmail": {
                "fields": [
                    "username",
                    "password"
                ]
            }
        }
    ]
 }
EOT;

        $a = new forms\Form();
        $this->assertTrue($a->parseJSON($json_string));

        $validators = $a->getValidators();

        $this->assertEquals(2, count($validators));
        $this->assertEquals(AbstractValidator::FIELD_REQUIRED, $validators[0]->getType());
        $this->assertEquals(AbstractValidator::VALID_EMAIL, $validators[1]->getType());
    }

    public function testProcessData()
    {
        $json_string = <<<EOT
{
    "name": "oratush",
    "action": "login.php",
    "controls": [
        {
            "Fieldset": {
                "legend": "Unit test",
                "controls": [
                    { "InputText": { "name": "text", "value": "dummytext" } },
                    { "InputPassword": { "name": "pass", "value": "dummypass" } }
                ]
            }
        },
        { "InputHidden": { "name": "hidden", "value": "dummyhidden" } },
        { "TextArea": { "name": "area", "value": "dummyarea" } },
        { "Select": { "name": "select1", "multiple": true, "elements": [
                        {
                            "label": "Greece",
                            "value": "GR",
                            "selected": true
                        },
                        {
                            "label": "Germany",
                            "value": "DE"
                        },
                        {
                            "label": "France",
                            "value": "FR"
                        } ] } },
        { "Select": { "name": "select2", "elements": [
                        {
                            "label": "Germany",
                            "value": "DE",
                            "selected": true
                        },
                        {
                            "label": "France",
                            "value": "FR"
                        } ] } },
        { "InputCheckbox": { "name": "check", "elements": [
                        {
                            "label": "Greece",
                            "value": "GR"
                        },
                        {
                            "label": "Germany",
                            "value": "DE"
                        },
                        {
                            "label": "France",
                            "value": "FR",
                            "selected": true
                        } ] } },
        { "InputRadio": { "name": "radio", "elements": [
                        {
                            "label": "Germany",
                            "value": "DE",
                            "selected": true
                        },
                        {
                            "label": "France",
                            "value": "FR"
                        } ] } },
        { "InputImage": { "name": "image", "src": "dummy.png" } },
        { "InputFile": { "name": "file1", "multiple": false } },
        { "InputFile": { "name": "file2", "multiple": true } }
    ]
 }
EOT;
        $a = new forms\Form();
        $this->assertTrue($a->parseJSON($json_string));

        //Check default
        $values["text"] = "dummytext";
        $values["pass"] = "dummypass";
        $values["hidden"] = "dummyhidden";
        $values["area"] = "dummyarea";
        $values["select1"][0] = "GR";
        $values["select2"] = "DE";
        $values["check"][0] = "FR";
        $values["radio"] = "DE";

        $this->assertEquals(11, count($a->getAllControls()));
        $this->checkForm($a->getAllControls(), $values);
        //--

        //Check 1
        unset($values);
        $values["text"] = "anothertext";
        $values["pass"] = "anotherpass";
        $values["hidden"] = "anotherhidden";
        $values["area"] = "anotherarea";
        $values["select1"][0] = "DE";
        $values["select1"][1] = "FR";
        $values["select2"] = "FR";
        $values["check"][0] = "DE";
        $values["radio"] = "FR";

        //$a->processDataArray($values);
        //$this->checkForm($a->getAllControls(), $values);
        //--

        //Check 2 multi-select checkbox
        $values["check"][1] = "FR";
        //$a->processDataArray($values);
        //$this->checkForm($a->getAllControls(), $values);
        //--

        //Check 3 - spaces no trim enabled
        unset($values);
        $values["text"] = " dummytext ";
        $values["pass"] = " dummypass ";
        $values["hidden"] = " dummyhidden ";
        $values["area"] = " dummyarea ";
        $values["select1"][0] = "GR";
        $values["select2"] = "DE";
        $values["check"][0] = "FR";
        $values["radio"] = "DE";
        //$a->processDataArray($values);
        //$this->checkForm($a->getAllControls(), $values);
        //--

        //Check 4 - spaces trim enabled
        $a->setTrimValues(true);
        //$a->processDataArray($values);

        $values["text"] = trim(" dummytext ");
        $values["hidden"] = trim(" dummyhidden ");
        $values["area"] = trim(" dummyarea ");
        //$this->checkForm($a->getAllControls(), $values);
        //--

        //Check 5 _POST array processData
        unset($values);
        $values["text"] = "anothertext";
        $values["pass"] = "anotherpass";
        $values["hidden"] = "anotherhidden";
        $values["area"] = "anotherarea";
        $values["select1"][0] = "DE";
        $values["select1"][1] = "FR";
        $values["select2"] = "FR";
        $values["check"][0] = "DE";
        $values["radio"] = "FR";
        $_POST = $values;
        //$a->processData();
        //$this->checkForm($a->getAllControls(), $values);
        //--

        //Check 6 file and image input elements
        unset($values);
        $values["image.x"] = "200";
        $values["image.y"] = "100";
        $_FILES["file1"]["name"] = "example.pdf";
        $_FILES["file1"]["tmp_name"] = "tmp.pdf";
        $_FILES["file2"]["name"][0] = "example_new.pdf";
        $_FILES["file2"]["tmp_name"][0] = "tmp_new.pdf";
        $_FILES["file2"]["name"][1] = "example_new1.pdf";
        $_FILES["file2"]["tmp_name"][1] = "tmp_new1.pdf";

        $_POST = $values;
        $a->processData();
        $all_items = $a->getAllControls();
        $this->assertEquals($values["image.x"] .",". $values["image.y"], $all_items["image"]->getValue());
        $this->assertEquals($values["image.x"], $all_items["image"]->getX());
        $this->assertEquals($values["image.y"], $all_items["image"]->getY());

        $files1 = $all_items["file1"]->getFiles();
        $files2 = $all_items["file2"]->getFiles();
        $this->assertEquals(count($_FILES["file1"]["name"]), count($files1));
        $this->assertEquals(count($_FILES["file2"]["name"]), count($files2));
        $this->assertEquals($_FILES["file1"]["name"], $files1[0]["name"]);
        $this->assertEquals($_FILES["file2"]["name"][0], $files2[0]["name"]);
        $this->assertEquals($_FILES["file2"]["name"][1], $files2[1]["name"]);
    }

    private function checkForm($all_items, $values)
    {
        $this->assertEquals($values["text"], $all_items["text"]->getValue());
        $this->assertEquals($values["pass"], $all_items["pass"]->getValue());
        $this->assertEquals($values["hidden"], $all_items["hidden"]->getValue());
        $this->assertEquals($values["area"], $all_items["area"]->getValue());
        $this->assertEquals($values["select1"], $all_items["select1"]->getValues());
        $this->assertEquals($values["select2"], $all_items["select2"]->getValue());
        $this->assertEquals($values["check"], $all_items["check"]->getValues());
        $this->assertEquals($values["radio"], $all_items["radio"]->getValue());
    }

    public function testValid()
    {
        $json_string = <<<EOT
{
    "name": "oratush",
    "action": "login.php",
    "controls": [
        {
            "Fieldset": {
                "legend": "Unit test",
                "controls": [
                    { "InputText": { "name": "text", "value": "dummy" } },
                    { "InputPassword": { "name": "pass" } }
                ]
            }
        }
    ],
    "validators": [
        {
            "FieldRequired": {
                "fields": [
                    "text",
                    "pass"
                ]
            }
        },
        {
            "ValidEmail": {
                "fields": [
                    "text"
                ]
            }
        }
    ]
 }
EOT;

        $a = new forms\Form();
        $this->assertTrue($a->parseJSON($json_string));

        $this->assertEquals(0, count($a->getErrors()));
        $this->assertFalse($a->isValid());
        $errors = $a->getErrors();
        $this->assertEquals(2, count($errors));
        $this->assertEquals(forms\FormError::ERROR_NO_VALUE, $errors[0]->getCode());
        $this->assertEquals(forms\FormError::ERROR_INVALID_EMAIL, $errors[1]->getCode());

        $values["text"] = "you@example.com";
        $values["pass"] = "dummypass";
        $_POST = $values;
        $this->assertNull($a->checkData());
        $this->assertEquals(0, count($a->getErrors()));
        $this->assertTrue($a->isValid());
    }

    public function testBadJSON()
    {
        $json_string = <<<EOT
{
    BAD
 }
EOT;

        $a = new forms\Form();
        $this->assertFalse($a->parseJSON($json_string));
    }
}
