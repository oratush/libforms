<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  InputImageTest.php
 *
 * Test input image functionality
 */

use com\oratush\forms\controls;

class InputImageTest extends PHPUnit_Framework_TestCase
{
    public function testImage()
    {
        $width = 100;
        $height = 200;
        $src = "images/test.png";
        $alt = "Test image";

        $json_string = <<<EOT
{
    "width": $width,
    "height": $height,
    "src": "$src",
    "alt": "$alt"
 }
EOT;
        $coord_x = 10;
        $coord_y = 20;

        $a = new controls\InputImage();
        $a->parseJSON(json_decode($json_string, true));

        $this->assertEquals($width, $a->getWidth());
        $this->assertEquals($height, $a->getHeight());
        $this->assertEquals($src, $a->getSource());
        $this->assertEquals($alt, $a->getAlt());

        //Invalid selection
        $a->setX(-100);
        $a->setX(-200);
        $this->assertEquals(-1, $a->getX());
        $this->assertEquals(-1, $a->getY());

        $a->setX($coord_x);
        $a->setY($coord_y);
        $this->assertEquals($coord_x, $a->getX());
        $this->assertEquals($coord_y, $a->getY());
    }
}
