<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  BaseControlTest.php
 *
 * Test input functionality
 */

use com\oratush\forms\controls;

class BaseControlTest extends PHPUnit_Framework_TestCase
{
    public function testBaseControl()
    {
        $id = "12345";
        $name = "control";
        $value = "some value";
        $label = "The label:";
        $autofocus = true;
        $disabled = false;
        $classes[0] = "btn";
        $classes[1] = "btn-default";
        $attributes["style"] = "padding-left: 20px";
        $attributes["onClick"] = "some js";

        if ( $autofocus )
            $autofocus_text = "true";
        else
            $autofocus_text = "false";
        if ( $disabled )
            $disabled_text = "true";
        else
            $disabled_text = "false";

        $classes_str = '"'. $classes[0] .'"';
        for($i=1 ; $i<count($classes) ; $i++) {
            $classes_str .= ', "'. $classes[$i] .'"';
        }

        $attribute_keys = array_keys($attributes);
        $attributes_str = '"'. $attribute_keys[0] .'": "'. $attributes[$attribute_keys[0]] .'"';
        for($i=1 ; $i<count($attribute_keys) ; $i++) {
            $attributes_str .= ', "'. $attribute_keys[$i] .'": "'. $attributes[$attribute_keys[$i]] .'"';
        }

        $json_string = <<<EOT
{
    "id": "$id",
    "name": "$name",
    "value": "$value",
    "label": "$label",
    "autofocus": $autofocus_text,
    "disabled": $disabled_text,
    "classes": [
        $classes_str
     ],
    "attributes": {
        $attributes_str
     }
 }
EOT;

        $a = new controls\BaseControl();
        $this->assertEquals(false, $a->isAutofocus());
        $this->assertEquals(false, $a->isDisabled());

        $a->parseJSON(json_decode($json_string, true));

        $this->assertEquals($id, $a->getID());
        $this->assertEquals($name, $a->getName());
        $this->assertEquals($value, $a->getValue());
        $this->assertEquals($label, $a->getLabel());
        $this->assertEquals($autofocus, $a->isAutofocus());
        $this->assertEquals($disabled, $a->isDisabled());
        $this->assertEquals($classes, $a->getClasses());
        $this->assertEquals($attributes, $a->getAttributes());
    }
}
