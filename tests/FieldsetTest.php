<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  FieldsetTest.php
 *
 * Test Fieldset functionality
 */

use com\oratush\forms;

class FieldsetTest extends PHPUnit_Framework_TestCase
{
    public function testBasic()
    {
        $id = "unique";
        $disabled = true;
        $legend = "Title";

        if ($disabled) {
            $disabled_text = "true";
        }
        else {
            $disabled_text = "false";
        }

        $json_string = <<<EOT
{
    "id": "$id",
    "legend": "$legend",
    "disabled": $disabled_text
 }
EOT;

        $a = new forms\Fieldset();

        $this->assertFalse($a->isDisabled());

        $a->parseJSON(json_decode($json_string, true));

        $this->assertEquals($id, $a->getID());
        $this->assertEquals($legend, $a->getLegend());
        $this->assertEquals($disabled, $a->isDisabled());
    }

    public function testElements()
    {
        $json_string = <<<EOT
{
    "controls": [
            {
              "InputText": {
                  "name": "username"
                }
            },
            {
              "InputPassword": {
                  "name": "password"
                }
            },
            {
              "InputFile": {
                  "name": "file"
                }
            }
     ]
 }
EOT;

        $a = new forms\Fieldset();
        $a->parseJSON(json_decode($json_string, true));

        $elements = $a->getControls();

        $this->assertEquals(3, count($elements));
        $this->assertEquals("username", $elements[0]->getName());
        $this->assertEquals("password", $elements[1]->getName());
        $this->assertEquals("file", $elements[2]->getName());
    }
}
