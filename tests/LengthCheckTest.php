<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  LengthCheckTest.php
 *
 * Test length validator functionality
 */

use com\oratush\forms;

class LengthCheckTest extends ValidatorBase
{
    public function getValidators()
    {
        $validators = <<<EOT
{
    "LengthCheck": {
        "fields": [
            { "name": "inputtext1", "min": 2, "max": 10 },
            { "name": "inputtext2", "min": 2, "max": 10 },
            { "name": "inputtext3", "min": 2, "max": 10 },

            { "name": "inputtext2", "min": 2 },
            { "name": "inputtext1", "max": 10 },

            { "name": "inputtext3", "min": 5, "max": 5 },

            { "name": "inputtext1", "min": 2 },
            { "name": "inputtext2", "max": 10 },

            { "name": "inputtext1", "min": 1 },
            { "name": "inputtext2", "max": 11 }
        ]
    }
}
EOT;

        return $validators;
    }

    public function getElements()
    {
        $fields  = parent::getInputText("inputtext1", "a");
        $fields .= ", ". parent::getInputText("inputtext2", "12345678901");
        $fields .= ", ". parent::getInputText("inputtext3", "value");

        return $fields;
    }

    public function testValidator()
    {
        $errors = self::getForm(self::getElements(), self::getValidators());

        $this->assertEquals(4, count($errors));

        $this->assertEquals(forms\FormError::ERROR_LENGTH_TOO_SMALL, $errors[0]->getCode());
        $this->assertEquals("inputtext1", $errors[0]->getControl()->getName());
        $payload = json_decode($errors[0]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals(1, $payload["length"]);
        $this->assertEquals(2, $payload["min_length"]);

        $this->assertEquals(forms\FormError::ERROR_LENGTH_OVERFLOW, $errors[1]->getCode());
        $this->assertEquals("inputtext2", $errors[1]->getControl()->getName());
        $payload = json_decode($errors[1]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals(11, $payload["length"]);
        $this->assertEquals(10, $payload["max_length"]);

        $this->assertEquals(forms\FormError::ERROR_LENGTH_TOO_SMALL, $errors[2]->getCode());
        $this->assertEquals("inputtext1", $errors[2]->getControl()->getName());
        $payload = json_decode($errors[2]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals(1, $payload["length"]);
        $this->assertEquals(2, $payload["min_length"]);

        $this->assertEquals(forms\FormError::ERROR_LENGTH_OVERFLOW, $errors[3]->getCode());
        $this->assertEquals("inputtext2", $errors[3]->getControl()->getName());
        $payload = json_decode($errors[3]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals(11, $payload["length"]);
        $this->assertEquals(10, $payload["max_length"]);
    }
}
