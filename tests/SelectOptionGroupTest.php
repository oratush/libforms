<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  SelectOptionGroupTest.php
 *
 * Test select option functionality
 */

use com\oratush\forms\controls;

class SelectOptionGroupTest extends PHPUnit_Framework_TestCase
{
    public function testSelectOptionGroup()
    {
        $disabled = true;
        $label = "Greece";

        if ( $disabled )
            $disabled_text = "true";
        else
            $disabled_text = "false";

        $json_string = <<<EOT
{
    "label": "$label",
    "disabled": $disabled_text,
    "values": [
            {
                "label": "Elem1",
                "value": "EL1"
            },
            {
                "label": "Elem2",
                "value": "EL2"
            }
        ]
 }
EOT;

        $a = new controls\SelectOptionGroup();
        $a->parseJSON(json_decode($json_string, true));

        $this->assertEquals($label, $a->getLabel());
        $this->assertEquals($disabled, $a->isDisabled());

        $this->assertNotNull($a->getElements());
        $this->assertEquals(2, count($a->getElements()));
    }
}
