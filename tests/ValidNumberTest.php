<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  ValidNumberTest.php
 *
 * Test number validator functionality
 */

use com\oratush\forms;

class ValidNumberTest extends ValidatorBase
{
    public function getValidators()
    {
        $validators = <<<EOT
{
    "ValidNumber": {
        "fields": [
            "inputtext1",
            "inputtext2",
            "inputtext3",
            "inputtext4",
            "inputtext5",
            "inputtext6",
            "inputtext8",
            "inputtext9"
        ]
    }
}
EOT;

        return $validators;
    }

    public function getElements()
    {
        $fields  = parent::getInputText("inputtext1");
        $fields .= ", ". parent::getInputText("inputtext2", "3");
        $fields .= ", ". parent::getInputPassword("inputtext3", "+3");
        $fields .= ", ". parent::getInputHidden("inputtext4", "-3");
        $fields .= ", ". parent::getInputText("inputtext5", "3.14");
        $fields .= ", ". parent::getInputPassword("inputtext6", "value");
        $fields .= ", ". parent::getInputText("inputtext7", "value");
        $fields .= ", ". parent::getInputText("inputtext8", "");
        $fields .= ", ". parent::getInputHidden("inputtext9", " ");

        return $fields;
    }

    public function testValidator()
    {
        $errors = self::getForm(self::getElements(), self::getValidators());

        $this->assertEquals(3, count($errors));

        $this->assertEquals(forms\FormError::ERROR_INVALID_NUMBER, $errors[0]->getCode());
        $this->assertEquals("inputtext5", $errors[0]->getControl()->getName());
        $this->assertEquals(forms\FormError::ERROR_INVALID_NUMBER, $errors[1]->getCode());
        $this->assertEquals("inputtext6", $errors[1]->getControl()->getName());
        $this->assertEquals(forms\FormError::ERROR_INVALID_NUMBER, $errors[2]->getCode());
        $this->assertEquals("inputtext9", $errors[2]->getControl()->getName());
    }
}
