<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  InputTextTest.php
 *
 * Test input text functionality
 */

use com\oratush\forms\controls;

class InputTextTest extends PHPUnit_Framework_TestCase
{
    public function testInputText()
    {
        $placeholder = "Enter username";
        $autocomplete = true;
        $size = 10;
        $maxlength = 100;

        if ( $autocomplete )
            $autocomplete_text = "true";
        else
            $autocomplete_text = "false";

        $json_string = <<<EOT
{
    "placeholder": "$placeholder",
    "autocomplete": $autocomplete_text,
    "size": $size,
    "maxlength": $maxlength
 }
EOT;

        $a = new controls\InputText();
        $a->parseJSON(json_decode($json_string, true));

        $this->assertEquals($placeholder, $a->getPlaceholder());
        $this->assertEquals($autocomplete, $a->isAutocomplete());
        $this->assertEquals($size, $a->getSize());
        $this->assertEquals($maxlength, $a->getMaxlength());
    }
}
