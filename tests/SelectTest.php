<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  SelectTest.php
 *
 * Test select functionality
 */

use com\oratush\forms\controls;

class SelectTest extends PHPUnit_Framework_TestCase
{
    private function getSelect($multiple, $size, $helptext) {
        if ( $multiple==true )
            $multiple_text = "true";
        else
            $multiple_text = "false";

        $json_string = <<<EOT
{
  "multiple": $multiple_text,
  "size": $size,
  "helptext": "$helptext",
  "elements": [
    {
      "label": "Greece",
      "value": "GR",
      "selected": true
    },
    {
      "label": "Germany",
      "value": "DE"
    },
    {
      "label": "America",
      "disabled": true,
      "values": [
        {
          "label": "United Stated",
          "value": "US"
        },
        {
          "label": "Canada",
          "value": "CA"
        }
       ]
    },
    {
      "label": "France",
      "value": "FR"
    }
   ]
 }
EOT;

        $a = new controls\Select();
        $a->parseJSON(json_decode($json_string, true));

        return $a;
    }

    public function testSelect()
    {
        $multiple = true;
        $size = 2;
        $helptext = "This is a help text.";
        $a = self::getSelect($multiple, $size, $helptext);

        $this->assertEquals($size, $a->getSize());
        $this->assertEquals($multiple, $a->isMultiple());
        $this->assertEquals($helptext, $a->getHelptext());

        $this->assertNotNull($a->getElements());
        $this->assertEquals(4, count($a->getElements()));
    }

    public function testEmptySelect()
    {
        $a = new controls\Select();

        $this->assertEquals(1, $a->getSize());
        $this->assertEquals(controls\AbstractControl::SELECT, $a->getControlType());
        $this->assertEquals(false, $a->isMultiple());
        $this->assertNull($a->getHelptext());
        $this->assertNull($a->getElements());
    }

    public function getSelectWithValues()
    {
        $multiple = true;
        $size = 2;
        $helptext = "This is a help text.";
        $a = self::getSelect($multiple, $size, $helptext);

        return $a;
    }

    public function testSelectValue()
    {
        $a = self::getSelectWithValues();

        $this->assertEquals(true, $a->hasItemSelected());
        $this->assertEquals("GR", $a->getValue());
    }

    public function testSelectReset()
    {
        $a = self::getSelectWithValues();
        $a->removeSelections();

        $this->assertEquals(false, $a->hasItemSelected());
    }

    public function testSelectReselect()
    {
        $a = self::getSelectWithValues();
        $a->removeSelections();
        $a->setSelected("DE");

        $this->assertEquals(true, $a->hasItemSelected());
        $this->assertEquals("DE", $a->getValue());
    }

    public function testSelectValues()
    {
        $a = self::getSelectWithValues();
        $a->setSelected("DE");

        $this->assertEquals(array("GR", "DE"), $a->getValues());
    }

    public function testSelectReselect2()
    {
        $a = self::getSelectWithValues();
        $a->removeSelections();
        $a->setSelected("US");

        $this->assertEquals(true, $a->hasItemSelected());
        $this->assertEquals("US", $a->getValue());
    }

    public function testSelectValues2()
    {
        $a = self::getSelectWithValues();
        $a->setSelected("US");

        $this->assertEquals(array("GR", "US"), $a->getValues());
    }

    public function testSelectValuesNotMulti()
    {
        $multiple = false;
        $size = 2;
        $helptext = "This is a help text.";
        $a = self::getSelect($multiple, $size, $helptext);
        $a->setSelected("US");

        $this->assertNotEquals(array("GR", "US"), $a->getValues());
    }
}
