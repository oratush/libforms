<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  CheckCredentialsTest.php
 *
 * Test CheckCredentials validator functionality, requires database
 */

use com\oratush\forms;

class CheckCredentialsTest extends ValidatorBase
{
    public function getValidators1()
    {
        $validators = <<<EOT
{
    "CheckCredentials": {
        "usr": "inputtext1",
        "pwd": "inputpass1",
        "dbc": "db_conn",
        "query": "SELECT password FROM users WHERE username=?"
    }
},
{
    "CheckCredentials": {
        "usr": "inputtext2",
        "pwd": "inputpass1",
        "dbc": "db_conn",
        "query": "SELECT password FROM users WHERE username=?"
    }
},
{
    "CheckCredentials": {
        "usr": "inputtext1",
        "pwd": "inputpass2",
        "dbc": "db_conn",
        "query": "SELECT password FROM users WHERE username=?"
    }
},
{
    "CheckCredentials": {
        "usr": "inputtext2",
        "pwd": "inputpass2",
        "dbc": "db_conn",
        "query": "SELECT password FROM users WHERE username=?"
    }
},
{
    "CheckCredentials": {
        "usr": "inputtext1",
        "pwd": "inputpass1",
        "dbc": "db_conn",
        "query": "BAD QUERY"
    }
},
{
    "CheckCredentials": {
        "usr": "inputtext1",
        "pwd": "inputpass1",
        "dbc": "db_conn",
        "query": "SELECT user_id FROM users WHERE username='dummy' AND username=?"
    }
},
{
    "CheckCredentials": {
        "usr": "inputtext3",
        "pwd": "inputpass3",
        "dbc": "db_conn",
        "query": "SELECT password FROM users WHERE username=?"
    }
}
EOT;

        return $validators;
    }

    public function getValidators2()
    {
        $validators = <<<EOT
{
    "CheckCredentials": {
        "usr": "inputtext1",
        "pwd": "inputpass1",
        "dbc": "db_conn",
        "query": "SELECT password FROM users WHERE username=?"
    }
},
{
    "CheckCredentials": {
        "usr": "inputtext2",
        "pwd": "inputpass1",
        "dbc": "db_conn",
        "query": "SELECT password FROM users WHERE username=?"
    }
},
{
    "CheckCredentials": {
        "usr": "inputtext1",
        "pwd": "inputpass2",
        "dbc": "db_conn",
        "query": "SELECT password FROM users WHERE username=?"
    }
},
{
    "CheckCredentials": {
        "usr": "inputtext2",
        "pwd": "inputpass2",
        "dbc": "db_conn",
        "query": "SELECT password FROM users WHERE username=?"
    }
},
{
    "CheckCredentials": {
        "usr": "inputtext3",
        "pwd": "inputpass3",
        "dbc": "db_conn",
        "query": "SELECT password FROM users WHERE username=?"
    }
}
EOT;

        return $validators;
    }

    public function getElements1()
    {
        $fields  = parent::getInputText("inputtext1", "oratush");
        $fields .= ", ". parent::getInputPassword("inputpass1", "oratush");
        $fields .= ", ". parent::getInputText("inputtext2");
        $fields .= ", ". parent::getInputPassword("inputpass2");
        $fields .= ", ". parent::getInputText("inputtext3", "oratush");
        $fields .= ", ". parent::getInputPassword("inputpass3", "secret");

        return $fields;
    }

    public function getElements2()
    {
        $fields  = parent::getInputText("inputtext1", "oratush");
        $fields .= ", ". parent::getInputPassword("inputpass1", "oratush");
        $fields .= ", ". parent::getInputHidden("inputtext3", "oratush");
        $fields .= ", ". parent::getInputPassword("inputpass3", "oratush");

        return $fields;
    }

    public function testValidator()
    {
        $db_conn = new PDO('mysql:host=localhost;dbname=oratush;charset=utf8', 'oratush', 'oratush');
        $db_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $GLOBALS["db_conn"] = $db_conn;

        $errors = self::getForm(self::getElements1(), self::getValidators1());

        $this->assertEquals(3, count($errors));

        $this->assertEquals(forms\FormError::ERROR_DB_BAD_CREDENTIALS, $errors[0]->getCode());
        $payload = json_decode($errors[0]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertStringStartsWith("Database exception: ", $payload["message"]);

        $this->assertEquals(forms\FormError::ERROR_DB_BAD_CREDENTIALS, $errors[1]->getCode());
        $payload = json_decode($errors[1]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertStringStartsWith("Query must return a row!", $payload["message"]);

        $this->assertEquals(forms\FormError::ERROR_DB_BAD_CREDENTIALS, $errors[2]->getCode());
        $payload = json_decode($errors[2]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals("Failed to validate the password.", $payload["message"]);
    }

    public function testNoDBValidator()
    {
        $errors = self::getForm(self::getElements2(), self::getValidators2());

        $this->assertEquals(5, count($errors));

        $this->assertEquals(forms\FormError::ERROR_DB_BAD_CREDENTIALS, $errors[0]->getCode());
        $fields = $errors[0]->getControls();
        $this->assertEquals("inputtext1", $fields[0]->getName());
        $this->assertEquals("inputpass1", $fields[1]->getName());
        $payload = json_decode($errors[0]->getPayload(), true);
        $this->assertNotNull($payload);
        $this->assertEquals("Database variable not found in GLOBALS array: db_conn", $payload["message"]);

        $this->assertEquals(forms\FormError::ERROR_FIELD_NOT_FOUND, $errors[1]->getCode());
        $this->assertEquals("inputtext2", json_decode($errors[1]->getPayload(), true)["field"]);

        $this->assertEquals(forms\FormError::ERROR_FIELD_NOT_FOUND, $errors[2]->getCode());
        $this->assertEquals("inputpass2", json_decode($errors[2]->getPayload(), true)["field"]);

        $this->assertEquals(forms\FormError::ERROR_FIELD_NOT_FOUND, $errors[3]->getCode());
        $this->assertEquals("inputtext2", json_decode($errors[3]->getPayload(), true)["field"]);
        $this->assertEquals(forms\FormError::ERROR_FIELD_NOT_FOUND, $errors[4]->getCode());
        $this->assertEquals("inputpass2", json_decode($errors[4]->getPayload(), true)["field"]);
    }
}
