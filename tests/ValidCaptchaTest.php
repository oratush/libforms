<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  ValidCaptchaTest.php
 *
 * Test captcha validator functionality
 */

use com\oratush\forms;

class ValidCaptchaTest extends ValidatorBase
{
    public function getValidators()
    {
        $validators = <<<EOT
{
    "ValidCaptcha": {
        "privatekey": "your_private_key"
    }
}
EOT;

        return $validators;
    }

    public function testCaptcha()
    {
        $fields = parent::getCaptcha("input1", "value1");

        //$_SERVER["REMOTE_ADDR"] = "198.51.100.10";
        //$_POST["recaptcha_challenge_field"] = "the_challenge";
        //$_POST["recaptcha_response_field"] = "the_response";

        $errors = self::getForm($fields, self::getValidators());

        $this->assertEquals(1, count($errors));

        $this->assertEquals(forms\FormError::ERROR_INVALID_CAPTCHA, $errors[0]->getCode());
        $this->assertEquals("input1", $errors[0]->getControl()->getName());
    }

    public function testNoCaptcha()
    {
        $fields = parent::getInputText("input1", "value1");

        $errors = self::getForm($fields, self::getValidators());

        $this->assertEquals(0, count($errors));
    }
}
