<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  InputFileTest.php
 *
 * Test input file functionality
 */

use com\oratush\forms\controls;

class InputFileTest extends PHPUnit_Framework_TestCase
{
    public function testInputFile()
    {
        $multiple = true;
        $accept = "image/*";

        if ( $multiple )
            $multiple_text = "true";
        else
            $multiple_text = "false";

        $json_string = <<<EOT
{
  "name": "file",
  "multiple": $multiple_text,
  "accept": "$accept"
 }
EOT;

        $a = new controls\InputFile();
        $a->parseJSON(json_decode($json_string, true));

        $this->assertEquals($multiple, $a->isMultiple());
        $this->assertEquals($accept, $a->getAccept());

        //Single file
        unset($_FILES);
        $_FILES["file"]["name"] = "example.pdf";
        $_FILES["file"]["tmp_name"] = "example.pdf";

        $a->setMultiple(false);
        $a->processFilesArray();
        $files = $a->getFiles();
        $this->assertEquals($_FILES["file"]["name"], $files[0]["name"]);
        $this->assertEquals($_FILES["file"]["tmp_name"], $files[0]["tmp_name"]);
        $this->assertEquals($_FILES["file"]["name"], $a->getValue());

        //Multi file
        unset($_FILES);
        $_FILES["file"]["name"][0] = "example.pdf";
        $_FILES["file"]["tmp_name"][0] = "temp.pdf";
        $_FILES["file"]["name"][1] = "example2.pdf";
        $_FILES["file"]["tmp_name"][1] = "temp2.pdf";

        $a->setMultiple(true);
        $a->processFilesArray();
        $files = $a->getFiles();
        $this->assertEquals($_FILES["file"]["name"][0], $files[0]["name"]);
        $this->assertEquals($_FILES["file"]["tmp_name"][0], $files[0]["tmp_name"]);
        $this->assertEquals($_FILES["file"]["name"][1], $files[1]["name"]);
        $this->assertEquals($_FILES["file"]["tmp_name"][1], $files[1]["tmp_name"]);
        $this->assertEquals($_FILES["file"]["name"][0], $a->getValue());
    }
}
