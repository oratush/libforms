<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  CaptchaTest.php
 *
 * Test captcha functionality
 */

use com\oratush\forms\controls;

class CaptchaTest extends PHPUnit_Framework_TestCase
{
    public function testCatcha()
    {
        $pubkey = "your_public_key";
        $help = "This is a help text.";

        $json_string = <<<EOT
{
  "label": "Type the words:",
  "publickey": "$pubkey",
  "version": 1,
  "helptext": "$help"
 }
EOT;

        $a = new controls\Captcha();
        $a->parseJSON(json_decode($json_string, true));

        $this->assertEquals($pubkey, $a->getPublic());
        $this->assertEquals($help, $a->getHelptext());
        $this->assertEquals(1, $a->getVersion());
    }
}
