<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  OneFieldRequiredTest.php
 *
 * Test one field required validator functionality
 */

use com\oratush\forms;

class OneFieldRequiredTest extends ValidatorBase
{
    public function getValidators()
    {
        $validators = <<<EOT
{
    "OneFieldRequired": {
        "fields": [
            "inputtext1",
            "inputtext2",
            "inputtext3",
            "inputtext4"
        ]
    }
}
EOT;

        return $validators;
    }

    public function getBadValidators()
    {
        $validators = <<<EOT
{
    "OneFieldRequired": {
        "fields": [
            "inputtext1",
            "inputtext2",
            "inputtext3",
            "inputtext4",
            "inputtext5"
        ]
    }
}
EOT;

        return $validators;
    }

    public function getEmptyElements()
    {
        $fields  = parent::getInputText("inputtext1");
        $fields .= ", ". parent::getInputText("inputtext2");
        $fields .= ", ". parent::getInputText("inputtext3");
        $fields .= ", ". parent::getInputText("inputtext4");

        return $fields;
    }

    public function getElements()
    {
        $fields  = parent::getInputText("inputtext1");
        $fields .= ", ". parent::getInputText("inputtext2");
        $fields .= ", ". parent::getInputText("inputtext3", "value");
        $fields .= ", ". parent::getInputText("inputtext4");

        return $fields;
    }

    public function testValidator()
    {
        $errors = self::getForm(self::getElements(), self::getValidators());

        $this->assertEquals(0, count($errors));
    }

    public function testBadValidator()
    {
        $errors = self::getForm(self::getEmptyElements(), self::getBadValidators());

        $this->assertEquals(2, count($errors));

        $this->assertEquals(forms\FormError::ERROR_FIELD_NOT_FOUND, $errors[0]->getCode());
        $payload = json_decode($errors[0]->getPayload(), true);
        $this->assertEquals("inputtext5", $payload["field"]);
        
        $this->assertEquals(forms\FormError::ERROR_NO_GROUP_VALUE, $errors[1]->getCode());
        $fields = $errors[1]->getControls();
        $this->assertEquals(4, count($fields));

        $this->assertEquals("inputtext1", $fields[0]->getName());
        $this->assertEquals("inputtext2", $fields[1]->getName());
        $this->assertEquals("inputtext3", $fields[2]->getName());
        $this->assertEquals("inputtext4", $fields[3]->getName());
    }
}
