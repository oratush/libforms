<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @file
 *  MatchPasswordsTest.php
 *
 * Test if passwords match validator functionality
 */

use com\oratush\forms;

class MatchPasswordsTest extends ValidatorBase
{
    public function getValidators()
    {
        $validators = <<<EOT
{
    "MatchPasswords": {
        "pwd1": "input1",
        "pwd2": "input2"
    }
}
EOT;

        return $validators;
    }

    public function testNoMatch()
    {
        $fields  = parent::getInputText("input1", "value1");
        $fields .= ", ". parent::getInputText("input2", "value2");

        $errors = self::getForm($fields, self::getValidators());

        $this->assertEquals(1, count($errors));

        $this->assertEquals(forms\FormError::ERROR_MATCHING_PASSWORDS, $errors[0]->getCode());
        $this->assertEquals("input1", $errors[0]->getControls()[0]->getName());
        $this->assertEquals("input2", $errors[0]->getControls()[1]->getName());
    }

    public function testMatch()
    {
        $fields  = parent::getInputPassword("input1", "value1");
        $fields .= ", ". parent::getInputPassword("input2", "value1");

        $errors = self::getForm($fields, self::getValidators());

        $this->assertEquals(0, count($errors));
    }

    public function testBadField()
    {
        $fields  = parent::getInputText("input1", "value1");
        $fields .= ", ". parent::getInputText("inputbad2", "value2");

        $errors = self::getForm($fields, self::getValidators());

        $this->assertEquals(1, count($errors));

        $this->assertEquals(forms\FormError::ERROR_FIELD_NOT_FOUND, $errors[0]->getCode());
        $this->assertEquals("input2", json_decode($errors[0]->getPayload(), true)["field"]);
    }

    public function testBadFields()
    {
        $fields  = parent::getSelect("input1", true);
        $fields .= ", ". parent::getSelect("input2");

        $errors = self::getForm($fields, self::getValidators());

        $this->assertEquals(0, count($errors));
    }
}
