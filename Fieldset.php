<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms
 * @file
 *  Fieldset.php
 *
 * Fieldset class defines the object for fieldsets.
 *
 * Example JSON input :
 * {
 *    "id": "12345",
 *    "disabled": false,
 *    "legend": "Form legend",
 *    "controls": {
 *      //See form
 *    }
 * }
 */

namespace com\oratush\forms;
use com\oratush\forms\controls\AbstractControl as AbstractControl;

class Fieldset extends AbstractControl
{
    private $id;
    private $disabled;
    private $legend;
    private $controls;

    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        $this->id = null;
        $this->disabled = null;
        $this->legend = null;
        $this->controls = null;

        $this->control = AbstractControl::FIELDSET;
    }

    /**
     * Get the fieldset ID.
     *
     * @access public
     *
     * @return string|null
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * Set the ID of this fieldset.
     *
     * @access public
     *
     * @param $id string the ID.
     */
    public function setID($id)
    {
        $this->id = $id;
    }

    /**
     * Check if the fieldset is disabled or not.
     *
     * @access public
     *
     * @return boolean
     */
    public function isDisabled()
    {
        if ($this->disabled != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the fieldset legend.
     *
     * @access public
     *
     * @return string|null
     */
    public function getLegend()
    {
        return $this->legend;
    }

    /**
     * Set the legent of this fieldset.
     *
     * @access public
     *
     * @param $legend string the legend to display.
     */
    public function setLegend($legend)
    {
        $this->legend = $legend;
    }

    /**
     * Get all controls for this fieldset.
     *
     * @access public
     *
     * @return array|null
     */
    public function getControls()
    {
        return $this->controls;
    }

    /**
     * Parse the JSON input with the following format and set the internal variables:
     *
     * {
     *   "id": "12345",
     *   "disabled": false,
     *   "legend": "Form legend",
     *   "controls": {
     *     //See form
     *     }
     * }
     *
     * @see Form
     *
     * @access public
     *
     * @param $json array associative array with the decoded JSON string.
     */
    public function parseJSON($json)
    {
        if (isset($json["id"])) {
            $this->setID($json["id"]);
        }
        if (isset($json["disabled"])) {
            $this->setDisabled($json["disabled"]);
        }
        if (isset($json["legend"])) {
            $this->setLegend($json["legend"]);
        }

        if (isset($json["controls"])) {
            foreach ($json["controls"] as $control_array) {
                // Fetch the key of current's pointer position.
                $class_name = key($control_array);

                $this->controls[] = Form::parseControl($class_name, $control_array[$class_name]);
            }
        }
    }

    /**
     * You can disable or enable the fieldset by setting true or false.
     *
     * @access public
     *
     * @param $disabled boolean true to disable or false to enable.
     */
    public function setDisabled($disabled)
    {
        if ($disabled) {
            $this->disabled = "disabled";
        } else {
            $this->disabled = null;
        }
    }
}
