<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms
 * @file
 *  Form.php
 *
 * Form class defines the object form, holds controls and validators.
 *
 * Example JSON with required keys:
 * {
 *     "name": "login_frm",
 *     "action": "login.php",
 *     "controls": [
 *         {
 *             "InputText": {
 *                 "id": "input01",
 *                 "name": "username",
 *                 "label": "Username:",
 *                 "autofocus": true,
 *                 "placeholder": "Username to login"
 *             }
 *         },
 *         {
 *             "InputPassword": {
 *                 "id": "input02",
 *                 "name": "password",
 *                 "label": "Password:",
 *                 "placeholder": "Your password"
 *             }
 *         }
 *     ],
 *     "validators": [
 *         {
 *             "FieldRequired": {
 *                 "fields": [
 *                     "username",
 *                     "password"
 *                 ]
 *             }
 *         }
 *     ]
 * }
 *
 * Full JSON:
 * {
 *     "name": "login_frm",
 *     "action": "login.php",
 *     "method": "post",
 *     "id": "login_frm",
 *     "attributes": {
 *         "style": "margin-top: 5px;"
 *     },
 *     "classes": [
 *         "input-xlarge"
 *     ],
 *     "trim": true,
 *     "autocomplete": true,
 *     "novalidate": true,
 *     "target": "_parent",
 *     "enctype": "application/x-www-form-urlencoded",
 *     "accept-charset": "UTF-8",
 *     "controls": [
 *       //See above
 *     ],
 *     "validators": [
 *       //See above
 *     ]
 * }
 */

namespace com\oratush\forms;
use com\oratush\forms\controls\AbstractControl;

class Form
{
    const ENCODING_FORM_URLENCODED = "application/x-www-form-urlencoded";
    const ENCODING_FORM_DATA = "multipart/form-data";
    const METHOD_GET = "get";
    const METHOD_POST = "post";
    const NAMESPACE_CONTROLS = "com\\oratush\\forms\\controls\\";
    const NAMESPACE_VALIDATORS = "com\\oratush\\forms\\validators\\";

    //Specifies the name of a form
    private $name;
    //Specifies where to send the form-data when a form is submitted
    private $action;

    //It contains either FieldSets or Controls
    private $controls;
    private $all_controls;

    //Support validation and error messages
    private $errors;
    private $validators;
    private $errormessage;

    //Specifies the HTTP method to use when sending form-data (default POST)
    private $method;
    //Specifies how the form-data should be encoded when submitting it to the server (only for method="post")
    private $enctype;
    //The form ID
    private $id;
    //Additional attributes (array)
    private $attributes;
    //Additional classes (array)
    private $classes;
    //If set the values are trimmed
    private $trimvalues;

    //Specifies the character encodings that are to be used for the form submission
    private $acceptcharset;
    //Specifies whether a form should have autocomplete on or off
    private $autocomplete;
    //Specifies that the form should not be validated when submitted
    private $novalidate;
    //Specifies where to display the response that is received after submitting the form (_blank|_self|_parent|_top|framename)
    private $target;

    public function __construct()
    {
        $this->name = "oratush";
        $this->action = "";

        $this->method = $this->getDefaultMethod();
        $this->enctype = $this->getDefaultEncoding();

        $this->id = null;
        $this->attributes = null;
        $this->classes = null;
        $this->trimvalues = false;
        $this->validators = null;
        $this->errormessage = null;

        $this->errors = array();
        $this->controls = array();
        $this->all_controls = array();

        $this->acceptcharset = null;
        $this->autocomplete = null;
        $this->novalidate = null;
        $this->target = null;
    }

    //Setters/Getters

    public function setID($id)
    {
        $this->id = $id;
    }

    public function getID()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setEncodingType($enctype)
    {
        if ($this->isEncodingValid($enctype)) {
            $this->enctype = $enctype;
        } else {
            $this->enctype = $this->getDefaultEncoding();
        }
    }

    public function getEncodingType()
    {
        return $this->enctype;
    }

    public function setAction($action)
    {
        $this->action = $action;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function setMethod($method)
    {
        if ($this->isMethodValid($method)) {
            $this->method = $method;
        } else {
            $this->method = $this->getDefaultMethod();
        }
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function setAutocomplete($complete)
    {
        if ($complete) {
            $this->autocomplete = "on";
        } else {
            $this->autocomplete = "off";
        }
    }

    public function isAutocomplete()
    {
        return (is_null($this->autocomplete) || $this->autocomplete == "on");
    }

    public function setAcceptCharset($acceptcharset)
    {
        $this->acceptcharset = $acceptcharset;
    }

    public function getAcceptCharset()
    {
        return $this->acceptcharset;
    }

    public function setTarget($target)
    {
        $this->target = $target;
    }

    public function getTarget()
    {
        return $this->target;
    }

    public function setValidate($validate)
    {
        if ($validate) {
            $this->novalidate = null;
        } else {
            $this->novalidate = "novalidate";
        }
    }

    public function isValidate()
    {
        return is_null($this->novalidate);
    }

    public function setTranslatedErrorMessage($message)
    {
        $this->errormessage = $message;
    }

    public function getTranslatedErrorMessage()
    {
        return $this->errormessage;
    }

    public function setTrimValues($trim)
    {
        if ($trim) {
            $this->trimvalues = true;
        } else {
            $this->trimvalues = false;
        }
    }

    public function isTrimEnabled()
    {
        return $this->trimvalues;
    }

    public function setAttribute($key, $value)
    {
        if ($key != null && $value != null) {
            $this->attributes[$key] = $value;
        }
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function addClass($value)
    {
        if ( $value != null ) {
            $this->classes[] = $value;
        }
    }

    public function getClasses()
    {
        return $this->classes;
    }

    //Various retrieval methods

    public function getControls()
    {
        return $this->controls;
    }

    public function getAllControls()
    {
        return $this->all_controls;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    //Data processing to set control values

    public function processData()
    {
        if ($this->method == Form::METHOD_GET) {
            $this->processDataArray($_GET);
        } else {
            $this->processDataArray($_POST);
        }
    }

    public function processDataArray($data)
    {
        if ($data != null) {
            //Part 1 - Clear selections
            $keys = array_keys($this->all_controls);

            foreach ($keys as $key) {
                $field = $this->all_controls[$key];

                if ($field->getControlType() == AbstractControl::INPUT) {
                    if ($field->getType() == AbstractControl::INPUT_CHECKBOX || $field->getType() == AbstractControl::INPUT_RADIO) {
                        $field->removeSelections();
                    }
                } else if ($field->getControlType() == AbstractControl::SELECT) {
                    $field->removeSelections();
                }
            }

            //Fix the data array with values from the $_FILES global
            $file_keys = array_keys($_FILES);
            foreach ($file_keys as $file) {
                $data[$file] = "upload";
            }

            //Part 2 - Set values/selections
            $keys = array_keys($data);

            foreach ($keys as $elem) {
                //For InputImages check and fix for the trailing .x and .y
                if ( substr($elem, -2)==".x" && !empty($data[substr($elem, 0, -2) .".y"]) ) {
                    $elem = substr($elem, 0, -2);
                }

                if (isset($this->all_controls[$elem])) {
                    $field = $this->all_controls[$elem];

                    $field_control = $field->getControlType();
                    $field_subcontrol = null;

                    if ($field_control == AbstractControl::INPUT) {
                        $field_subcontrol = $field->getType();

                        if ($field_subcontrol == AbstractControl::INPUT_TEXT
                            || $field_subcontrol == AbstractControl::INPUT_HIDDEN
                            || $field_subcontrol == AbstractControl::INPUT_PASSWORD) {
                            //Do we need to trim the value
                            if ($field_subcontrol != AbstractControl::INPUT_PASSWORD && $this->isTrimEnabled()) {
                                $field->setValue(trim($data[$elem]));
                            } else {
                                $field->setValue($data[$elem]);
                            }
                        } else if ($field_subcontrol == AbstractControl::INPUT_CHECKBOX) {
                            $this->setMultipleSelections($field, $data[$elem]);
                        } else if ($field_subcontrol == AbstractControl::INPUT_RADIO) {
                            $field->setSelected($data[$elem]);
                        } else if ($field_subcontrol == AbstractControl::INPUT_IMAGE) {
                            if ( !empty($data[$elem .".x"]) )
                                $field->setX($data[$elem .".x"]);
                            if ( !empty($data[$elem .".y"]) )
                                $field->setY($data[$elem .".y"]);

                            $field->setValue($field->getX() .",". $field->getY());
                        } else if ($field_subcontrol == AbstractControl::INPUT_FILE) {
                            $field->processFilesArray();
                        }
                    } else if ($field_control == AbstractControl::TEXTAREA) {
                        //Do we need to trim the value
                        if ($this->isTrimEnabled()) {
                            $field->setValue(trim($data[$elem]));
                        } else {
                            $field->setValue($data[$elem]);
                        }
                    } else if ($field_control == AbstractControl::SELECT) {
                        if ($field->isMultiple()) {
                            $this->setMultipleSelections($field, $data[$elem]);
                        } else {
                            $field->setSelected($data[$elem]);
                        }
                    }
                }
            }
        }
    }

    private function setMultipleSelections($field, $value)
    {
        if (is_array($value)) {
            foreach($value as $single_value) {
                $field->setSelected($single_value);
            }
        } else {
            $field->setSelected($value);
        }
    }

    /**
     * Form parser json function, sets all values and attributes to the Form object,
     * according to the given json.
     *
     * @access public
     *
     * @param $json string
     *
     * @return boolean true for success, false on failure
     */
    public function parseJSON($json)
    {
        $result = false;

        //TODO Schema validation on JSON string

        $json_input = json_decode($json, true);
        if ($json_input != null) {
            $result = true;
            $this->parseJSONArray($json_input);
        }

        return $result;
    }

    /**
     * Parse the JSON input and set the internal variables.
     *
     * @access private
     *
     * @param $json array associative array with the decoded JSON string.
     */
    private function parseJSONArray($json)
    {
        if (isset($json["id"])) {
            $this->setID($json["id"]);
        }
        if (isset($json["name"])) {
            $this->setName($json["name"]);
        }
        if (isset($json["action"])) {
            $this->setAction($json["action"]);
        }
        if (isset($json["method"])) {
            $this->setMethod($json["method"]);
        }
        if (isset($json["trim"])) {
            $this->setTrimValues($json["trim"]);
        }
        if (isset($json["autocomplete"])) {
            $this->setAutocomplete($json["autocomplete"]);
        }
        if (isset($json["novalidate"])) {
            $this->setValidate(!$json["novalidate"]);
        }
        if (isset($json["target"])) {
            $this->setTarget($json["target"]);
        }
        if (isset($json["enctype"])) {
            $this->setEncodingType($json["enctype"]);
        }
        if (isset($json["accept-charset"])) {
            $this->setAcceptCharset($json["accept-charset"]);
        }

        if (isset($json["classes"])) {
            foreach($json["classes"] as $class) {
                $this->addClass($class);
            }
        }

        if (isset($json["attributes"])) {
            $total = count($json["attributes"]);

            $keys = array_keys($json["attributes"]);
            for ($i = 0; $i < $total; $i++) {
                $this->setAttribute($keys[$i], $json["attributes"][$keys[$i]]);
            }
        }

        if (isset($json["controls"])) {
            foreach ($json["controls"] as $control) {
                $key = key($control);

                if ($key == "Fieldset") {
                    $fieldset = new Fieldset();
                    $fieldset->parseJSON($control[$key]);

                    $this->addFieldset($fieldset);
                } else {
                    $new_control = Form::parseControl($key, $control[$key]);

                    $this->addControl($new_control);
                }
            }
        }

        if (isset($json["validators"])) {
            foreach($json["validators"] as $validator) {
                $key = key($validator);
                $new_validator = Form::parseValidator($key, $validator[$key]);
                $this->addValidator($new_validator);
            }
        }
    }

    //Controls methods

    public function addFieldset($fieldset)
    {
        if ($fieldset!=null && is_a($fieldset, __NAMESPACE__ ."\\Fieldset"))
        {
            $all_controls = $fieldset->getControls();

            if ($all_controls != null) {
                foreach($all_controls as $control) {
                    $this->addToAllControls($control);
                }
            }

            $this->addNewControl($fieldset, false);
        }
    }

    public function addControl($item)
    {
        if ($item != null && is_a($item, Form::NAMESPACE_CONTROLS ."AbstractControl")) {
            $this->addNewControl($item);
        }
    }

    private function addNewControl($item, $to_all_controls = true)
    {
        $this->controls[] = $item;

        if ($to_all_controls) {
            $this->addToAllControls($item);
        }
    }

    private function addToAllControls($item)
    {
        if ($item->getControlType() == AbstractControl::BUTTON
            || $item->getControlType() == AbstractControl::BUTTONSET) {
            return;
        }

        $name = $item->getName();
        if ($name != null) {
            $this->all_controls[$name] = $item;
        }
    }

    /**
     * Create a new control class and initialize it using
     * the provided JSON
     *
     * @access public
     *
     * @param $classname string the class name of the control.
     * @param $json array associative array with the decoded JSON string.
     *
     * @return object control
     */
    public static function parseControl($classname, $json)
    {
        $class = "\\". Form::NAMESPACE_CONTROLS . $classname;

        $item = new $class();
        $item->parseJSON($json);

        return $item;
    }

    //Validator methods

    /**
     * Check if a form is valid running all validators
     *
     * access public
     *
     * @return boolean true if valid
     */
    public function isValid()
    {
        $outcome = false;
        $this->errors = array();

        if ($this->validators != null) {
            foreach ($this->validators as $validator) {
                $new_errors = $validator->execute($this->all_controls);

                if ($new_errors != null) {
                    $this->errors = array_merge($this->errors, $new_errors);
                }
            }
        }

        if (count($this->errors) == 0) {
            $outcome = true;
        }

        return $outcome;
    }

    /**
     * First process the submitted data (get or post) then call the isValid
     * method. If everything is OK return null or return the errors array.
     *
     * access public
     *
     * @return null|array a FormError array is returned if the form is not valid!
     */
    public function checkData()
    {
        $this->processData();

        if ( $this->isValid() ) {
            return null;
        } else {
            return $this->errors;
        }
    }

    public function getValidators()
    {
        return $this->validators;
    }

    public function addValidator($validator)
    {
        if ($validator != null && is_a($validator, Form::NAMESPACE_VALIDATORS ."AbstractValidator")) {
            $this->validators[] = $validator;
        }
    }

    /**
     * Create a new validator class and initialize it using
     * the provided JSON
     *
     * @access public
     *
     * @param $classname string the class name of the validator.
     * @param $json array associative array with the decoded JSON string.
     *
     * @return object validator
     */
    public static function parseValidator($classname, $json)
    {
        $class = "\\". Form::NAMESPACE_VALIDATORS . $classname;

        $item = new $class();
        $item->parseJSON($json);

        return $item;
    }

    //Internal methods for default values and sanity checks

    private function getDefaultMethod()
    {
        return Form::METHOD_POST;
    }

    private function isMethodValid($method)
    {
        return (($method == Form::METHOD_GET) || ($method == Form::METHOD_POST));
    }

    private function getDefaultEncoding()
    {
        return Form::ENCODING_FORM_URLENCODED;
    }

    private function isEncodingValid($enctype)
    {
        return (($enctype == Form::ENCODING_FORM_URLENCODED)
            || ($enctype == Form::ENCODING_FORM_DATA));
    }
}
