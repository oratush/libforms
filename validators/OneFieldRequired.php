<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  OneFieldRequired.php
 *
 * OneFieldRequired class defines the object for validating that at least one value is filled in.
 */

namespace com\oratush\forms\validators;
use com\oratush\forms\FormError as FormError;

class OneFieldRequired extends FieldRequired
{
    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct();

        $this->type = parent::ONE_FIELD_REQUIRED;
    }

    /**
     * Overrides parent's execute.
     *
     * @access public
     *
     * @param  $all_controls array containing all available controls.
     *
     * @return \com\oratush\forms\FormError array, otherwise null.
     */
    public function execute($all_controls)
    {
        $errors = parent::execute($all_controls);

        $total_errors = is_null($errors) ? 0 : count($errors);
        $total_fields = is_null($this->fields) ? 0 : count($this->fields);

        $final_errors = null;

        //Copy FieldNotFound errors
        for ($i = 0; $i < $total_errors; $i++)
        {
            if ( $errors[$i]->getCode()==FormError::ERROR_FIELD_NOT_FOUND ) {
                $final_errors[] = $errors[$i];
            }
        }

        //If all fields have errors we fail!
        if ($total_fields > 0 && $total_fields == $total_errors) {
            $one_error = parent::getError(FormError::ERROR_NO_GROUP_VALUE);

            for ($i = 0; $i < $total_fields; $i++) {
                if ( isset($all_controls[$this->fields[$i]]) ) {
                    $one_error->addControl($all_controls[$this->fields[$i]]);
                }
            }

            $final_errors[] = $one_error;
        }

        return $final_errors;
    }
}
