<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  ValidNumber.php
 *
 * ValidNumber class defines the object for validating int numbers.
 */

namespace com\oratush\forms\validators;
use com\oratush\forms\FormError as FormError;
use com\oratush\forms\controls\AbstractControl as AbstractControl;

class ValidNumber extends FieldRequired
{
    public function __construct()
    {
        parent::__construct();

        $this->type = parent::VALID_NUMBER;
    }

    /**
     * Checks a single control against int requirements.
     *
     * @access protected
     *
     * @param $control object for the attached control.
     *
     * @return \com\oratush\forms\FormError, otherwise null.
     */
    protected function checkSingleControl($control)
    {
        $error = null;

        if (parent::validationRequired($control, array(AbstractControl::INPUT => array(
                AbstractControl::INPUT_TEXT,
                AbstractControl::INPUT_HIDDEN,
                AbstractControl::INPUT_PASSWORD)))) {
            $value = $control->getValue();

            if (!empty($value)) {
                $error = $this->checkValue($value);

                if ($error != null) {
                    $error->setControl($control);
                }
            }
        }

        return $error;
    }

    /**
     * Checks given value against regex int value format.
     *
     * @access protected
     *
     * @param $value string int.
     *
     * @return \com\oratush\forms\FormError, otherwise null.
     */
    protected function checkValue($value)
    {
        return (0 == preg_match("/^[-+]?[0-9]+$/", $value)) ?
            parent::getError(FormError::ERROR_INVALID_NUMBER) :
            null;
    }
}
