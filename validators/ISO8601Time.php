<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  ISO8601Time.php
 *
 * ISO8601Time class defines the object for validating time in ISO8601 format.
 */

namespace com\oratush\forms\validators;
use com\oratush\forms\FormError as FormError;

class ISO8601Time extends ValidNumber
{
    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct();

        $this->type = parent::ISO8601_TIME;
    }

    /**
     * Checks given value against H:i:s format.
     *
     * @access protected
     *
     * @param $value string time.
     *
     * @return \com\oratush\forms\FormError, otherwise null.
     */
    protected function checkValue($value)
    {
        $time_without_timezone = "H:i:s";
        $time_with_timezone = $time_without_timezone . "O";

        $error = null;

        $date = \DateTime::createFromFormat($time_with_timezone, $value);
        $date_errors = \DateTime::getLastErrors();
        if ($date === false || $date_errors["warning_count"]>0) {
            $date = \DateTime::createFromFormat($time_without_timezone, $value);
            $date_errors = \DateTime::getLastErrors();

            if ($date === false || $date_errors["warning_count"]>0) {
                $error = parent::getError(FormError::ERROR_INVALID_ISO_TIME);
                $error->setPayload(json_encode($date_errors));
            }
        }

        return $error;
    }
}
