<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  EntryAlreadyExists.php
 *
 * EntryAlreadyExists class defines the object for same entries in the
 * database using PHP Data Objects (PDO). Please note that PDO must be
 * configured to throw exceptions on errors, so set the following:
 * <DB>->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 *
 * Example json input :
 *  {
 *   "id": "CheckUsername",
 *   "name": "username",
 *   "dbc": "db_conn",
 *   "query": "SELECT COUNT(user_id) FROM admins WHERE enabled=1 AND username=?"
 *  }
 */

namespace com\oratush\forms\validators;
use com\oratush\forms\FormError as FormError;
use com\oratush\forms\controls\AbstractControl as AbstractControl;

class EntryAlreadyExists extends AbstractValidator
{
    private $id;        //Identifier for reference
    private $field;     //Field to retrieve the value to check to the database
    private $database;  //Database variable name (must be available in GLOBALS array)
    private $query;     //The query to execute 

    //There are cases that you want to exclude specific table rows. The exclude string
    //is appended in the query to work as a filter. Example:
    //
    //Assume that we have a form to manage users in our system and we want the username
    //for a given user to be unique. Both the ADD and EDIT forms use the same JSON input.
    //So we set an EntryAlreadyExists validator for the username field.
    //
    //This works well when I ADD users. But when I try to EDIT a user and I do not
    //want to update his username this validation will fail. If we append the user ID
    //that we are currently editing in the exclude only for the EDIT form everything is
    //working as expected.
    private $exclude;

    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        $this->type = parent::ENTRY_ALREADY_EXISTS;

        $this->id = null;
        $this->field = null;
        $this->database = null;
        $this->query = null;
        $this->exclude = null;
    }

    public function getID()
    {
        return $this->id;
    }

    public function setID($id)
    {
        $this->id = $id;
    }

    public function getExclude()
    {
        return $this->exclude;
    }

    public function setExclude($exclude)
    {
        $this->exclude = $exclude;
    }

    /**
     * Overrides parent's execute.
     *
     * @access public
     *
     * @param  $all_controls array containing all available controls.
     *
     * @return \com\oratush\forms\FormError array, otherwise null.
     */
    public function execute($all_controls)
    {
        $errors = null;
        $outcome = true;
        $payload = null;

        if ( isset($all_controls[$this->field]) )
        {
            $control = $all_controls[$this->field];

            if (parent::validationRequired($control, array(AbstractControl::INPUT => array(
                    AbstractControl::INPUT_TEXT,
                    AbstractControl::INPUT_HIDDEN))))
            {
                $control_value = $control->getValue();

                if (!empty($control_value)) {
                    $outcome = false;

                    if (isset($GLOBALS[$this->database]))
                    {
                        $db = $GLOBALS[$this->database];

                        try {
                            $query = $this->query;
                            if (!empty($this->exclude)) {
                                $query .= $this->exclude;
                            }

                            $stmt = $db->prepare($query);
                            $stmt->bindValue(1, $control_value, \PDO::PARAM_STR);
                            $stmt->execute();
                            $row_count = $stmt->rowCount();

                            if ($row_count != 0) {
                                $row = $stmt->fetch(\PDO::FETCH_NUM);

                                if (is_numeric($row[0]) && $row[0] == 0) {
                                    $outcome = true;
                                }
                                else {
                                    $payload = "Result should be 0 but got: ". $row[0];
                                }
                            }
                            else
                            {
                                $payload  = "Query must return a row! Example: SELECT COUNT(*) FROM users";
                                $payload .= " WHERE username=?";
                            }
                        } catch (\PDOException $ex) {
                            $payload  = "Database exception: ". $ex->getMessage();
                        }
                    }
                    else
                    {
                        $payload = "Database variable not found in GLOBALS array: ". $this->database;
                    }
                }
            }
        }
        else
        {
            $errors[] = parent::getErrorFieldNotFound($this->field);
        }

        if (!$outcome)
        {
            $error = parent::getError(FormError::ERROR_DB_ENTRY_EXISTS);
            $error->setControl($all_controls[$this->field]);

            $json["message"] = $payload;
            $error->setPayload(json_encode($json));

            $errors[] = $error;
        }

        return $errors;
    }

    /**
     * Overrides parent function.
     *
     * @access public
     *
     * @param $json array to be parsed.
     */
    public function parseJSON($json)
    {
        if (isset($json["id"])) {
            $this->id = $json["id"];
        }
        if (isset($json["name"])) {
            $this->field = $json["name"];
        }
        if (isset($json["dbc"])) {
            $this->database = $json["dbc"];
        }
        if (isset($json["query"])) {
            $this->query = $json["query"];
        }
    }
}
