<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  MatchRegex.php
 *
 * MatchRegex class defines the object for validating against regex.
 *
 * Example JSON output :
 * {
 *  "fields": [
 *      { "name": "username", "expression": "^69[0-9]{8}$", "errorno": 10,
 *              "errordesc": "This is not a valid telephone number." }
 *    ]
 * }
 */

namespace com\oratush\forms\validators;
use com\oratush\forms\FormError as FormError;
use com\oratush\forms\controls\AbstractControl as AbstractControl;

class MatchRegex extends AbstractValidator
{
    //The fields to validate
    protected $fields;

    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        $this->type = parent::MATCH_REGEX;
        $this->fields = null;
    }

    /**
     * Overrides parent's execute.
     *
     * @access public
     *
     * @param  $all_controls array containing all available controls.
     *
     * @return null|array of FormError.
     */
    public function execute($all_controls)
    {
        $errors = null;

        if ($this->fields != null)
        {
            $total = count($this->fields);

            for($i=0; $i<$total; $i++)
            {
                if ( isset($all_controls[$this->fields[$i]["name"]]) )
                {
                    $error = $this->checkSingleControl($all_controls[$this->fields[$i]["name"]], $this->fields[$i]);

                    if ($error != null) {
                        $errors[] = $error;
                    }
                }
                else
                {
                    $errors[] = parent::getErrorFieldNotFound($this->fields[$i]["name"]);
                }
            }
        }

        return $errors;
    }

    /**
     * Checks a single control against regex.
     *
     * @access protected.
     *
     * @param $control object for the attached control.
     * @param $details array holding details.
     *
     * @return \com\oratush\forms\FormError array, otherwise null.
     */
    protected function checkSingleControl($control, $details)
    {
        $error = null;

        if (parent::validationRequired($control, array(AbstractControl::INPUT => array(
                AbstractControl::INPUT_TEXT,
                AbstractControl::INPUT_HIDDEN,
                AbstractControl::INPUT_PASSWORD),
                AbstractControl::TEXTAREA => array()))) {
            $value = $control->getValue();

            if (!empty($value)) {
                $error = $this->checkValue($value, $details);

                if ($error != null) {
                    $error->setControl($control);
                }
            }
        }

        return $error;
    }

    /**
     * Checks given value against regex int value format.
     *
     * @access protected
     *
     * @param $value string field value.
     * @param $details array holding details.
     *
     * @return \com\oratush\forms\FormError, otherwise null.
     */
    protected function checkValue($value, $details)
    {
        $error = null;

        if (preg_match("/" . $details["expression"] . "/", $value) == 0)
        {
            $error = parent::getError(FormError::ERROR_MATCHING_REGEX);

            $results = null;
            if (isset($details["errorno"])) {
                $results["errorno"] = $details["errorno"];
            }
            if (isset($details["errordesc"])) {
                $results["errordesc"] = $details["errordesc"];
            }

            if ($results != null) {
                $error->setPayload(json_encode($results));
            }
        }

        return $error;
    }

    public function clearFields()
    {
        $this->fields = null;
    }

    /**
     * Overrides parent function.
     *
     * @access public
     *
     * @param $json array to be parsed.
     */
    public function parseJSON($json)
    {
        if (isset($json["fields"])) {
            foreach ($json["fields"] as $field) {
                $this->fields[] = $field;
            }
        }
    }
}
