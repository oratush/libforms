<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  ValidateList.php
 *
 * ValidateList class defines the object for validating a list of values.
 *
 * Example JSON input :
 * {
 *  "fields": [
 *      { "name": "username", "separator": ",", "values": "one,two,three" }
 *      { "name": "username", "multiple": true, "separator": ",", "values": "one,two,three" }
 *    ]
 * }
 */

namespace com\oratush\forms\validators;
use com\oratush\forms\FormError as FormError;

class ValidateList extends MatchRegex
{
    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct();

        $this->type = parent::VALIDATE_LIST;
    }

    /**
     * Checks given value against selection requirements.
     *
     * @access protected
     *
     * @param $value string field value.
     * @param $details array holding details.
     *
     * @return \com\oratush\forms\FormError, otherwise null.
     */
    protected function checkValue($value, $details)
    {
        $error = null;

        if (isset($details["multiple"]) && $details["multiple"]) {
            $values = explode($details["separator"], $value);
        } else {
            $values = array();
            $values[0] = $value;
        }

        $items = explode($details["separator"], $details["values"]);

        $total_values = count($values);
        $total_items = count($items);

        for ($i = 0; $i < $total_values; $i++) {
            $found = false;

            for ($j = 0; $j < $total_items; $j++) {
                if ($values[$i] == $items[$j]) {
                    $found = true;
                    break;
                }
            }

            if (!$found) {
                $error = parent::getError(FormError::ERROR_INVALID_LIST);
                $payload["selection"] = $value;
                $payload["values"] = $details["values"];
                if (isset($details["multiple"]) && $details["multiple"]) {
                    $payload["multiple"] = true;
                }
                else {
                    $payload["multiple"] = false;
                }
                $error->setPayload(json_encode($payload));

                break;
            }
        }

        return $error;
    }
}
