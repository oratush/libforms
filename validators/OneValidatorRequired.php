<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  OneValidatorRequired.php
 *
 * Class OneValidatorRequired defines a way to execute multiple
 * validators and return an error only when all validators have
 * errors. This is usefull for example if a field must be checked
 * to either be a valid number of a valid email.
 *
 * Example JSON input :
 * {
 *  "validators": [
 *    {
 *      "ValidEmail": {
 *        "fields": [
 *          "username"
 *        ]
 *      }
 *    },
 *    {
 *      "ValidNumber": {
 *        "fields": [
 *          "username"
 *        ]
 *      }
 *    }
 *  ]
 * }
 */

namespace com\oratush\forms\validators;
use com\oratush\forms\Form as Form;

class OneValidatorRequired extends AbstractValidator
{
    //Validators to execute
    protected $validators;

    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        $this->type = parent::ONE_VALIDATOR_REQUIRED;
        $this->validators = null;
    }

    /**
     * Overrides parent's execute.
     *
     * @access public
     *
     * @param  $all_controls array containing all available controls.
     *
     * @return \com\oratush\forms\FormError array, otherwise null.
     */
    public function execute($all_controls)
    {
        $errors = null;

        $total_validators = count($this->validators);
        $total_errors = 0;

        for($i=0; $i<$total_validators; $i++)
        {
            $validator = $this->validators[$i];

            $new_errors = $validator->execute($all_controls);
            if ($new_errors != null) {
                $total_errors++;

                //We only return the errors from the first validator (if all failed)
                if ($errors == null) {
                    $errors = $new_errors;
                }
            }
        }

        //Only return error if all validators failed!
        if ($total_errors == $total_validators) {
            return $errors;
        }
        else {
            return null;
        }
    }

    /**
     * Overrides parent function.
     *
     * @access public
     *
     * @param $json array to be parsed.
     */
    public function parseJSON($json)
    {
        if (isset($json["validators"])) {
            foreach($json["validators"] as $validator) {
                $key = key($validator);
                $element = Form::parseValidator($key, $validator[$key]);
                $this->validators[] = $element;
            }
        }
    }
}
