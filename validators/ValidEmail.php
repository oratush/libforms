<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  ValidEmail.php
 *
 * ValidEmail class defines the object for validating email address.
 */

namespace com\oratush\forms\validators;
use com\oratush\forms\FormError as FormError;

class ValidEmail extends ValidNumber
{
    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct();

        $this->type = parent::VALID_EMAIL;
    }

    /**
     * Checks given value against valid email value format.
     *
     * @access protected
     *
     * @param $value string email.
     *
     * @return \com\oratush\forms\FormError, otherwise null.
     */
    protected function checkValue($value)
    {
        return !filter_var($value, FILTER_VALIDATE_EMAIL) ?
            parent::getError(FormError::ERROR_INVALID_EMAIL) :
            null;
    }
}
