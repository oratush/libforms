<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  ValidDate.php
 *
 * Class ValidDate defines a validator where each field must follow
 * the format provided in order to be parsed as a valid date.
 * For details on the format: http://php.net/manual/en/datetime.createfromformat.php
 *
 * Example JSON input :
 * {
 *  "format": "j/n/Y H:i",
 *  "fields": [
 *      "date1",
 *      "date2"
 *    ]
 * }
 */

namespace com\oratush\forms\validators;
use com\oratush\forms\FormError as FormError;

class ValidDate extends ValidNumber
{
    protected $format;

    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct();

        $this->type = parent::VALID_DATE;
    }

    /**
     * Checks given value against date format parser.
     *
     * @access protected
     *
     * @param $value string the date.
     *
     * @return \com\oratush\forms\FormError, otherwise null.
     */
    protected function checkValue($value)
    {
        $error = null;

        $date = \DateTime::createFromFormat($this->format, $value);
        $date_errors = \DateTime::getLastErrors();
        if ($date === false || $date_errors["warning_count"]>0) {
            $error = parent::getError(FormError::ERROR_INVALID_DATE);
            $date_errors["format"] = $this->format;
            $error->setPayload(json_encode($date_errors));
        }

        return $error;
    }

    /**
     * Overrides parent function.
     *
     * @access public
     *
     * @param $json array to be parsed.
     */
    public function parseJSON($json)
    {
        parent::parseJSON($json);

        if (isset($json["format"])) {
            $this->format = $json["format"];
        }
    }
}
