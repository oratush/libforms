<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  FieldEmpty.php
 *
 * Class FieldEmpty defines a validator where each field
 * must NOT have a value or selection.
 *
 * Example JSON input :
 * {
 *  "fields": [
 *      "username",
 *      "password"
 *    ]
 * }
 */

namespace com\oratush\forms\validators;
use com\oratush\forms\FormError as FormError;
use com\oratush\forms\controls\AbstractControl as AbstractControl;

class FieldEmpty extends FieldRequired
{
    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct();

        $this->type = parent::FIELD_EMPTY;
    }

    /**
     * Takes one field at a time and checks its validity based on the control type.
     *
     * @access protected
     *
     * @param AbstractControl the control to validate.
     *
     * @return \com\oratush\forms\FormError, otherwise null.
     */
    protected function checkSingleControl($control)
    {
        $error = null;

        switch ($control->getControlType()) {
            case AbstractControl::INPUT:

                switch ($control->getType()) {
                    case AbstractControl::INPUT_TEXT:
                    case AbstractControl::INPUT_PASSWORD:
                    case AbstractControl::INPUT_HIDDEN:
                        $value = $control->getValue();
                        if (!empty($value)) {
                            $error = parent::getError(FormError::ERROR_HAS_VALUE);
                        }
                        break;
                    case AbstractControl::INPUT_CHECKBOX:
                    case AbstractControl::INPUT_RADIO:
                        if ($control->hasItemSelected()) {
                            $error = parent::getError(FormError::ERROR_HAS_SELECTION);
                        }
                        break;
                    case AbstractControl::INPUT_FILE:
                        $file_name = $control->getName();

                        if (isset($_FILES[$file_name])) {
                            if ($control->isMultiple()) {
                                $file_error_array = $_FILES[$file_name]["error"];
                            }
                            else {
                                $file_error_array[] = $_FILES[$file_name]["error"];
                            }

                            foreach($file_error_array as $file_error) {
                                if ($file_error != 4) {
                                    //Value: 4; No file was uploaded.
                                    $error = parent::getError(FormError::ERROR_FILE_SELECTED);
                                }

                                if ($error != null) {
                                    break;
                                }
                            }
                        } else {
                            $error = parent::getError(FormError::ERROR_FILE_BAD_FORM_TYPE);
                        }
                        break;
                }
                break;
            case AbstractControl::TEXTAREA:
                $value = $control->getValue();
                if (!empty($value)) {
                    $error = parent::getError(FormError::ERROR_HAS_VALUE);
                }
                break;
            case AbstractControl::SELECT:
                if ($control->hasItemSelected()) {
                    $error = parent::getError(FormError::ERROR_HAS_SELECTION);
                }
                break;
        }

        if ($error != null) {
            $error->setControl($control);
        }

        return $error;
    }
}
