<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  ValidFloat.php
 *
 * ValidFloat class defines the object for validating floats.
 *
 * Example input:
 *
 * {
 *      "separator": ".",
 *      "fields": [
 *              "field1",
 *              "field2"
 *        ]
 * }
 */

namespace com\oratush\forms\validators;
use com\oratush\forms\FormError as FormError;

class ValidFloat extends ValidNumber
{
    private $decimal_separator; //Some times it is comma (3,14) some times period (3.14)

    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct();

        $this->type = parent::VALID_FLOAT;
        $this->decimal_separator = ".";
    }

    /**
     * Checks given value against regex float value format.
     *
     * @access protected
     *
     * @param $value string float.
     *
     * @return \com\oratush\forms\FormError, otherwise null.
     */
    protected function checkValue($value)
    {
        return 0 == preg_match("/^[-+]?[0-9]+\\". $this->decimal_separator ."?[0-9]*$/", $value) ?
            parent::getError(FormError::ERROR_INVALID_FLOAT) :
            null;
    }

    /**
     * Overrides parent function.
     *
     * @access public
     *
     * @param $json array to be parsed.
     */
    public function parseJSON($json)
    {
        parent::parseJSON($json);

        if (isset($json["separator"])) {
            $this->decimal_separator = $json["separator"];
        }
    }
}
