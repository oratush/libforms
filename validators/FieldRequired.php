<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  FieldRequired.php
 *
 * Class FieldRequired defines a validator where each field
 * must have a value (not null) and the length must be greater than
 * zero.
 *
 * Example JSON input :
 * {
 *  "fields": [
 *      "username",
 *      "password"
 *    ]
 * }
 */

namespace com\oratush\forms\validators;
use com\oratush\forms\FormError as FormError;
use com\oratush\forms\controls\AbstractControl as AbstractControl;

class FieldRequired extends AbstractValidator
{
    //The items to validate
    protected $fields;

    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        $this->type = parent::FIELD_REQUIRED;
        $this->fields = null;
    }

    /**
     * Overrides parent's execute.
     *
     * @access public
     *
     * @param  $all_controls array containing all available items.
     *
     * @return \com\oratush\forms\FormError array, otherwise null.
     */
    public function execute($all_controls)
    {
        $errors = null;

        if ($this->fields != null)
        {
            $total = count($this->fields);

            for($i=0; $i<$total; $i++)
            {
                if ( isset($all_controls[$this->fields[$i]]) )
                {
                    $error = $this->checkSingleControl($all_controls[$this->fields[$i]]);

                    if ($error != null) {
                        $errors[] = $error;
                    }
                }
                else
                {
                    $errors[] = parent::getErrorFieldNotFound($this->fields[$i]);
                }
            }
        }

        return $errors;
    }

    /**
     * Takes one field at a time and checks its validity based on the control type.
     *
     * @access protected
     *
     * @param AbstractControl the control to validate.
     *
     * @return \com\oratush\forms\FormError, otherwise null.
     */
    protected function checkSingleControl($control)
    {
        $error = null;

        switch ($control->getControlType()) {
            case AbstractControl::INPUT:

                switch ($control->getType()) {
                    case AbstractControl::INPUT_TEXT:
                    case AbstractControl::INPUT_PASSWORD:
                    case AbstractControl::INPUT_HIDDEN:
                        $value = $control->getValue();
                        if (empty($value)) {
                            $error = parent::getError(FormError::ERROR_NO_VALUE);
                        }
                        break;
                    case AbstractControl::INPUT_CHECKBOX:
                    case AbstractControl::INPUT_RADIO:
                        if (!$control->hasItemSelected()) {
                            $error = parent::getError(FormError::ERROR_NO_SELECTION);
                        }
                        break;
                    case AbstractControl::INPUT_FILE:
                        $file_name = $control->getName();

                        if (isset($_FILES[$file_name])) {
                            if ($control->isMultiple()) {
                                $file_error_array = $_FILES[$file_name]["error"];
                            }
                            else {
                                $file_error_array[] = $_FILES[$file_name]["error"];
                            }

                            foreach($file_error_array as $file_error) {
                                if ($file_error == 4) {
                                    //Value: 4; No file was uploaded.
                                    $error = parent::getError(FormError::ERROR_FILE_NOT_SELECTED);
                                } elseif ($file_error == 2) {
                                    //Value: 2; The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.
                                    $error = parent::getError(FormError::ERROR_FILE_MAX_SIZE_LIMIT);
                                } elseif ($file_error > 0) {
                                    $error = parent::getError(FormError::ERROR_FILE_UPLOAD_ERROR);
                                    $error->setPayload('{ "error": '. $file_error .' }');
                                }

                                if ($error != null) {
                                    break;
                                }
                            }
                        } else {
                            $error = parent::getError(FormError::ERROR_FILE_BAD_FORM_TYPE);
                        }
                        break;
                }
                break;
            case AbstractControl::TEXTAREA:
                $value = $control->getValue();
                if (empty($value)) {
                    $error = parent::getError(FormError::ERROR_NO_VALUE);
                }
                break;
            case AbstractControl::SELECT:
                if (!$control->hasItemSelected()) {
                    $error = parent::getError(FormError::ERROR_NO_SELECTION);
                }
                break;
        }

        if ($error != null) {
            $error->setControl($control);
        }

        return $error;
    }

    public function clearFields()
    {
        $this->fields = null;
    }

    /**
     * Overrides parent function.
     *
     * @access public
     *
     * @param $json array to be parsed.
     */
    public function parseJSON($json)
    {
        if (isset($json["fields"])) {
            foreach ($json["fields"] as $field) {
                $this->fields[] = $field;
            }
        }
    }
}
