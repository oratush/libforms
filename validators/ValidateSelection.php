<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  ValidateSelection.php
 *
 * ValidateSelection class defines the object for validating list of selections.
 *
 * Example JSON input :
 * {
 *  "fields": [
 *      { "name": "option", "separator": ",", "values": "one,two,three" }
 *    ]
 * }
 */

namespace com\oratush\forms\validators;
use com\oratush\forms\FormError as FormError;
use com\oratush\forms\controls\AbstractControl as AbstractControl;

class ValidateSelection extends MatchRegex
{
    private $selected;

    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct();

        $this->type = parent::VALIDATE_SELECTION;
        $this->selected = null;
    }

    /**
     * Checks a single control against selection requirements.
     *
     * @access protected
     *
     * @param $control object for the attached control.
     * @param $details array holding details.
     *
     * @return \com\oratush\forms\FormError, otherwise null.
     */
    protected function checkSingleControl($control, $details)
    {
        $error = null;

        if (parent::validationRequired($control, array(AbstractControl::INPUT => array(
                AbstractControl::INPUT_CHECKBOX,
                AbstractControl::INPUT_RADIO),
                AbstractControl::SELECT => array()))) {
            if ($control->hasItemSelected()) {
                $this->selected = array();
                $this->findSelected($control->getElements());
                $total_values = count($this->selected);

                if ($total_values > 1) {
                    if (
                            ($control->getControlType() == AbstractControl::SELECT && !$control->isMultiple()) ||
                            ($control->getControlType() == AbstractControl::INPUT && $control->getType() == AbstractControl::INPUT_RADIO)
                    ) {
                        $error = parent::getError(FormError::ERROR_MULTIPLE_SELECTIONS);
                    }
                }

                if ($error == null) {
                    $error = $this->checkValue(null, $details);
                }

                if ($error != null) {
                    $error->setControl($control);
                }
            }
        }

        return $error;
    }

    /**
     * Checks against selection requirements.
     *
     * @access protected
     *
     * @param $value string field value.
     * @param $details array holding details.
     *
     * @return \com\oratush\forms\FormError, otherwise null.
     */
    protected function checkValue($value, $details)
    {
        $error = null;

        $items = explode($details["separator"], $details["values"]);

        $total_values = count($this->selected);
        $total_items = count($items);

        for ($i = 0; $i < $total_values; $i++)
        {
            $found = false;

            for ($j = 0; $j < $total_items; $j++) {
                if ($this->selected[$i]->getValue() == $items[$j]) {
                    $found = true;
                    break;
                }
            }

            if (!$found) {
                $error = parent::getError(FormError::ERROR_INVALID_SELECTIONS);
                $payload["selection"] = $this->selected[$i]->getValue();
                $payload["values"] = $details["values"];
                $error->setPayload(json_encode($payload));

                break;
            }
        }

        return $error;
    }

    /**
     * Finds which values are selected from the given list.
     *
     * @access private
     *
     * @param $elements array of values.
     */
    private function findSelected($elements)
    {
        $total = count($elements);
        for ($i = 0; $i < $total; $i++) {
            $item = $elements[$i];

            if ($item->getControlType() == AbstractControl::GROUP) {
                $this->findSelected($item->getElements());
            } else {
                if ($item->isSelected()) {
                    $this->selected[] = $item;
                }
            }
        }
    }
}
