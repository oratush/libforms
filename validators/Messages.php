<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  Messages.php
 *
 * Defines helper methods to centralize error messages.
 */

namespace com\oratush\forms\validators;

class Messages
{
    //Constant for fallback message code
    const FALLBACK_MESSAGE_CODE = 001;

    /**
     * Holds mapping of codes and errors messages.
     *
     * @access private
     *
     * @return array
     *  - key : error code
     *  - value : error_message
     */
    private static function getMessageArray() {
        return array(
            //Generic errors
            001 =>  "Unknown error occurred.",
            002 =>  "Field name not found.",
            003 =>  "GD library is missing and image dimensions validation was requested.",
            //Basic errors
            100 =>  "Required field has no value!",
            101 =>  "Required field has no selection!",
            102 =>  "You must provide value to at least one field!",
            103 =>  "Passwords do not match!",
            104 =>  "Value not found in the list (see payload)!",
            105 =>  "Multiple values selected on non multiple field!",
            106 =>  "Value not found in the list (see payload)!",
            107 =>  "Field must have no value!",
            108 =>  "Field must have no selection!",
            //Database errors
            120 =>  "Invalid credentials (see payload)!",
            121 =>  "Entry already exists in the database (see payload)!" ,
            //Numeric errors
            130 =>  "The value contained is not float!",
            131 =>  "The value contained is not a number!",
            //Email error
            140 =>  "Invalid email provided!",
            //Comparison errors
            150 =>  "String is smaller than min length (see payload)!",
            151 =>  "String is bigger than max length (see payload)!",
            152 =>  "Number is smaller than min value (see payload)!",
            153 =>  "Number is bigger than max value (see payload)!",
            //Date errors
            160 =>  "The value is not a valid date/time (see payload)!",
            161 =>  "The value is not a valid ISO date and time (see payload)!",
            162 =>  "The value is not a valid ISO date (see payload)!",
            163 =>  "The value is not a valid ISO time (see payload)!",
            //Reg-ex error
            170 =>  "Regular expression not matched (see payload).",
            //Captcha errors
            180 =>  "Invalid captcha!",
            //File handling errors
            200 =>  "Upload file error (see payload)!",
            220 =>  "File variable not set!",
            221 =>  "No file was selected by user!",
            222 =>  "Max file size exceeded (MAX_FILE_SIZE)!",
            223 =>  "Max file size exceeded (see payload)!",
            224 =>  "MIME type not allowed (see payload)!" ,
            225 =>  "File extension not allowed (see payload)!",
            226 =>  "Invalid image dimensions (see payload)!",
            227 =>  "Not a valid image to compare dimensions!",
            228 =>  "File was selected by user!",
        );
    }

    /**
     * Function to retrieve an error message by code.
     *
     * @access public
     *
     * @param $error_code int the error code
     *
     * @return string the error message
     */
    public static function getMessage($error_code) {
        static $error_message_mapping;

        if (empty($error_message_mapping)) {
            // Initialize error_message_mapping.
            $error_message_mapping = Messages::getMessageArray();
        }

        if (isset($error_message_mapping[$error_code])) {
            return $error_message_mapping[$error_code];
        } else {
            return $error_message_mapping[Messages::FALLBACK_MESSAGE_CODE];
        }
    }
}
