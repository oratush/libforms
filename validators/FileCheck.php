<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  FileCheck.php
 *
 * FileCheck class defines the object for validating file uploads. Please
 * note that image width and height checks require PHP GD library installed.
 *
 * JSON example input :
 * {
 *  "fields": [
 *      { "name": "file", "max": 1024, "mime": "image/gif,image/jpeg,image/jpg,image/pjpeg,image/x-png", "ext": "gif,jpeg,jpg,png", "img_width": 200, "img_height": 200 },
 *      { "name": "file", "max": 1024, "mime": "image/gif,image/jpeg,image/jpg,image/pjpeg,image/x-png", "ext": "gif,jpeg,jpg,png" },
 *      { "name": "file", "max": 1024 }
 *    ]
 * }
 */

namespace com\oratush\forms\validators;
use com\oratush\forms\FormError as FormError;
use com\oratush\forms\controls\AbstractControl as AbstractControl;

class FileCheck extends MatchRegex
{
    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct();

        $this->type = parent::FILE_CHECK;
    }

    /**
     * Checks a single control against file upload requirements.
     *
     * @access protected.
     *
     * @param $control object for the attached control.
     * @param $details array holding details.
     *
     * @return \com\oratush\forms\FormError, otherwise null.
     */
    protected function checkSingleControl($control, $details)
    {
        $error = null;

        if (parent::validationRequired($control, array(AbstractControl::INPUT => array(
                AbstractControl::INPUT_FILE)))) {
            $file_field = $control->getName();

            if (isset($_FILES[$file_field]))
            {
                $no_error = true;
                if ($control->isMultiple()) {
                    $file_error_array = $_FILES[$file_field]["error"];
                }
                else {
                    $file_error_array[] = $_FILES[$file_field]["error"];
                }

                foreach($file_error_array as $file_error) {
                    if ($file_error != 0) {
                        $no_error = false;
                        break;
                    }
                }

                if ( $no_error ) {
                    $error = $this->checkValue($file_field, $details, $control->isMultiple());

                    if ($error != null) {
                        $error->setControl($control);
                    }
                }
            }
        }

        return $error;
    }

    /**
     * Checks given value against file requirements.
     *
     * @access protected
     *
     * @param $value string field name.
     * @param $details array holding details.
     * @param $multiple boolean true if the control is used to upload multiple files.
     *
     * @return \com\oratush\forms\FormError, otherwise null.
     */
    protected function checkValue($value, $details, $multiple)
    {
        $error = null;

        //Gather data for next step
        if ($multiple) {
            for($i=0; $i<count($_FILES[$value]["size"]); $i++) {
                $file_array[$i]["size"] = $_FILES[$value]["size"][$i];
                $file_array[$i]["type"] = $_FILES[$value]["type"][$i];
                $file_array[$i]["name"] = $_FILES[$value]["name"][$i];
                $file_array[$i]["tmp_name"] = $_FILES[$value]["tmp_name"][$i];
            }
        }
        else {
            $file_array[] = $_FILES[$value];
        }
        //--

        foreach($file_array as $file)
        {
            //Check the upload size
            if (isset($details["max"]) && $file["size"] > $details["max"]) {
                $error = parent::getError(FormError::ERROR_FILE_MAX_SIZE_LIMIT_WITH_PAYLOAD);
                $json["name"] = $file["name"];
                $json["max"] = $details["max"];
                $json["actual"] = $file["size"];
                $error->setPayload(json_encode($json));
            }

            //If we do not have a previous error check the MIME type
            if ($error==null && isset($details["mime"]))
            {
                $found = false;
                $values = explode(",", $details["mime"]);

                for ($i = 0; $i < count($values); $i++) {
                    if ($file["type"] == $values[$i]) {
                        $found = true;
                        break;
                    }
                }

                if (!$found) {
                    $error = parent::getError(FormError::ERROR_FILE_MIME_TYPE);
                    $json["name"] = $file["name"];
                    $json["allowed"] = $details["mime"];
                    $json["mime"] = $file["type"];
                    $error->setPayload(json_encode($json));
                }
            }

            //If we do not have a previous error check the file extension
            if ($error==null && isset($details["ext"]))
            {
                $temp = explode(".", $file["name"]);
                $extension = strtolower(end($temp));

                $found = false;
                $values = explode(",", strtolower($details["ext"]));

                for ($i = 0; $i < count($values); $i++) {
                    if ($extension == $values[$i]) {
                        $found = true;
                        break;
                    }
                }

                if (!$found) {
                    $error = parent::getError(FormError::ERROR_FILE_EXTENSION);
                    $json["name"] = $file["name"];
                    $json["allowed"] = strtolower($details["ext"]);
                    $json["extension"] = $extension;
                    $error->setPayload(json_encode($json));
                }
            }

            //If we do not have a previous error check the image dimensions
            if ($error==null && (isset($details["img_width"]) || isset($details["img_height"]))) {
                if (extension_loaded("gd") && function_exists("gd_info")) {
                    $size = getimagesize($file["tmp_name"]);
                    if ($size !== false) {
                        $found = false;

                        $json["name"] = $file["name"];

                        if (isset($details["img_width"]) && $size[0] != $details["img_width"]) {
                            $json["width"]["required"] = $details["img_width"];
                            $json["width"]["actual"] = $size[0];
                            $found = true;
                        }

                        if (isset($details["img_height"]) && $size[1] != $details["img_height"]) {
                            $json["height"]["required"] = $details["img_height"];
                            $json["height"]["actual"] = $size[1];
                            $found = true;
                        }

                        if ( $found ) {
                            $error = parent::getError(FormError::ERROR_FILE_BAD_IMAGE_DIMENSIONS);
                            $error->setPayload(json_encode($json));
                        }
                    } else {
                        $error = parent::getError(FormError::ERROR_FILE_BAD_IMAGE);
                    }
                }
                else {
                    $error = parent::getError(FormError::ERROR_GD_LIBRARY_MISSING);
                }
            }

            if ($error != null) {
                break;
            }
        }

        return $error;
    }
}
