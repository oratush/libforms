<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  ValidCaptcha.php
 *
 * ValidCaptcha class defines the object for validating against a captcha pic.
 */

namespace com\oratush\forms\validators;
use com\oratush\forms\FormError as FormError;
use com\oratush\forms\controls\AbstractControl as AbstractControl;

class ValidCaptcha extends AbstractValidator
{
    private $privatekey;

    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        $this->type = parent::VALID_CAPTCHA;

        $this->privatekey = null;
    }

    /**
     * Overrides parent's execute.
     *
     * @access public
     *
     * @param  $all_controls array containing all available controls.
     *
     * @return \com\oratush\forms\FormError array, otherwise null.
     */
    public function execute($all_controls)
    {
        $errors = null;
        $captcha = null;
        $success = true;

        //Does the form contain a captcha control?
        foreach($all_controls as $control)
        {
            if ($control->getControlType() == AbstractControl::CAPTCHA)
            {
                $captcha = $control;
                break;
            }
        }

        if ($captcha != null)
        {
            $success = false;

            if ( $captcha->getVersion()==2 )
            {
                if (isset($_POST['g-recaptcha-response']))
                {
                    $recaptcha = new \ReCaptcha\ReCaptcha($this->privatekey);
                    $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);

                    $success = $resp->isSuccess();
                }
            }
            else
            {
                if (isset($_POST["recaptcha_challenge_field"]) && isset($_POST["recaptcha_response_field"]))
                {
                    //Check with Google servers
                    $resp = recaptcha_check_answer(
                        $this->privatekey,
                        $_SERVER["REMOTE_ADDR"],
                        $_POST["recaptcha_challenge_field"],
                        $_POST["recaptcha_response_field"]
                    );

                    $success = $resp->is_valid;
                }
            }
        }

        if (!$success)
        {
            $error = parent::getError(FormError::ERROR_INVALID_CAPTCHA);
            $error->setControl($captcha);

            $errors[] = $error;
        }

        return $errors;
    }

    /**
     * Overrides parent function.
     *
     * @access public
     *
     * @param $json array to be parsed.
     */
    public function parseJSON($json)
    {
        if (isset($json["privatekey"])) {
            $this->privatekey = $json["privatekey"];
        }
    }
}
