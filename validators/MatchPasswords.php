<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  MatchPasswords.php
 *
 * MatchPasswords class defines the object for validating matching passwords.
 *
 * Example JSON input :
 * {
 *  "pwd1": "password",
 *  "pwd2": "password2"
 * }
 */

namespace com\oratush\forms\validators;
use com\oratush\forms\FormError as FormError;
use com\oratush\forms\controls\AbstractControl as AbstractControl;

class MatchPasswords extends AbstractValidator
{
    private $pwd1;
    private $pwd2;

    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        $this->type = parent::MATCH_PASSWORDS;

        $this->pwd1 = null;
        $this->pwd2 = null;
    }

    /**
     * Overrides parent's execute.
     *
     * @access public
     *
     * @param  $all_controls array containing all available controls.
     *
     * @return \com\oratush\forms\FormError array, otherwise null.
     */
    public function execute($all_controls)
    {
        $errors = null;
        $outcome = true;

        if ( isset($all_controls[$this->pwd1]) && isset($all_controls[$this->pwd2]) )
        {
            $pwd1_value = null;
            $pwd2_value = null;

            $control_type = array(AbstractControl::INPUT => array(
                    AbstractControl::INPUT_TEXT,
                    AbstractControl::INPUT_PASSWORD));

            if (parent::validationRequired($all_controls[$this->pwd1], $control_type)) {
                $pwd1_value = $all_controls[$this->pwd1]->getValue();
            }
            if (parent::validationRequired($all_controls[$this->pwd2], $control_type)) {
                $pwd2_value = $all_controls[$this->pwd2]->getValue();
            }

            if (!empty($pwd1_value) || !empty($pwd2_value)) {
                $outcome = false;

                if ($pwd1_value === $pwd2_value) {
                    $outcome = true;
                }
            }
        }
        else {
            if ( !isset($all_controls[$this->pwd1]) ) {
                $errors[] = parent::getErrorFieldNotFound($this->pwd1);
            }

            if ( !isset($all_controls[$this->pwd2]) ) {
                $errors[] = parent::getErrorFieldNotFound($this->pwd2);
            }
        }

        if (!$outcome) {
            $errors = array();
            $errors[0] = parent::getError(FormError::ERROR_MATCHING_PASSWORDS);

            $errors[0]->addControl($all_controls[$this->pwd1]);
            $errors[0]->addControl($all_controls[$this->pwd2]);
        }

        return $errors;
    }

    /**
     * Overrides parent function.
     *
     * @access public
     *
     * @param $json array to be parsed.
     */
    public function parseJSON($json)
    {
        if (isset($json["pwd1"])) {
            $this->pwd1 = $json["pwd1"];
        }
        if (isset($json["pwd2"])) {
            $this->pwd2 = $json["pwd2"];
        }
    }
}
