<?php
/**
 * @copyright   2015 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  AllValidatorsRequired.php
 *
 * Class AllValidatorsRequired defines a way to execute multiple
 * validators and return all errors found. This is usefull for example if
 * a field must be checked to have a value and be a valid email.
 *
 * Example JSON input :
 * {
 *  "validators": [
 *    {
 *      "FieldRequired": {
 *        "fields": [
 *          "username"
 *        ]
 *      }
 *    },
 *    {
 *      "ValidEmail": {
 *        "fields": [
 *          "username"
 *        ]
 *      }
 *    }
 *  ]
 * }
 */

namespace com\oratush\forms\validators;
use com\oratush\forms\Form as Form;

class AllValidatorsRequired extends OneValidatorRequired
{
    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct();

        $this->type = parent::ALL_VALIDATORS_REQUIRED;
    }

    /**
     * Overrides parent's execute.
     *
     * @access public
     *
     * @param  $all_controls array containing all available controls.
     *
     * @return \com\oratush\forms\FormError array, otherwise null.
     */
    public function execute($all_controls)
    {
        $errors = array();

        $total_validators = count($this->validators);
        $total_errors = 0;

        for($i=0; $i<$total_validators; $i++)
        {
            $validator = $this->validators[$i];

            $new_errors = $validator->execute($all_controls);
            if ($new_errors != null) {
                $errors = array_merge($errors, $new_errors);
                $total_errors++;
            }
        }

        //Only return error if an error occured!
        if ($total_errors > 0) {
            return $errors;
        }
        else {
            return null;
        }
    }
}

