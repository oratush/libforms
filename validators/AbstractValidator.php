<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  AbstractValidator.php
 *
 * Class AbstractValidator defines the abstract validator object
 * to be extended by any other validator.
 */

namespace com\oratush\forms\validators;
use com\oratush\forms\FormError as FormError;

abstract class AbstractValidator
{
    const FIELD_REQUIRED = "FieldRequired";
    const FIELD_EMPTY = "FieldEmpty";
    const VALID_FLOAT = "ValidFloat";
    const VALID_NUMBER = "ValidNumber";
    const VALID_EMAIL = "ValidEmail";
    const MATCH_REGEX = "MatchRegex";
    const MATCH_PASSWORDS = "MatchPasswords";
    const VALID_CAPTCHA = "ValidCaptcha";
    const ONE_FIELD_REQUIRED = "OneFieldRequired";
    const LENGTH_CHECK = "LengthCheck";
    const RANGE_CHECK = "RangeCheck";
    const ENTRY_ALREADY_EXISTS = "EntryAlreadyExists";
    const CHECK_CREDENTIALS = "CheckCredentials";
    const VALID_DATE = "ValidDate";
    const ISO8601_DATETIME = "ISO8601DateTime";
    const ISO8601_DATE = "ISO8601Date";
    const ISO8601_TIME = "ISO8601Time";
    const VALIDATE_SELECTION = "ValidateSelection";
    const VALIDATE_LIST = "ValidateList";
    const FILE_CHECK = "FileCheck";
    const ONE_VALIDATOR_REQUIRED = "OneValidatorRequired";
    const ALL_VALIDATORS_REQUIRED = "AllValidatorsRequired";

    //One of the above types
    protected $type;

    /**
     * Abstract function to execute the actual validations.
     *
     * @access public
     *
     * @param $all_controls array associative array with all the form controls.
     */
    abstract public function execute($all_controls);

    /**
     * Abstract function to parse a JSON input and initialize the validator.
     *
     * @access public
     *
     * @param $json array associative array with the decoded JSON string
     */
    abstract public function parseJSON($json);

    /**
     * Get the type of the validator.
     *
     * @return string the validator type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Determines if validation must be applied for the given $control, against $check_values criteria.
     *
     * @param $control AbstractControl the control to check
     * @param $check_values array
     *  - key: control type (e.g "INPUT" or "SELECT" etc.)
     *  - val: array of sub-control types (e.g "checkbox", "radio", "text" etc).
     *
     * @return bool true if validation needed, otherwise false.
     */
    protected function validationRequired($control, array $check_values)
    {
        $result = false;
        $control_type = $control->getControlType();

        if (array_key_exists($control_type, $check_values)) {
            $accepted_types = $check_values[$control_type];

            $result = empty($accepted_types) ? true : in_array($control->getType(), $accepted_types);
        }

        return $result;
    }

    /**
     * Based on the error code retrieve the Error object with the appropriate message.
     *
     * @param $error_code int the error code
     * @param $payload JSON string or null
     *
     * @return FormError the form error object
     */
    protected function getError($error_code, $payload = null)
    {
        $error = new FormError($error_code, Messages::getMessage($error_code));
        if ($payload != null ) {
            $error->setPayload($payload);
        }

        return $error;
    }

    /**
     * Error returned for field not found.
     *
     * @param $field string.
     *
     * @return FormError the form error object
     */
    protected function getErrorFieldNotFound($field)
    {
        $payload["field"] = $field;
        $error = self::getError(FormError::ERROR_FIELD_NOT_FOUND, json_encode($payload));

        return $error;
    }
}
