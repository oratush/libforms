<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  RangeCheck.php
 *
 * RangeCheck class defines the object for validating against ranges. For
 * better results apply first a ValidFloat or ValidNumber validator to the
 * fields you want to perform RangeCheck.
 *
 * Example JSON input :
 * {
 *  "fields": [
 *      { "name": "username", "min": 2, "max": 10 },
 *      { "name": "password", "min": 2, "max": 10 }
 *    ]
 * }
 */

namespace com\oratush\forms\validators;
use com\oratush\forms\FormError as FormError;

class RangeCheck extends MatchRegex
{
    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct();
 
        $this->type = parent::RANGE_CHECK;
    }

    /**
     * Checks given value against range requirements.
     *
     * @access protected
     *
     * @param $value string field value.
     * @param $details array holding details.
     *
     * @return \com\oratush\forms\FormError, otherwise null.
     */
    protected function checkValue($value, $details)
    {
        $error = null;

        if ( is_numeric($value) )
        {
            if (isset($details["min"])) {
                if ($value < $details["min"]) {
                    $error = parent::getError(FormError::ERROR_RANGE_TOO_LOW);

                    $payload["min_range"] = $details["min"];
                    $error->setPayload(json_encode($payload));
                }
            }

            if (isset($details["max"])) {
                if ($value > $details["max"]) {
                    $error = parent::getError(FormError::ERROR_RANGE_TOO_HIGH);

                    $payload["max_range"] = $details["max"];
                    $error->setPayload(json_encode($payload));
                }
            }
        }

        return $error;
    }
}
