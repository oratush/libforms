<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\validators
 * @file
 *  CheckCredentials.php
 *
 * CheckCredentials class defines the object for validating credentials
 * in the database using PHP Data Objects (PDO). Please note that PDO
 * must be configured to throw exceptions on errors, so set the following:
 * <DB>->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 */

namespace com\oratush\forms\validators;
use com\oratush\forms\FormError as FormError;
use com\oratush\forms\controls\AbstractControl as AbstractControl;

class CheckCredentials extends AbstractValidator
{
    private $username;
    private $password;
    private $database;
    private $query;

    /**
     * Constructor
     *
     * @access public
     */
    public function __construct()
    {
        $this->type = parent::CHECK_CREDENTIALS;

        $this->username = null;
        $this->password = null;
        $this->database = null;
        $this->query = null;
    }

    /**
     * Overrides parent's execute.
     *
     * @access public
     *
     * @param  $all_controls array containing all available controls.
     *
     * @return \com\oratush\forms\FormError, otherwise null.
     */
    public function execute($all_controls)
    {
        $errors = null;
        $outcome = true;
        $payload = null;

        if ( isset($all_controls[$this->username]) && isset($all_controls[$this->password]) )
        {
            if (parent::validationRequired($all_controls[$this->username], array(AbstractControl::INPUT => array(
                    AbstractControl::INPUT_TEXT))) &&
                parent::validationRequired($all_controls[$this->password], array(AbstractControl::INPUT => array(
                    AbstractControl::INPUT_PASSWORD))))
            {
                $username_value = $all_controls[$this->username]->getValue();
                $password_value = $all_controls[$this->password]->getValue();

                if (!empty($username_value) && !empty($password_value))
                {
                    $outcome = false;

                    if (isset($GLOBALS[$this->database]))
                    {
                        $db = $GLOBALS[$this->database];

                        try {
                            $stmt = $db->prepare($this->query);
                            $stmt->bindValue(1, $username_value, \PDO::PARAM_STR);
                            $stmt->execute();
                            $row_count = $stmt->rowCount();

                            if ($row_count != 0) {
                                $row = $stmt->fetch(\PDO::FETCH_NUM);

                                $outcome = validate_password($password_value, $row[0]);
                                if (!$outcome) {
                                    $payload = "Failed to validate the password.";
                                }
                            }
                            else {
                                $payload  = "Query must return a row! Example: SELECT password FROM users";
                                $payload .= " WHERE username=?";
                            }
                        } catch (\PDOException $ex) {
                            $payload  = "Database exception: ". $ex->getMessage();
                        }
                    }
                    else
                    {
                        $payload = "Database variable not found in GLOBALS array: ". $this->database;
                    }
                }
            }
        }
        else
        {
            if (!isset($all_controls[$this->username])) {
                $errors[] = parent::getErrorFieldNotFound($this->username);
            }

            if (!isset($all_controls[$this->password])) {
                $errors[] = parent::getErrorFieldNotFound($this->password);
            }
        }

        if (!$outcome) {
            $error = parent::getError(FormError::ERROR_DB_BAD_CREDENTIALS);
            $error->addControl($all_controls[$this->username]);
            $error->addControl($all_controls[$this->password]);

            $json["message"] = $payload;
            $error->setPayload(json_encode($json));

            $errors[] = $error;
        }

        return $errors;
    }

    /**
     * Overrides parent function.
     *
     * @access public
     *
     * @param $json array to be parsed.
     */
    public function parseJSON($json)
    {
        if (isset($json["usr"])) {
            $this->username = $json["usr"];
        }
        if (isset($json["pwd"])) {
            $this->password = $json["pwd"];
        }
        if (isset($json["dbc"])) {
            $this->database = $json["dbc"];
        }
        if (isset($json["query"])) {
            $this->query = $json["query"];
        }
    }
}
