<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms
 * @file
 *  FormError.php
 *
 * FormError class defines the Exception object for controlling form Exceptions.
 *
 * Exception can be caused because of one or multiple controls (i.e. passwords do not
 * match are a two controls error).
 *
 * The payload (if set) contains additional information about the exception. For
 * example in file uploads it contains the PHP FILE upload library error code.
 */

namespace com\oratush\forms;

class FormError
{
    const ERROR_FIELD_NOT_FOUND = 2;
    const ERROR_GD_LIBRARY_MISSING = 3;
    const ERROR_NO_VALUE = 100;
    const ERROR_NO_SELECTION = 101;
    const ERROR_NO_GROUP_VALUE = 102;
    const ERROR_MATCHING_PASSWORDS = 103;
    const ERROR_INVALID_LIST = 104;
    const ERROR_MULTIPLE_SELECTIONS = 105;
    const ERROR_INVALID_SELECTIONS = 106;
    const ERROR_HAS_VALUE = 107;
    const ERROR_HAS_SELECTION = 108;
    const ERROR_DB_BAD_CREDENTIALS = 120;
    const ERROR_DB_ENTRY_EXISTS = 121;
    const ERROR_INVALID_FLOAT = 130;
    const ERROR_INVALID_NUMBER = 131;
    const ERROR_INVALID_EMAIL = 140;
    const ERROR_LENGTH_TOO_SMALL = 150;
    const ERROR_LENGTH_OVERFLOW = 151;
    const ERROR_RANGE_TOO_LOW = 152;
    const ERROR_RANGE_TOO_HIGH = 153;
    const ERROR_INVALID_DATE = 160;
    const ERROR_INVALID_ISO_DATETIME = 161;
    const ERROR_INVALID_ISO_DATE = 162;
    const ERROR_INVALID_ISO_TIME = 163;
    const ERROR_MATCHING_REGEX = 170;
    const ERROR_INVALID_CAPTCHA = 180;
    const ERROR_FILE_UPLOAD_ERROR = 200;
    const ERROR_FILE_BAD_FORM_TYPE = 220;
    const ERROR_FILE_NOT_SELECTED = 221;
    const ERROR_FILE_MAX_SIZE_LIMIT = 222;
    const ERROR_FILE_MAX_SIZE_LIMIT_WITH_PAYLOAD = 223;
    const ERROR_FILE_MIME_TYPE = 224;
    const ERROR_FILE_EXTENSION = 225;
    const ERROR_FILE_BAD_IMAGE_DIMENSIONS = 226;
    const ERROR_FILE_BAD_IMAGE = 227;
    const ERROR_FILE_SELECTED = 228;

    private $code;
    private $message;

    private $controls;

    private $payload; //Payload with additional error information

    public function __construct($code, $message)
    {
        $this->code = $code;
        $this->message = $message;

        $this->controls = null;
        $this->payload = null;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function setControl($control)
    {
        unset($this->controls);
        $this->controls[] = $control;
    }

    public function setMultiControls($controls)
    {
        unset($this->controls);
        $this->controls = $controls;
    }

    public function addControl($control)
    {
        $this->controls[] = $control;
    }

    public function getControl()
    {
        return $this->controls[0];
    }

    public function getControls()
    {
        return $this->controls;
    }

    public function getPayload()
    {
        return $this->payload;
    }

    public function setPayload($payload)
    {
        $this->payload = $payload;
    }
}
