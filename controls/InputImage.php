<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\controls
 * @file
 *  InputImage.php
 *
 * InputImage class defines the object for Image element.
 *
 * Example JSON input :
 * {
 *  "id": "12345",
 *  "name": "img",
 *  "value": "img value",
 *  "label": "img label",
 *  "autofocus": true,
 *  "disabled": false,
 *  "classes": [
 *      "btn",
 *      "btn-default"
 *    ],
 *  "attributes": {
 *      "style": "padding-left: 20px"
 *    },
 *  "helptext": "Please view the image.",
 *  "readonly": false,
 *  "width": 200,
 *  "height": 100,
 *  "src": "images/test.png",
 *  "alt": "Test image"
 * }
 */

namespace com\oratush\forms\controls;

class InputImage extends Input
{
    private $width;
    private $height;
    private $src;
    private $alt;
    private $x_coord;
    private $y_coord;

    public function __construct()
    {
        parent::__construct();

        $this->type = parent::INPUT_IMAGE;

        $this->width = null;
        $this->height = null;
        $this->src = null;
        $this->alt = null;

        $this->x_coord = -1;
        $this->y_coord = -1;
    }

    public function getX()
    {
        return $this->x_coord;
    }

    public function setX($x_coord)
    {
        if ( $x_coord>=0 ) {
            $this->x_coord = $x_coord;
        }
    }

    public function getY()
    {
        return $this->y_coord;
    }

    public function setY($y_coord)
    {
        if ( $y_coord>=0 ) {
            $this->y_coord = $y_coord;
        }
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function setWidth($width)
    {
        if ($width != null && $width > 0) {
            $this->width = $width;
        }
        else {
            $this->width = null;
        }
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function setHeight($height)
    {
        if ($height != null && $height > 0) {
            $this->height = $height;
        }
        else {
            $this->height = null;
        }
    }

    public function getSource()
    {
        return $this->src;
    }

    public function setSource($src)
    {
        $this->src = $src;
    }

    public function getAlt()
    {
        return $this->alt;
    }

    public function setAlt($alt)
    {
        $this->alt = $alt;
    }

    public function parseJSON($json)
    {
        parent::parseJSON($json);

        if (isset($json["width"])) {
            $this->setWidth($json["width"]);
        }
        if (isset($json["height"])) {
            $this->setHeight($json["height"]);
        }
        if (isset($json["src"])) {
            $this->setSource($json["src"]);
        }
        if (isset($json["alt"])) {
            $this->setAlt($json["alt"]);
        }
    }
}
