<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\controls
 * @file
 *  Input.php
 *
 * Input class defines the base object for input elements.
 *
 * Example JSON input:
 * {
 *  "helptext": "This is a help text.",
 *  "readonly": true
 * }
 */

namespace com\oratush\forms\controls;

class Input extends BaseControl
{
    //The input type
    protected $type;
    private $readonly;
    private $helptext;

    public function __construct()
    {
        parent::__construct();

        $this->control = parent::INPUT;

        $this->type = parent::INPUT_UNKNOWN;
        $this->readonly = null;
        $this->helptext = null;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getHelptext()
    {
        return $this->helptext;
    }

    public function setHelptext($helptext)
    {
        $this->helptext = $helptext;
    }

    public function isReadonly()
    {
        if ($this->readonly != null) {
            return true;
        } else {
            return false;
        }
    }

    public function setReadonly($readonly)
    {
        if ($readonly) {
            $this->readonly = "readonly";
        } else {
            $this->readonly = null;
        }
    }

    public function parseJSON($json)
    {
        parent::parseJSON($json);

        if (isset($json["helptext"])) {
            $this->setHelptext($json["helptext"]);
        }
        if (isset($json["readonly"])) {
            $this->setReadonly($json["readonly"]);
        }
    }
}
