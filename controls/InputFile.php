<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\controls
 * @file
 *  InputFile.php
 *
 * InputFile class defines the object for file input elements.
 * Example JSON input :
 * {
 *  "accept": "image/*",
 *  "multiple": true
 * }
 */

namespace com\oratush\forms\controls;

class InputFile extends Input
{
    private $accept;
    private $multiple;
    private $files;

    public function __construct()
    {
        parent::__construct();

        $this->type = parent::INPUT_FILE;
        $this->accept = null;
        $this->multiple = null;

        $this->files = null;
    }

    /**
     * Returns an array with the file names and the temp file names
     * that were uploaded. The array format is:
     *
     * [0]["name"] = <value>;
     * [0]["tmp_name] = <value>;
     * ...
     * [n]["name"] = <value>;
     * [n]["tmp_name] = <value>;
     *
     * @access public
     *
     * @return array
     */
    public function getFiles() {
        return $this->files;
    }

    /**
     * Parse $_FILES array and set the internal $files array with the
     * appropriate values. The first file name is also set as the control
     * value.
     *
     * @access public
     */
    public function processFilesArray() {
        $control_name = $this->getName();

        if (isset($_FILES[$control_name])) {
            if ($this->isMultiple()) {
                $total = count($_FILES[$control_name]["name"]);

                for($i=0; $i<$total; $i++) {
                    $this->files[$i]["name"] = $_FILES[$control_name]["name"][$i];
                    $this->files[$i]["tmp_name"] = $_FILES[$control_name]["tmp_name"][$i];
                }
            }
            else {
                $this->files[0]["name"] = $_FILES[$control_name]["name"];
                $this->files[0]["tmp_name"] = $_FILES[$control_name]["tmp_name"];
            }

            $this->setValue($this->files[0]["name"]);
        }
    }

    public function getAccept()
    {
        return $this->accept;
    }

    public function setAccept($accept)
    {
        $this->accept = $accept;
    }

    public function isMultiple()
    {
        if ($this->multiple !=null) {
            return true;
        } else {
            return false;
        }
    }

    public function parseJSON($json)
    {
        parent::parseJSON($json);

        if (isset($json["multiple"])) {
            $this->setMultiple($json["multiple"]);
        }
        if (isset($json["accept"])) {
            $this->setAccept($json["accept"]);
        }
    }

    public function setMultiple($multiple)
    {
        if ($multiple) {
            $this->multiple = "multiple";
        } else {
            $this->multiple = null;
        }
    }
}
