<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\controls
 * @file
 *  SelectOptionGroup.php
 *
 * SelectOptionGroup class defines the object for SelectOptionGroup element.
 */

namespace com\oratush\forms\controls;

class SelectOptionGroup extends AbstractControl
{
    private $label;
    private $disabled;
    private $elements;

    public function __construct()
    {
        $this->label = null;
        $this->disabled = null;
        $this->elements = null;

        $this->control = AbstractControl::GROUP;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function isDisabled()
    {
        if ($this->disabled != null) {
            return true;
        } else {
            return false;
        }
    }

    public function setDisabled($disabled)
    {
        if ($disabled) {
            $this->disabled = "disabled";
        } else {
            $this->disabled = null;
        }
    }

    public function getElements()
    {
        return $this->elements;
    }

    public function parseJSON($json)
    {
        if (isset($json["label"])) {
            $this->setLabel($json["label"]);
        }
        if (isset($json["disabled"])) {
            $this->setDisabled($json["disabled"]);
        }
        if (isset($json["values"])) {
            $total = count($json["values"]);
            for ($i = 0; $i < $total; $i++) {
                $element = new SelectOption();
                $element->parseJSON($json["values"][$i]);

                $this->addElement($element);
            }
        }
    }

    public function addElement($element)
    {
        $this->elements[] = $element;
    }
}
