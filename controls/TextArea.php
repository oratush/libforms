<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\controls
 * @file
 *  TextArea.php
 *
 * TextArea class defines the object for TextArea element.
 *
 * Example JSON input :
 * {
 *  "cols": 10,
 *  "rows": 20,
 *  "wrap": "soft",
 *  "maxlength": 50,
 *  "helptext": "This is a help text.",
 *  "placeholder": "Enter text",
 *  "readonly": true
 * }
 */

namespace com\oratush\forms\controls;

class TextArea extends BaseControl
{
    private $cols;
    private $rows;
    private $wrap;
    private $maxlength;
    private $placeholder;
    private $readonly;
    private $helptext;

    public function __construct()
    {
        parent::__construct();

        $this->control = parent::TEXTAREA;

        $this->cols = null;
        $this->rows = null;
        $this->wrap = null;
        $this->maxlength = null;
        $this->placeholder = null;
        $this->readonly = null;
        $this->helptext = null;
    }

    public function getColumns()
    {
        return $this->cols;
    }

    public function setColumns($cols)
    {
        if ($cols != null && $cols > 0) {
            $this->cols = $cols;
        }
        else {
            $this->cols = null;
        }
    }

    public function getRows()
    {
        return $this->rows;
    }

    public function setRows($rows)
    {
        if ($rows != null && $rows > 0) {
            $this->rows = $rows;
        }
        else {
            $this->rows = null;
        }
    }

    public function getPlaceholder()
    {
        return $this->placeholder;
    }

    public function setPlaceholder($placeholder)
    {
        $this->placeholder = $placeholder;
    }

    public function getMaxlength()
    {
        return $this->maxlength;
    }

    public function setMaxlength($maxlength)
    {
        if ($maxlength != null && $maxlength > 0) {
            $this->maxlength = $maxlength;
        }
        else {
            $this->maxlength = null;
        }
    }

    public function isReadonly()
    {
        if ($this->readonly != null) {
            return true;
        } else {
            return false;
        }
    }

    public function setReadonly($readonly)
    {
        if ($readonly) {
            $this->readonly = "readonly";
        } else {
            $this->readonly = null;
        }
    }

    public function getWrap()
    {
        return $this->wrap;
    }

    public function setWrap($wrap)
    {
        if ($wrap != null && ($wrap == "soft" || $wrap == "hard")) {
            $this->wrap = $wrap;
        }
        else {
            $this->wrap = null;
        }
    }

    public function getHelptext()
    {
        return $this->helptext;
    }

    public function setHelptext($helptext)
    {
        $this->helptext = $helptext;
    }

    public function parseJSON($json)
    {
        parent::parseJSON($json);

        if (isset($json["cols"])) {
            $this->setColumns($json["cols"]);
        }
        if (isset($json["rows"])) {
            $this->setRows($json["rows"]);
        }
        if (isset($json["wrap"])) {
            $this->setWrap($json["wrap"]);
        }
        if (isset($json["maxlength"])) {
            $this->setMaxlength($json["maxlength"]);
        }
        if (isset($json["placeholder"])) {
            $this->setPlaceholder($json["placeholder"]);
        }
        if (isset($json["readonly"])) {
            $this->setReadonly($json["readonly"]);
        }
        if (isset($json["helptext"])) {
            $this->setHelptext($json["helptext"]);
        }
    }
}
