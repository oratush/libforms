<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\controls
 * @file
 *  Captcha.php
 *
 * Captcha class defines the object for CAPTCHA element.
 * Example JSON input :
 * {
 *  "label": "Type the words:",
 *  "publickey": "your_public_key",
 *  "version": 2,
 *  "helptext": "This is a help text."
 * }
 */

namespace com\oratush\forms\controls;

class Captcha extends BaseControl
{
    private $publickey;
    private $helptext;
    private $version;

    public function __construct()
    {
        parent::__construct();

        $this->control = parent::CAPTCHA;

        $this->publickey = null;
        $this->helptext = null;
        $this->version = 2;
    }

    public function getPublic()
    {
        return $this->publickey;
    }

    public function setPublic($public)
    {
        $this->publickey = $public;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getHelptext()
    {
        return $this->helptext;
    }

    public function setHelptext($helptext)
    {
        $this->helptext = $helptext;
    }

    public function parseJSON($json)
    {
        parent::parseJSON($json);

        if (isset($json["publickey"])) {
            $this->setPublic($json["publickey"]);
        }
        if (isset($json["helptext"])) {
            $this->setHelptext($json["helptext"]);
        }
        if (isset($json["version"])) {
            $this->setVersion($json["version"]);
        }
    }
}
