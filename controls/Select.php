<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\controls
 * @file
 *  Select.php
 *
 * Select class defines the object for Select element.
 *
 * Example JSON input:
 *
 * MIN SET:
 * {
 *  "label": "Select country",
 *  "name": "countries",
 *  "elements": [
 *      {
 *        "label": "Greece",
 *        "value": "GR",
 *        "disabled": true,
 *        "selected": true
 *      },
 *      {
 *        "label": "Europe",
 *        "disabled": true,
 *        "values": [
 *            {
 *              "label": "Greece",
 *              "value": "GR"
 *            },
 *            {
 *              "label": "Germany",
 *              "value": "DE"
 *            }
 *          ]
 *      },
 *      {
 *        "label": "Germany",
 *        "value": "DE"
 *      }
 *    ]
 * }
 *
 * FULL SET:
 * {
 *  "id": "12345",
 *  "label": "Select country",
 *  "name": "countries",
 *  "multiple": true,
 *  "size": 2,
 *  "helptext": "This is a help text.",
 *  "elements": [
 *      // SEE ABOVE
 *    ],
 *  "autofocus": true,
 *  "disabled": true,
 *  "classes": [
 *      "btn",
 *      "btn-default"
 *    ],
 *  "attributes": {
 *      "style": "padding-left: 20px"
 *    }
 * }
 */

namespace com\oratush\forms\controls;

class Select extends BaseControl
{
    private $multiple;
    private $size;
    private $elements;
    private $helptext;

    public function __construct()
    {
        parent::__construct();

        $this->control = parent::SELECT;

        $this->multiple = null;
        $this->size = 1;
        $this->elements = null;
        $this->helptext = null;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size)
    {
        if ($size > 0) {
            $this->size = $size;
        }
    }

    public function getElements()
    {
        return $this->elements;
    }

    /**
     * Marks as selected the element for the provided value.
     *
     * @access public
     *
     * @param $value string the value of the element to flag as selected.
     */
    public function setSelected($value)
    {
        $this->setSelectedElement($value, $this->elements);
    }

    /**
     * Since elements can be direct elements or elements inside a group this function is recursive
     * taking as input the value and the local elements array.
     *
     * @access private
     *
     * @param $value string the value of the element to flag as selected.
     * @param $elems array the elements array to look inside.
     *
     * @return boolean true if an element was marked as selected.
     */
    private function setSelectedElement($value, $elems)
    {
        $done = false;

        if ($elems != null)
        {
            $total = count($elems);
            for ($i = 0; $i < $total; $i++) {
                $item = $elems[$i];

                if ($item->getControlType() == parent::GROUP) {
                    //Call self, one level down
                    $done = $this->setSelectedElement($value, $item->getElements());
                } else {
                    if ($value == $item->getValue()) {
                        $item->setSelected(true);
                        $done = true;
                    }
                }

                if ($done == true) {
                    break;
                }
            }
        }

        return $done;
    }

    /**
     * Removes the selected flag from all elements.
     *
     * @access public
     */
    public function removeSelections()
    {
        $this->removeSelectionsElement($this->elements);
    }

    /**
     * Removes the selected flag from the array of elements. This method is recursive for elements
     * and group of elements.
     *
     * @access private
     *
     * @param $elems array the elements array to look inside.
     */
    private function removeSelectionsElement($elems)
    {
        if ($elems != null)
        {
            $total = count($elems);
            for ($i = 0; $i < $total; $i++) {
                $item = $elems[$i];

                if ($item->getControlType() == parent::GROUP) {
                    //Call self, one level down
                    $this->removeSelectionsElement($item->getElements());
                } else {
                    $item->setSelected(false);
                }
            }
        }
    }

    /**
     * Check if this field has at least one item selected.
     *
     * @access public
     *
     * @return boolean
     */
    public function hasItemSelected()
    {
        return $this->hasElementSelected($this->elements);
    }

    /**
     * Check if this field has at least one item selected. This method is recursive for elements
     * and group of elements.
     *
     * @access private
     *
     * @param $elems array the elements array to look inside.
     *
     * @return boolean
     */
    private function hasElementSelected($elems)
    {
        $outcome = false;

        if ($elems != null)
        {
            $total = count($elems);
            for ($i = 0; $i < $total; $i++) {
                $item = $elems[$i];

                if ($item->getControlType() == parent::GROUP) {
                    //Call self, one level down
                    $outcome = $this->hasElementSelected($item->getElements());
                } else {
                    if ($item->isSelected()) {
                        $outcome = true;
                    }
                }

                if ($outcome == true) {
                    break;
                }
            }
        }

        return $outcome;
    }

    /**
     * Get the first element that is flagged as selected.
     *
     * @access public
     *
     * @return string|null value of the selected element or null if no element is selected.
     */
    public function getValue()
    {
        $results = $this->getValues();

        if ($results != null) {
            return $results[0];
        } else {
            return null;
        }
    }

    /**
     * Get all elements that are flagged as selected.
     *
     * @access public
     *
     * @return array|null array of strings or null if no element is selected.
     */
    public function getValues()
    {
        return $this->getElementValues($this->elements);
    }

    /**
     * Get all elements that are flagged as selected. This method is recursive for elements
     * and group of elements. Also multiple results are returned only if mutliple is set
     * for this control.
     *
     * @access private
     *
     * @param $elems array the elements array to look inside.
     *
     * @return array|null array of strings or null if no element is selected.
     */
    private function getElementValues($elems)
    {
        $outcome = array();
        $count = 0;

        if ($elems != null)
        {
            $total = count($elems);
            for ($i = 0; $i < $total; $i++) {
                $item = $elems[$i];

                if ($item->getControlType() == parent::GROUP) {
                    //Call self, one level down
                    $result = $this->getElementValues($item->getElements());

                    if ($result != null) {
                        //We may receive multiple results
                        $total_result = count($result);
                        for ($j = 0; $j < $total_result; $j++) {
                            $outcome[$count] = $result[$j];
                            $count++;
                        }
                    }
                } else {
                    if ($item->isSelected()) {
                        $outcome[$count] = $item->getValue();
                        $count++;
                    }
                }

                if ($count > 0 && !$this->isMultiple()) {
                    break;
                }
            }
        }

        if ($count == 0) {
            return null;
        } else {
            return $outcome;
        }
    }

    public function isMultiple()
    {
        if ($this->multiple != null) {
            return true;
        } else {
            return false;
        }
    }

    public function setMultiple($multiple)
    {
        if ($multiple) {
            $this->multiple = "multiple";
        } else {
            $this->multiple = null;
        }
    }

    public function getHelptext()
    {
        return $this->helptext;
    }

    public function setHelptext($helptext)
    {
        $this->helptext = $helptext;
    }

    public function parseJSON($json)
    {
        parent::parseJSON($json);

        if (isset($json["multiple"])) {
            $this->setMultiple($json["multiple"]);
        }
        if (isset($json["helptext"])) {
            $this->setHelptext($json["helptext"]);
        }
        if (isset($json["size"])) {
            $this->setSize($json["size"]);
        }
        if (isset($json["elements"])) {
            $total = count($json["elements"]);
            for ($i = 0; $i < $total; $i++) {
                if (isset($json["elements"][$i]["values"])) {
                    $element = new SelectOptionGroup();
                } else {
                    $element = new SelectOption();
                }

                $element->parseJSON($json["elements"][$i]);
                $this->addElement($element);
            }
        }
    }

    public function addElement($element)
    {
        $this->elements[] = $element;
    }
}
