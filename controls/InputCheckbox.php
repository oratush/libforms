<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\controls
 * @file
 *  InputCheckbox.php
 *
 * InputCheckbox class defines the object for checkbox elements.
 *
 * Example JSON input:
 * FULL SET:
 * {
 *  "id": "12345",
 *  "label": "Select country",
 *  "name": "countries",
 *  "multiline": true,
 *  "helptext": "This is a help text.",
 *  "elements": [
 *      {
 *        "label": "Greece",
 *        "value": "GR",
 *        "disabled": true,
 *        "selected": true
 *      },
 *      {
 *        "label": "Germany",
 *        "value": "DE"
 *      }
 *    ],
 *  "autofocus": true,
 *  "disabled": true,
 *  "classes": [
 *      "btn",
 *      "btn-default"
 *    ],
 *  "attributes": {
 *      "style": "padding-left: 20px"
 *    }
 * }
 */

namespace com\oratush\forms\controls;

class InputCheckbox extends Input
{
    private $elements;
    private $multiline;

    public function __construct()
    {
        parent::__construct();

        $this->type = parent::INPUT_CHECKBOX;

        $this->elements = null;
        $this->multiline = true;
    }

    public function isMultiline()
    {
        return $this->multiline;
    }

    public function setMultiline($multiline)
    {
        $this->multiline = $multiline;
    }

    public function getElements()
    {
        return $this->elements;
    }

    /**
     * Removes the selected flag from all elements.
     *
     * @access public
     */
    public function removeSelections()
    {
        if ( $this->elements!=null )
        {
            $total = count($this->elements);

            for ($i = 0; $i < $total; $i++) {
                $item = $this->elements[$i];

                $item->setSelected(false);
            }
        }
    }

    /**
     * Marks as selected the element for the provided value.
     *
     * @access public
     *
     * @param $value string the value of the element to flag as selected.
     */
    public function setSelected($value)
    {
        if ( $this->elements!=null )
        {
            $total = count($this->elements);

            for ($i = 0; $i < $total; $i++) {
                $item = $this->elements[$i];

                if ($value == $item->getValue()) {
                    $item->setSelected(true);
                    break;
                }
            }
        }
    }

    /**
     * Check if this field has at least one item selected.
     *
     * @access public
     *
     * @return boolean
     */
    public function hasItemSelected()
    {
        $outcome = false;

        if ( $this->elements!=null )
        {
            $total = count($this->elements);

            for ($i = 0; $i < $total; $i++) {
                $elem = $this->elements[$i];

                if ($elem->isSelected()) {
                    $outcome = true;
                    break;
                }
            }
        }

        return $outcome;
    }

    /**
     * Get the first element that is flagged as selected.
     *
     * @access public
     *
     * @return string|null value of the selected element or null if no element is selected.
     */
    public function getValue()
    {
        $results = $this->getValues();

        if ($results != null) {
            return $results[0];
        } else {
            return null;
        }
    }

    /**
     * Get all elements that are flagged as selected.
     *
     * @access public
     *
     * @return array|null array of strings or null if no element is selected.
     */
    public function getValues()
    {
        $outcome = array();
        $count = 0;

        if ( $this->elements!=null )
        {
            $total = count($this->elements);
            for ($i = 0; $i < $total; $i++) {
                $item = $this->elements[$i];

                if ($item->isSelected()) {
                    $outcome[$count] = $item->getValue();
                    $count++;
                }
            }
        }

        if ($count == 0) {
            return null;
        } else {
            return $outcome;
        }
    }

    public function parseJSON($json)
    {
        parent::parseJSON($json);

        if (isset($json["multiline"])) {
            $this->setMultiline($json["multiline"]);
        }
        if (isset($json["elements"])) {
            $total = count($json["elements"]);
            for ($i = 0; $i < $total; $i++) {
                $element = new SelectOption();

                $element->parseJSON($json["elements"][$i]);
                $this->addElement($element);
            }
        }
    }

    public function addElement($element)
    {
        $this->elements[] = $element;
    }
}
