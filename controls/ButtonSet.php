<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\controls
 * @file
 *  ButtonSet.php
 *
 * ButtonSet class defines the object for button set element.
 *
 * @see Button
 *
 * Example JSON input :
 * {
 *  "buttons": [
 *      {
 *        "label": "Submit",
 *        "type": "submit"
 *      },
 *      {
 *        "label": "Reset",
 *        "type": "reset"
 *      }
 *    ]
 * }
 */

namespace com\oratush\forms\controls;

use com\oratush\forms\Form;

class ButtonSet extends AbstractControl
{
    private $buttons;

    public function __construct()
    {
        $this->buttons = null;
        $this->control = AbstractControl::BUTTONSET;
    }

    public function getButtons()
    {
        return $this->buttons;
    }

    public function parseJSON($json)
    {
        if (isset($json["buttons"])) {
            $total = count($json["buttons"]);

            for ($i = 0; $i < $total; $i++) {
                $element = Form::parseControl("Button", $json["buttons"][$i]);

                $this->addButton($element);
            }
        }
    }

    public function addButton($button)
    {
        $this->buttons[] = $button;
    }
}
