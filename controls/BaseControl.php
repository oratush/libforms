<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\controls
 * @file
 *  BaseControl.php
 *
 * BaseControl class defines the object for BaseControl element.
 *
 * Example JSON input :
 * {
 *  "id": "12345",
 *  "name": "submit",
 *  "value": "frm_button",
 *  "label": "Submit",
 *  "autofocus": true,
 *  "disabled": true,
 *  "classes": [
 *      "btn",
 *      "btn-default"
 *    ],
 *  "attributes": {
 *      "style": "padding-left: 20px"
 *    }
 * }
 */

namespace com\oratush\forms\controls;

class BaseControl extends AbstractControl
{
    //The unique ID
    private $id;
    //The name
    private $name;
    //The value
    // - Not applicable for SELECT, CHECKBOX, RADIO
    private $value;

    //The label
    // - For button the label is the display text
    private $label;

    //Additional attributes (key-value array)
    private $attributes;
    //Additional classes (array)
    private $classes;

    //To autofocus the element
    private $autofocus;
    //Disabled element
    private $disabled;

    public function __construct()
    {
        $this->id = null;
        $this->name = null;
        $this->value = null;
        $this->label = null;
        $this->attributes = null;
        $this->classes = null;
        $this->autofocus = null;
        $this->disabled = null;

        $this->control = parent::UNKNOWN;
    }

    public function getID()
    {
        return $this->id;
    }

    public function setID($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function isDisabled()
    {
        if ($this->disabled != null) {
            return true;
        } else {
            return false;
        }
    }

    public function setDisabled($disabled)
    {
        if ($disabled) {
            $this->disabled = "disabled";
        } else {
            $this->disabled = null;
        }
    }

    public function isAutofocus()
    {
        if ($this->autofocus != null) {
            return true;
        } else {
            return false;
        }
    }

    public function setAutofocus($autofocus)
    {
        if ($autofocus) {
            $this->autofocus = "autofocus";
        } else {
            $this->autofocus = null;
        }
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function getClasses()
    {
        return $this->classes;
    }

    public function parseJSON($json)
    {
        if (isset($json["id"])) {
            $this->setID($json["id"]);
        }
        if (isset($json["name"])) {
            $this->setName($json["name"]);
        }
        if (isset($json["value"])) {
            $this->setValue($json["value"]);
        }
        if (isset($json["label"])) {
            $this->setLabel($json["label"]);
        }
        if (isset($json["autofocus"])) {
            $this->setAutofocus($json["autofocus"]);
        }
        if (isset($json["disabled"])) {
            $this->setDisabled($json["disabled"]);
        }

        if (isset($json["classes"])) {
            $total = count($json["classes"]);
            for ($i = 0; $i < $total; $i++) {
                $this->addClass($json["classes"][$i]);
            }
        }

        if (isset($json["attributes"])) {
            $total = count($json["attributes"]);

            $keys = array_keys($json["attributes"]);
            for ($i = 0; $i < $total; $i++) {
                $this->setAttribute($keys[$i], $json["attributes"][$keys[$i]]);
            }
        }
    }

    public function addClass($value)
    {
        $this->classes[] = $value;
    }

    public function setAttribute($key, $value)
    {
        $this->attributes[$key] = $value;
    }
}
