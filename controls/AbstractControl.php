<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\controls
 * @file
 *  AbstractControl.php
 *
 * AbstractControl class defines the object for AbstractControl element.
 */

namespace com\oratush\forms\controls;

abstract class AbstractControl
{
    const UNKNOWN = "UNKNOWN";
    const BUTTON = "BUTTON";
    const BUTTONSET = "BUTTONSET";
    const CAPTCHA = "CAPTCHA";
    const INPUT = "INPUT";
    const INPUT_UNKNOWN = "unknown";
    const INPUT_CHECKBOX = "checkbox";
    const INPUT_FILE = "file";
    const INPUT_HIDDEN = "hidden";
    const INPUT_IMAGE = "image";
    const INPUT_PASSWORD = "password";
    const INPUT_RADIO = "radio";
    const INPUT_TEXT = "text";
    const OPTION = "OPTION";
    const GROUP = "GROUP";
    const SELECT = "SELECT";
    const TEXTAREA = "TEXTAREA";
    const FIELDSET = "FIELDSET";

    //Type of control
    protected $control;

    /**
     * Get the control type.
     *
     * @access public
     *
     * @return string
     */
    public function getControlType()
    {
        return $this->control;
    }

    /**
     * Abstract function to parse a JSON input and initialize the control.
     *
     * @access public
     *
     * @param $json array associative array with the decoded JSON string
     */
    abstract public function parseJSON($json);
}
