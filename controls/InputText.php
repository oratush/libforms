<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\controls
 * @file
 *  InputText.php
 *
 * InputText class defines the object for InputText element.
 *
 * Example JSON input :
 * {
 *  "id": "12345",
 *  "name": "username",
 *  "value": "poursal",
 *  "label": "Username",
 *  "autofocus": true,
 *  "disabled": false,
 *  "classes": [
 *      "btn",
 *      "btn-default"
 *    ],
 *  "attributes": {
 *      "style": "padding-left: 20px"
 *    },
 *  "helptext": "Please enter your username.",
 *  "readonly": false,
 *  "placeholder": "Enter username",
 *  "autocomplete": true,
 *  "size": 10,
 *  "maxlength": 100
 * }
 */

namespace com\oratush\forms\controls;

class InputText extends Input
{
    private $placeholder;
    private $autocomplete;
    private $size;
    private $maxlength;

    public function __construct()
    {
        parent::__construct();

        $this->type = parent::INPUT_TEXT;

        $this->placeholder = null;
        $this->autocomplete = null;
        $this->size = null;
        $this->maxlength = null;
    }

    public function getPlaceholder()
    {
        return $this->placeholder;
    }

    public function setPlaceholder($placeholder)
    {
        $this->placeholder = $placeholder;
    }

    public function isAutocomplete()
    {
        if ($this->autocomplete != null || $this->autocomplete == "on") {
            return true;
        } else {
            return false;
        }
    }

    public function setAutocomplete($complete)
    {
        if ($complete) {
            $this->autocomplete = "on";
        } else {
            $this->autocomplete = "off";
        }
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size)
    {
        if ($size != null && $size > 0) {
            $this->size = $size;
        }
        else {
            $this->size = null;
        }
    }

    public function getMaxlength()
    {
        return $this->maxlength;
    }

    public function setMaxlength($maxlength)
    {
        if ($maxlength != null && $maxlength > 0) {
            $this->maxlength = $maxlength;
        }
        else {
            $this->maxlength = null;
        }
    }

    public function parseJSON($json)
    {
        parent::parseJSON($json);

        if (isset($json["placeholder"])) {
            $this->setPlaceholder($json["placeholder"]);
        }
        if (isset($json["autocomplete"])) {
            $this->setAutocomplete($json["autocomplete"]);
        }
        if (isset($json["size"])) {
            $this->setSize($json["size"]);
        }
        if (isset($json["maxlength"])) {
            $this->setMaxlength($json["maxlength"]);
        }
    }
}
