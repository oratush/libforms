<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\controls
 * @file
 *  SelectOption.php
 *
 * SelectOption class defines the object for SelectOption element.
 */

namespace com\oratush\forms\controls;

class SelectOption extends AbstractControl
{
    private $value;
    private $label;
    private $disabled;
    private $selected;

    public function __construct()
    {
        $this->value = null;
        $this->label = null;

        $this->disabled = null;
        $this->selected = null;
        $this->control = AbstractControl::OPTION;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function isDisabled()
    {
        if ($this->disabled != null) {
            return true;
        } else {
            return false;
        }
    }

    public function setDisabled($disabled)
    {
        if ($disabled) {
            $this->disabled = "disabled";
        } else {
            $this->disabled = null;
        }
    }

    public function isSelected()
    {
        if ($this->selected != null) {
            return true;
        } else {
            return false;
        }
    }

    public function setSelected($selected)
    {
        if ($selected) {
            $this->selected = "selected";
        } else {
            $this->selected = null;
        }
    }

    /**
     * {
     *  "label": "Greece",
     *  "value": "GR",
     *  "disabled": true,
     *  "selected": true
     * }
     */
    public function parseJSON($json)
    {
        if (isset($json["label"])) {
            $this->setLabel($json["label"]);
        }
        if (isset($json["value"])) {
            $this->setValue($json["value"]);
        }
        if (isset($json["disabled"])) {
            $this->setDisabled($json["disabled"]);
        }
        if (isset($json["selected"])) {
            $this->setSelected($json["selected"]);
        }
    }
}
