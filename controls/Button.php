<?php
/**
 * @copyright   2014 Oratush Team
 * @author      Vassilis Poursalidis (poursal@gmail.com)
 * @author      Christos Bekos (chris.bekos@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 *  com\oratush\forms\controls
 * @file
 *  Button.php
 *
 * Button class defines the object for button element.
 *
 * Example JSON input:
 * MIN SET:
 * {
 *  "label": "Submit",
 *  "type": "submit"
 * }
 *
 * FULL SET:
 * {
 *  "id": "12345",
 *  "label": "Submit",
 *  "type": "submit",
 *  "name": "submit",
 *  "value": "frm_button",
 *  "autofocus": true,
 *  "disabled": true,
 *  "classes": [
 *      "btn",
 *      "btn-default"
 *    ],
 *  "attributes": {
 *      "style": "padding-left: 20px"
 *    }
 * }
 */

namespace com\oratush\forms\controls;

class Button extends BaseControl
{
    const TYPE_BUTTON = "button";
    const TYPE_RESET  = "reset";
    const TYPE_SUBMIT = "submit";
    const TYPE_LINK   = "link";

    //The button type: button, reset, submit, link
    private $type;
    private $link;

    public function __construct()
    {
        parent::__construct();

        $this->control = parent::BUTTON;
        $this->type = self::TYPE_BUTTON;
        $this->link = null;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        if ($type == self::TYPE_BUTTON || $type == self::TYPE_RESET 
                || $type == self::TYPE_SUBMIT || $type == self::TYPE_LINK) {
            $this->type = $type;
        }
    }

    public function getLink()
    {
        return $this->link;
    }

    public function setLink($link)
    {
        $this->link = $link;
    }

    public function parseJSON($json)
    {
        parent::parseJSON($json);

        if (isset($json["type"])) {
            $this->setType($json["type"]);
        }
        if (isset($json["link"])) {
            $this->setLink($json["link"]);
        }
    }
}
